<?php
class ImageUploader{
    private $file;
    private $path;

    /**
     * ImageUploader constructor.
     * @param $file (принимает элемент массива $_FILES)
     */
    public function __construct($file){
        $this->file = $file;
        $this->path = ROOT_PATH."/images/uploads/"; //default path
    }

    /**
     * @param null $path (необязательный параметр, изменяет дефолтный путь для загрузки файла)
     * @param null $prefix (необязательный параметр, изменяет имя файла)
     * @return bool (возвращает имя загруженного файла или false в случае неудачи)
     */
    public function uploadFile($path = null, $prefix = null){
        $flag = false;
        $prefix_ = "";
        if($prefix != null) $prefix_ = $prefix;
        if($path != null) $this->path = ROOT_PATH.$path;

        if($this->checkName() && $this->checkType()){
            if(move_uploaded_file($this->file["tmp_name"],$this->path.$this->file["name"])){
                if(rename($this->path.$this->file["name"],$this->path.$prefix_.$this->file["name"]))
                    $flag = $prefix_.$this->file["name"];
                else
                    $flag = $this->file["name"];
            }
            else
                echo "Произошла ошибка загрузки файла на сервер! ";
        }

        return $flag;
    }

    /**
     * @param null $path
     * @param null $prefix
     * @return bool|string
     */
    public function uploadFiles($path = null, $prefix = null){
        $result = array();
        $prefix_ = "";
        if($prefix != null) $prefix_ = $prefix;
        if($path != null) $this->path = ROOT_PATH.$path;


        for($i=0;$i<count($this->file["tmp_name"]);$i++){
            if(move_uploaded_file($this->file["tmp_name"][$i],$this->path.$prefix_.$this->file["name"][$i])){
                array_push($result,$prefix_.$this->file["name"][$i]);
            }
            else
                echo "Произошла ошибка загрузки файла на сервер! ";
        }


//        for($i=0;$i<count($this->file["tmp_name"]);$i++){
//            if(move_uploaded_file($this->file["tmp_name"][$i],$this->path.$this->file["name"][$i])){
//                if(rename($this->path.$this->file["name"][$i],$this->path.$prefix_.$this->file["name"][$i]))
//                    array_push($result,$prefix_.$this->file["name"][$i]);
//                else
//                    array_push($result,$this->file["name"][$i]);
//            }
//            else
//                echo "Произошла ошибка загрузки файла на сервер! ";
//        }

        return $result;
    }


    /**
     * Проверяет является ли файл исполняемым
     * @return bool
     */
    private function checkName(){
        $assert = false;
        $file_name = $this->file['name'];
        $path = [".php",".php5", ".php4", ".php3", ".phtml", ".pl", ".js", ".html", ".htm"];

        if(strlen($file_name)>0) {
            foreach ($path as $item) {
                if (preg_match("/$item\$/i", $file_name)) {
                    echo 'Не правильное расширение файла!';
                    exit;
                }
            }
            $assert = true;
        }

        return $assert;
    }

    /**
     * Проверяет является ли файл изображением
     * @return bool
     */
    private function checkType(){
        $assert = false;
        $file_type = $this->file['type'];

        if($file_type == "image/gif" || $file_type == "image/jpeg" || $file_type == "image/png")
            $assert = true;
        else
            echo "<script>alert('Неправильное расширение файла!')</script>";

        return $assert;
    }

}