<?php

abstract class Model
{
	protected function query($sql,array $params=null)
	{
		return (is_null($params) ? DB::sqlQuery($sql) : DB::sqlQuery($sql,$params));
	}

	public function redirect($url)
	{
		echo "<script type='text/javascript'>window.location = 'http://".$url."';</script>";
	}
}
