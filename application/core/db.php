<?php
/**
 * Class DB (singleton)
 */
class DB{
    protected static $_instance;

    private $dbname = "alteza";
    private $user = "root";
    private $pass = "";
    private $DBH=null;
    private $params = null;
    private $data = array();

    /**
     * @return DB (единственный экземпляр)
     */
    public static function start() {
        if (self::$_instance === null) {
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    private function __construct(){
        $this->connect();
    }

    /**
     * Создает новый экземпляр класса PDO,
     *
     * подключается к БД,
     *
     * устанавливает кодировку БД
     */
    private function connect(){
        try{
            $this->DBH = new PDO("mysql:host=localhost;dbname=".$this->dbname,$this->user,$this->pass);
            //$this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
            $this->DBH->exec("SET NAMES 'utf8'");
            $this->DBH->exec("SET CHARACTER SET utf8");
            $this->DBH->exec("SET CHARACTER_SET_CONNECTION=utf8");
        }catch (PDOException $e){
            echo "Can't connect! ".$e->getMessage();
        }
    }

    /**
     * Запрещает клонирование объекта
     */
    private function __clone() {}

    /**
     * Запрещает клонирование объекта
     */
    private function __wakeup() {}

    /**
     * @param $sql (обязательный в виде SQL-выражения)
     *
     * @param array|null $params (не обязательный - массив параметров для insert,update и т.д.)
     *
     * @return array
     */
    protected static function query($sql,array $params=null){
        $self = self::$_instance;

        if(is_null($params) && !is_null($self->DBH)){
            try{
                $STH = $self->DBH->prepare($sql);
                $STH->execute();
                $STH->setFetchMode(PDO::FETCH_ASSOC);
                $i=0;
                while($row = $STH->fetch()){
                    $self->data[$i] = $row;
                    $i++;
                }
            }catch(PDOException $e){
                echo "Error! ".$e->getMessage();
            }
        }
        else{
            if(!is_null($self->DBH)){
                try{
                    foreach($params as $param){
                        $self->params[] = $param;
                    }
                    $STH = $self->DBH->prepare($sql);
                    $STH->execute($self->params);
                    $STH->setFetchMode(PDO::FETCH_ASSOC);
                    $i=0;
                    while($row = $STH->fetch()){
                        $self->data[$i] = $row;
                        $i++;
                    }
                }catch (PDOException $e){
                    echo "Error! ".$e->getMessage();
                }
            }
        }
        return $self->data;
    }

    /**
     * Очищаем массив data
     */
    protected static function cleanData(){
        self::$_instance->data = null;
    }

    /**
     * Закрываем соединение с БД
     */
    private function closeConnection(){
        $this->DBH = null;
    }

    /**
     * @param $sql
     * @param array|null $params
     * @return array
     */
    public static function sqlQuery($sql,array $params=null){
        $data = (is_null($params) ? self::query($sql) : self::query($sql,$params));
        self::cleanData();
        return $data;
    }
}

