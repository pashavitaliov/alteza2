<?php
class Route
{

	public static function start()
	{
		// контроллер и действие по умолчанию
		$controller_name = 'Main';
		$action_name = 'index';
		$param = null;
		$path = 'application/users/';
		$routes = explode('/', trim($_SERVER['REQUEST_URI']));

		$parsed_routes = "";
		foreach($routes as $key=>$route){
			if(substr_count($route,'?')>0){
				$arr = explode('?',$route);
				$route = $arr[0];
			}
			($key!=0) ? $parsed_routes .="/".$route : $parsed_routes .=$route;

		}
		$routes = explode('/',$parsed_routes);

		if(!empty($routes[1]) && $routes[1] == "admin"){
			if(count($routes) >=3)
			{
				if($routes[2] == "upload"){
					$controller_name = "Admin_Upload";
					$path = "application/admin/";
				}else{
					$controller_name = 'Admin_'.$routes[2];
					$path = "application/admin/";
					if ( !empty($routes[3]) )
					{
						$action_name = $routes[3];
						if (!empty($routes[4]))
						{
							$param = $routes[4];
						}
					}
				}
			}
		}else{
			if((!empty($routes[1]) && $routes[1] == "catalog" && !empty($routes[2])) || (!empty($routes[1]) && $routes[1] == "gallery" && !empty($routes[2])) ){
				if($routes[1] == "catalog"){
					if($routes[2] != "sistemy_kreplenija"){
						if($routes[2] == "get_color_data"){
							$controller_name = $routes[1];
							$action_name = "get_color_data";
						}else{
							$id = intval(Route::urlMaskMapping($routes[2]));
							if($id>0)
							{
								$controller_name = $routes[1];
								$action_name = "show_material";
								$param = $id;
							}else
							{
								Route::ErrorPage404();
							}
						}

					}else{
						$controller_name = $routes[1];
						$action_name = "show_systems";
					}

				}

				if($routes[1] == "gallery"){
					$id = intval(Route::urlMaskMapping($routes[2]));
					if($id>0)
					{
						$controller_name = $routes[1];
						$action_name = "show_gallery";
						$param = $id;
					}else
					{
						Route::ErrorPage404();  // ИСПРАВИТЬ 404 СТРАНИЦУ!!!!!!!
					}
				}
			}
			else{

				if(($routes[1] == "articles" || $routes[1] == "news") && !empty($routes[2])){
					$id = $routes[2];
					if(preg_match('/^\d+$/', $id)){
						$controller_name = $routes[1];
						$action_name = "show";
						$param = $id;
					}else{
						Route::ErrorPage404();
					}
				}else{
					if ( !empty($routes[1]))
					{
						$controller_name = $routes[1];
					}
					if ( !empty($routes[2]) )
					{
						$action_name = $routes[2];
					}
					if( !empty($routes[3]) )
					{
						$param = $routes[3];
					}
				}
			}
		}

		// добавляем префиксы
		$model_name = 'Model_'.$controller_name;
		$controller_name = 'Controller_'.$controller_name;
		$action_name = 'action_'.$action_name;

		// подцепляем файл с классом модели (файла модели может и не быть)

		$model_file = strtolower($model_name).'.php';
		$model_path = $path."models/".$model_file;
		if(file_exists($model_path))
		{
			include $path."models/".$model_file;
		}

		// подцепляем файл с классом контроллера
		$controller_file = strtolower($controller_name).'.php';
		$controller_path = $path."controllers/".$controller_file;
		if(file_exists($controller_path))
		{
			include $path."controllers/".$controller_file;

			// создаем контроллер
			$controller = new $controller_name;
			$action = $action_name;

			if(method_exists($controller, $action))
			{
				// вызываем действие контроллера
				$controller->$action($param);
			}
			else
			{
				Route::ErrorPage404();
			}
		}
		else
		{
			Route::ErrorPage404();
		}


	}

	private static function urlMaskMapping($url_mask){
		try{
			$id = DB::sqlQuery("SELECT id FROM materials WHERE enable = 1 AND url_mask = '{$url_mask}'")[0]['id'];
			if(intval($id) <= 0)
				Route::ErrorPage404();
			return $id;
		}catch(Error $e){
			return 0;
		}

	}

	public static function ErrorPage404()
	{
		include "application/users/controllers/controller_404.php";
		$controller = new Controller_404();
		$action = "action_index";
		if(method_exists($controller, $action))
		{
			$controller->$action();
		}
    }

	public static function ErrorPage4041($controller)
	{
		//include "application/users/controllers/controller_404.php";
		//$controller = new Controller_404();
		$action = "action_404";
		$controller->$action();

	}
    
}
