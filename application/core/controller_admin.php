<?php

class Controller_Admin extends Controller{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Model_Admin();
    }

    protected function check_auth()
    {
        if(method_exists($this->model,'check_auth_pages')){
            $this->model->check_auth_pages();
        }else
        {
            echo "Доступ запрещен";
            exit();
        }
    }
}