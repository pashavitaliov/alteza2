<?php

abstract class Controller {
	
	protected $model;
	protected $view;
	
	public function __construct()
	{
		$this->view = new View();
	}
	
	/**
	 * Действие (action), вызываемое по умолчанию
	 */
	//public function action_index(){}

	public function action_404(){
		header('HTTP/1.1 404 Not Found');
		$this->view->generate('users','404_view.php', 'template_view.php');
	}
}
