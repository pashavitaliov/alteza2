<?php

class Model_Users extends Model{
    private $pageData = [];

    public function __construct($name = null)
    {
        if (!is_null($name))
        {
            $this->pageData = $this->query("SELECT id, name, title, h_title, seo_title, seo_description, seo_keywords, system FROM pages WHERE name = '{$name}'")[0];
        }
    }

    public function setPage()
    {
        return new Page(
            $this->preparePageData()
        );
    }

    private function preparePageData()
    {
        return array(
            "page_data" => $this->pageData,
            "aside_items" => $this->getPageAsideItems()
        );
    }

    private function getPageAsideItems()
    {
        $items = [];
        $items_id = $this->query("SELECT nav_item_id AS id FROM navigation_mapping WHERE page_id = {$this->pageData['id']}");
        foreach($items_id as $item)
        {
            array_push(
                $items,
                $this->query("SELECT id, page_id, title, href, img FROM navigation WHERE id ={$item['id']} AND menu_type = 'aside'")[0]
            );
        }
        return $items;
    }

    public function getPageDefaultAside()
    {
        $items = [];
        $defaultAsideItemsIds = Page::$pageDefaultAsideItems;

        foreach($defaultAsideItemsIds as $item)
        {
            array_push(
                $items,
                $this->query("SELECT id, page_id, title, href, img FROM navigation WHERE id ={$item} AND menu_type = 'aside'")[0]
            );
        }
        return $items;

    }

    public function insertCall(){
        $this->query("INSERT INTO order_calls(name, phone) VALUES ('{$_POST['name']}','{$_POST['phone']}')");
    }

    public function insertDiller(){
        $this->query("INSERT INTO dillers(name, company, city, phone, email) VALUES ('{$_POST['name']}','{$_POST['company']}', '{$_POST['city']}', '{$_POST['phone']}', '{$_POST['email']}')");
    }

    public function getContent($id){
        return $this->query("SELECT content, mobile_content FROM pages_content WHERE p_id = {$id}")[0];
    }

}