<?php

class View
{
	
	//public $template_view; // здесь можно указать общий вид по умолчанию.
	
	/*
	$content_file - виды отображающие контент страниц;
	$template_file - общий для всех страниц шаблон;
	$data - массив, содержащий элементы контента страницы. Заполняется в модели.
	*/
	public function generate($type, $content_view, $template_view, $data = null, $side_bar = null)
	{
		include 'application/'.$type.'/views/'.$template_view;
	}
}
