<?php

class Page{

    private $id;
    private $name;
    private $h_title;
    private $seo_title;
    private $seo_description;
    private $seo_keywords;
    private $aside = array();
    public static $pageDefaultAsideItems = [1,3,4,8];

    public function __construct(array $data)
    {
        $this->id = $data["page_data"]["id"];
        $this->name = $data["page_data"]["name"];
        $this->h_title = $data["page_data"]["h_title"];
        $this->page_title = $data["page_data"]["title"];
        $this->seo_title = $data["page_data"]["seo_title"];
        $this->seo_description = $data["page_data"]["seo_description"];
        $this->seo_keywords = $data["page_data"]["seo_keywords"];
        $this->aside = $data["aside_items"];
    }

    /**
     * @return array
     */
    public function getSeoData()
    {
        return array(
            "title" => $this->seo_title,
            "desc" => $this->seo_description,
            "keywords" => $this->seo_keywords,
            "page_title" => $this->page_title,
        );
    }

    public function setSeoData($title,$description,$keywords)
    {
        $this->seo_title = $title;
        $this->seo_description = $description;
        $this->seo_keywords = $keywords;
    }

    /**
     * @return mixed
     */
    public function getSeoTitle()
    {
        return $this->seo_title;
    }

    /**
     * @return mixed
     */
    public function getSeoDescription()
    {
        return $this->seo_description;
    }

    /**
     * @return mixed
     */
    public function getSeoKeywords()
    {
        return $this->seo_keywords;
    }

    /**
     * @return mixed
     */
    public function getAside()
    {
        return $this->aside;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $title
     */
    public function setSeoTitle($title)
    {
        $this->seo_title = $title;
    }

    /**
     * @return mixed
     */
    public function getHTitle()
    {
        return $this->h_title;
    }

    /**
     * @param mixed $h_title
     */
    public function setHTitle($h_title)
    {
        $this->h_title = $h_title;
    }
}