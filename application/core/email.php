<?php
class Mail {

  private $from = "admin@alteza.by";
  private $from_name = "";
  private $type = "text/html";
  private $encoding = "utf-8";
  private $notify = false;
  private $to = []; // адреса для рассылки

  /* Изменение обратного e-mail адреса */
  public function setFrom($from) {
    $this->from = $from;
  }

  /* Изменение имени в обратном адресе */
  public function setFromName($from_name) {
    $this->from_name = $from_name;
  }

  /* Изменение типа содержимого письма */
  public function setType($type) {
    $this->type = $type;
  }

  /* Нужно ли запрашивать подтверждение письма */
  public function setNotify($notify) {
    $this->notify = $notify;
  }

  /* Изменение кодировки письма */
  public function setEncoding($encoding) {
    $this->encoding = $encoding;
  }

  public function setTo(array $adrs){
    $this->to = $adrs;
  }

  /* Метод отправки письма */
  public function send($subject, $message) {
    $from = "=?utf-8?B?".base64_encode($this->from_name)."?="." <".$this->from.">";
    $headers = "From: ".$from."\r\nReply-To: ".$from."\r\nContent-type: ".$this->type."; charset=".$this->encoding."\r\n";
    if ($this->notify) $headers .= "Disposition-Notification-To: ".$this->from."\r\n";
    $subject = "=?utf-8?B?".base64_encode($subject)."?=";

    foreach($this->to as $email){
      mail($email, $subject, $message, $headers);
    }
    return true;
  }

}