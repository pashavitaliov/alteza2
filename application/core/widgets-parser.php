<?php

class WidgetParser {
    private $widgets;

    public function __construct() {
        $this->widgets = DB::sqlQuery("SELECT pattern, widget FROM widgets WHERE enable = 1");
    }

    public function replacePatternsToWidgets($content) {
        $template = $content;
        foreach ($this->widgets as $widget) {
            $template = str_replace($widget['pattern'], $widget['widget'], $template);
        }

        return $template;
    }

    public static function parse($content) {
        $self = new self;
        return $self->replacePatternsToWidgets($content);
    }
}

// CREATE TABLE `widgets` ( `id` INT(255) NOT NULL AUTO_INCREMENT , `pattern` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , `widget` LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_unicode_ci;
// ALTER TABLE `widgets` ADD `enable` INT(1) NOT NULL DEFAULT '1' AFTER `widget`;