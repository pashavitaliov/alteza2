<section class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/news" itemprop="item"><span itemprop="name">Новости</span><meta itemprop="position" content="2"></a>
        </li>
    </ul>
</section>
<section class="articles-news articles_page">
        <div class="section-content">
            <div class="articles-news-wrapper clearfix">
                <div class="articles-short">
                    <h1 class="h1-title articles-h2-title">Новости компании</h1>
                    <ul class="articles-short-list">
                        <?php
                            foreach($data['news'] as $article){
                                echo "
                                    <li class=\"articles-short-item\"><a href=\"/news/{$article['id']}\" class=\"articles-short-content\" title='{$article['title']}'>
                                        <div class=\"articles-short-img\"><img src=\"/images/news/{$article['img']}\" alt=\"{$article['title']}\" title='{$article['title']}'></div>
                                        <div class=\"articles-short-time\">
                                            <time>{$article['date']}</time>
                                        </div>
                                        <div class=\"articles-short-title\">{$article['title']}</div>
                                    </a>
                                    </li>
                                ";
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
</section>
<section class="advantages t-border t-marg">
        <div class="section-content">
            <ul class="advantages-list">
                <li class="advantages-item advantages-number-one">
                    <span>Крупнейший производитель натяжных потолков в Беларуси</span>
                </li>
                <li class="advantages-item advantages-quality">
                    <span>Наше кредо - быстро, профессионально и безопасно</span>
                </li>
                <li class="advantages-item advantages-guarantee">
                    <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
                </li>
            </ul>
        </div>
</section>