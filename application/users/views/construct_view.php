<link rel="stylesheet" href="/css/farbtastic.css">
<div class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/construct" itemprop="item"><span itemprop="name">Конструктор потолков</span><meta itemprop="position" content="2"></a>
        </li>
    </ul>
</div>
<div class="content_wrapper">
    <h1><?php echo $data['pageTitle'];?></h1>
    <div class="common-page-content t-marg-sm"><?php echo $data['content'];?></div>
    <div class="construct_wrap">
        <div class="construct_images">
            <div class="constr_elem base">
                <img src="/images/2construct-base.png" id="floor">
            </div>
            <div class="constr_elem inter">
                <img src="/images/2construct-inter.png">
            </div>
            <div class="constr_elem inter">
                <img src="/images/2construct-inter2.png">
            </div>
            <div class="constr_elem inter">
                <img src="/images/1construct-inter3.png">
            </div>
            <div class="constr_elem ">
                <img src="/images/2construct-top.png">
            </div>
            <canvas id="c-floor" height="746" class="constr_elem"></canvas>
            <canvas id="c-wall-main" height="746" class="constr_elem"></canvas>
            <canvas id="c-wall-el1" height="746" class="constr_elem"></canvas>
            <canvas id="c-wall-el2" height="746" class="constr_elem"></canvas>
            <canvas id="c-ceiling" height="746" class="constr_elem"></canvas>
            <div class="constr_elem constr_controls">
                <div class="constr_controls_elem color_wid overflow" data-constr-elem ="walls">
                    <div class="controls_content_wrap">
                        <div class="controls_elem_title">
                            Цвет стен
                        </div>
                        <div class="controls_elem_content">
                            <div class="controls_elem_content_color walls_color"></div>
                        </div>
                    </div>
                    <div class="constr_controls_arrow"></div>
                </div>
                <div class="constr_controls_elem color_wid overflow" data-constr-elem ="floor">
                    <div class="controls_content_wrap">
                        <div class="controls_elem_title">
                            Цвет пола
                        </div>
                        <div class="controls_elem_content">
                            <div class="controls_elem_content_color floor_color"></div>
                        </div>
                    </div>
                    <div class="constr_controls_arrow"></div>
                </div>
                <div class="constr_controls_elem texture_wid overflow">
                    <div class="controls_content_wrap">
                        <div class="controls_elem_title">
                            Фактура пленки
                        </div>
                        <div class="controls_elem_content constr_material_title">
                            Мат
                        </div>
                    </div>
                    <div class="constr_controls_arrow"></div>
                </div>
                <div class="constr_controls_elem overflow ceil_color_wid">
                    <div class="controls_content_wrap">
                        <div class="controls_elem_title">
                            Цвет потолка
                        </div>
                        <div class="controls_elem_content">
                            <div class="controls_elem_content_color"></div>
                        </div>
                    </div>
                    <div class="constr_controls_arrow"></div>
                </div>
                <!--<div class="constr_controls_elem overflow constr_count_price">
                    Рассчитать стоимость
                </div>-->
                <div class="constr_controls_elem overflow constr_calc">
                    <!--<div class="controls_content_wrap">
                        <div class="controls_elem_title">
                            Цена потолка
                        </div>
                        <div class="controls_elem_content constr_material_title">
                            от 9,9 б.р./м2
                        </div>
                    </div>
                    <div class="constr_calc_form">
                        <form class="overflow">
                            <div class="input_wrap">
                                <label for="square">Площадь</label>
                                <input type="text" name="square" id="square"/>
                            </div>
                            <div class="input_wrap">
                                <label for="perimeter">Периметр</label>
                                <input type="text" name="perimeter" id="perimeter"/>
                            </div>
                        </form>
                    </div>
                    <div class="controls_content_wrap">
                        <div class="controls_elem_title">
                            Итого
                        </div>
                        <div class="controls_elem_content constr_material_title">
                            25.75 б.р.
                        </div>
                    </div>-->
                    <div class="constr_controls_elem overflow constr_count_price order_call">
                        Заказать
                    </div>
                </div>

                <div class="colors"></div>

                <div class="textures_list">
                    <div class="texture_list_el" data-id="1">
                        Мат
                    </div>
                    <div class="texture_list_el" data-id="2">
                        Глянец
                    </div>
                    <div class="texture_list_el" data-id="4">
                        Сатин
                    </div>
                    <div class="close_widget"></div>
                </div>

                <div class="ceil_colors_wrap">
                    <div class="ceil_colors"></div>
                    <div class="color_desc">
                        <div class="current_color"></div>
                        <div class="current_color_title"></div>
                    </div>
                    <div class="close_widget"></div>
                </div>

                <div id="color_val"></div>
            </div>
        </div>

    </div>
</div>

<script src="/js/farbtastic.js"></script>
<script src="/js/constructor.js"></script>