<section class="advantages">
        <div class="section-content">
            <ul class="advantages-list">
                <li class="advantages-item advantages-number-one">
                    <span>Крупнейший производитель натяжных потолков в Беларуси</span>
                </li>
                <li class="advantages-item advantages-quality">
                    <span>Наше кредо - быстро, профессионально и безопасно</span>
                </li>
                <li class="advantages-item advantages-guarantee">
                    <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
                </li>
            </ul>
        </div>
</section>
<section class="info-slider-panel">
        <div class="section-content">
            <div class="info-slider-wrapper">
                <ul class="info-slider">
                    <?php
                        foreach($data["actions"] as $action){
                            ($action["img"] == "0") ? $content = $action["slide"] : $content = '<div class="info-slider-img"><img src="/images/actions/'.$action["img"].'" alt="'.$action["title"].'" title="'.$action["title"].'"></div>';
                            echo "
                                <li class=\"info-slider-item\">
                                    <a href='/actions' title='{$action['title']}'>
                                        {$content}
                                    </a>
                                </li>
                            ";
                        }
                    ?>
                </ul>
            </div>
        </div>
</section>
<section class="services">
        <div class="section-content">
            <ul class="services-list">
                <li class="services-item services-constructor">
                    <a href="/construct" class="services-item-container">
                        <p class="services-title">Конструктор потолков</p>
                        <div class="services-decription">
                            <span class="services-description-text">Спроектируйте потолок своей мечты прямо у нас на сайте, а затем закажите его себе</span>
                        </div>
                    </a>
                </li>
                <li class="services-item services-price">
                    <a href="/gallery/matovi_natiajnoy_potolok" class="services-item-container">
                        <p class="services-title">Фото проектов</p>
                        <div class="services-decription">
                            <span class="services-description-text">Фотографии проектов компании Alteza</span>
                        </div>
                    </a>
                </li>
                <li class="services-item services-shares">
                    <a href="/actions" class="services-item-container">
                        <p class="services-title">Акции и скидки</p>
                        <div class="services-decription">
                            <span class="services-description-text">У нас действует программа скидок, которая поможет существенно сэкономить при заказе натяжного потолка</span>
                        </div>
                    </a>
                </li>
            </ul>

        </div>
</section>
<section class="strong-five">
        <div class="section-content">
            <div class="strong-five-wrapper clearfix">
                <h2 class="h2-title">Сильная пятерка Alteza</h2>
                <p class="h2-subtitle">Мы создаем потолки, которые выходят за рамки стандартных схем <br>и заурядных замыслов.</p>
                <div class="strong-five-video" id="strong-five-video">
                    <video id="video" width="100%" preload="auto" controls>
                        <source src="/video/video.mp4">
                    </video>
                    <div id="video_pause"></div>
                </div>
                <div class="strong-five-advantages">
                    <ul class="strong-five-list">
                        <li class="strong-five-item">
                            <div class="strong-five-icon">1</div>
                            <div class="strong-five-text">Собственное производство</div>
                        </li>
                        <li class="strong-five-item">
                            <div class="strong-five-icon">2</div>
                            <div class="strong-five-text">Качественные материалы</div>
                        </li>
                        <li class="strong-five-item">
                            <div class="strong-five-icon">3</div>
                            <div class="strong-five-text">Лучшие системы профилей</div>
                        </li>
                        <li class="strong-five-item">
                            <div class="strong-five-icon">4</div>
                            <div class="strong-five-text">Монтаж</div>
                        </li>
                        <li class="strong-five-item">
                            <div class="strong-five-icon">5</div>
                            <div class="strong-five-text">25 лет гарантии</div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
</section>
<section class="call-to-action">
        <div class="section-content">
            <div class="cta-content">
                <div class="cta-phones">
                    <span class="cta-phones-text">Связаться с отделом продаж для бесплатной консультации</span>
                    <ul class="cta-phones-list">
                        <li class="cta-phones-item"><a href="tel:+375333333006" class="ga_ym_t">+375 33 33-33-006 (МТС) </a></li>
                        <li class="cta-phones-item"><a href="tel:+375293333006" class="ga_ym_t">+375 29 33-33-006 (Velcom)</a></li>
                    </ul>
                </div>
                <div class="cta-separator">
                    <span class="cta-separator-text">или</span>
                </div>
                <div class="cta-button">
                    <button class="btn red-btn cta-btn order_call ga_ym_zf">Заказать звонок</button>
                </div>
            </div>
        </div>
</section>
<section class="plenki">
        <div class="section-content">
            <h2 class="h2-title">Оригинальные потолочные пленки</h2>
            <div class="p h2-subtitle">Мы создаем потолки, которые выходят за рамки стандартных схем <br>и заурядных замыслов.</div>
            <ul class="plenki-list">

                <?php
                    foreach($data['materials'] as $key => $material){
                        ($material['new'] == 1) ? $new = "plenki-title-new" : $new = "plenki-title";
                        ($key == 0 || $key == 6) ? $xl = "plenki-xl" : $xl = "";
                        echo "
                            <li class=\"plenki-item {$xl}\">
                                <a href=\"/catalog/{$material['url_mask']}\" class=\"plenki-content\" title='{$material['main_title']}'>
                                    <div class=\"plenki-img\">
                                        <img src=\"/images/materials/main-page/{$material['img']}\" alt=\"{$material['main_title']}\" title='{$material['main_title']}'>
                                    </div>
                                    <div class=\"plenki-title-wrap\">
                                        <div class=\"{$new}\">
                                            <span>{$material['title']}</span>
                                        </div>
                                    </div>
                                    <div class=\"plenki-zoom\">
                                        <span>Быстрый просмотр</span>
                                    </div>
                                </a>
                            </li>
                        ";
                    }
                ?>


            </ul>
        </div>
</section>
<section class="profiles">
        <div class="section-content">
            <h2 class="h2-title">Уникальные профили</h2>
            <p class="h2-subtitle">Мы создаем потолки, которые выходят за рамки стандартных схем <br>и заурядных замыслов.</p>
            <div class="profiles-slider-wrapper">
                <div class="profiles-slider-nav">
                    <ul class="ps-nav-list">
                        <?php
                            foreach($data["systems"] as $key=>$syst){
                                ($key == 0) ? $active = 'ps-nav-active' : $active = "";
                                echo "<li class=\"ps-nav-item {$active}\" data-pagination-el='el-{$syst['id']}'><span class=\"ps-nav-text\">{$syst['title']}</span></li>";
                            }
                        ?>
                    </ul>
                </div>
                <ul class="profiles-slider img-width">
                    <?php
                    foreach($data["systems"] as $syst){

                        if($syst['video'] != '0') {
                            $href = "/video/{$syst['video']}";
                            $class = "video-zoom";
                        }
                        else{
                            $href = "#";
                            $class = "no-active";
                        }

                        echo "
                                <li class=\"profiles-slider-item\" data-pagination='el-{$syst['id']}'>
                                    <a href=\"/catalog/sistemy_kreplenija\" class=\"profiles-slider-img\" title='{$syst['title']}' style=\"background-image: url(/images/systems/{$syst['main_img']}) \"></a>
                                    ".
                            "<a href=\"{$href}\" class=\"profiles-slider-video {$class}\" title='{$syst['title']}'>
                                                <div class=\"ps-video-info\">
                                                    <div class=\"ps-video-title\">Посмотреть видео наглядно демонстрирующее монтаж системы {$syst['title']}</div>
                                                    <div class=\"ps-video-time\">2:34</div>
                                                </div>
                                        </a>"
                            ."
                                    <div class=\"profiles-slider-description\"><p>{$syst['preview']}</p></div>
                                </li>
                            ";

//                        echo "
//                                <li class=\"profiles-slider-item\" data-pagination='el-{$syst['id']}'>
//                                    <a href=\"/catalog/sistemy_kreplenija\" class=\"profiles-slider-img\" title='{$syst['title']}'><img src=\"/images/systems/{$syst['main_img']}\" alt=\"{$syst['title']}\" title='{$syst['title']}'></a>
//                                    ".
//                                    "<a href=\"{$href}\" class=\"profiles-slider-video {$class}\" title='{$syst['title']}'>
//                                                <div class=\"ps-video-info\">
//                                                    <div class=\"ps-video-title\">Посмотреть видео наглядно демонстрирующее монтаж системы {$syst['title']}</div>
//                                                    <div class=\"ps-video-time\">2:34</div>
//                                                </div>
//                                        </a>"
//                                    ."
//                                    <div class=\"profiles-slider-description\"><p>{$syst['preview']}</p></div>
//                                </li>
//                            ";
                    }
                    ?>

                </ul>
            </div>
            <div class="center-btn">
                <a href="/catalog/sistemy_kreplenija" class="btn small-btn red-btn">Посмотреть все</a>
            </div>
        </div>
</section>
<section class="call-to-action">
        <div class="section-content">
            <div class="cta-content">
                <div class="cta-phones">
                    <span class="cta-phones-text">Связаться с отделом продаж для бесплатной консультации</span>
                    <ul class="cta-phones-list">
                        <li class="cta-phones-item"><a href="tel:+375333333006" class="ga_ym_t">+375 33 33-33-006 (МТС) </a></li>
                        <li class="cta-phones-item"><a href="tel:+375293333006" class="ga_ym_t">+375 29 33-33-006 (Velcom)</a></li>
                    </ul>
                </div>
                <div class="cta-separator">
                    <span class="cta-separator-text">или</span>
                </div>
                <div class="cta-button">
                    <button class="btn red-btn cta-btn order_call ga_ym_zf">Заказать звонок</button>
                </div>
            </div>
        </div>
</section>
<div class="services-info">
  <h1 class="h1-title">Натяжные потолки в Минске от Alteza</h1>
  <p class="h1-subtitle">Мы создаем потолки, которые выходят за рамки стандартных схем <br>и заурядных замыслов.</p>
  <div class="services-info-columns">
    <div class="services-info-columns-item">
      <p>Натяжные потолки ALTEZA - это индивидуальный дизайн и отменное качество, разнообразие цветов и фактур. Наши дизайнеры воплотят в жизнь самые смелые и неожиданные идеи, подчеркнут ваш уникальный стиль, а также профессионально проконсультируют.</p>
      <p> Мы предлагаем натяжные потолки под ключ, которые не требуют дополнительных монтажных операций. Благодаря команде специалистов высочайшего уровня мы создаём интерьерные шедевры.</p>
    </div>
    <div class="services-info-columns-item">
      <p>Как лидеры рынка натяжных потолков, мы предлагаем самый широкий в стране ассортимент, отменное качество и кратчайшие сроки исполнения заказов. На сегодняшний день, купить натяжные потолки в Минске и в регионах не составит труда, поскольку у нас разветвлённая сеть филиалов.</p>
      <p>Мы уделяем огромное внимание поддержанию ассортимента на наших складах, что позволяет значительно сократить сроки изготовления натяжных потолков и сделать весь процесс максимально комфортным для заказчика. Собственное производство ALTEZA одно из самых крупных в ЕС.</p>
      <p>Мы осуществляем поставки комплектующих для натяжных потолков напрямую из Германии, Франции и Юго-Восточной Азии в больших объёмах и поэтому можем предложить самые выгодные цены для наших клиентов. </p>
    </div>
    <div class="services-info-columns-item">
      <p>Все составляющие наших натяжных потолков идеально подогнаны друг под друга. Мы тщательно контролируем все этапы производства.</p><p> ALTEZA - это качественные и разнообразные варианты натяжных потолков, которые удовлетворят даже самых искушенных клиентов.</p>
      <p>Мы гордимся тем, что мы делаем. Если вы хотите создать свой собственный стиль в премиум исполнении, вам нужны натяжные потолки ALTEZA.</p>
    </div>
  </div>
</div>
<section class="articles-news">
        <div class="section-content">
            <div class="articles-news-wrapper clearfix">
                <div class="articles-short">
                    <h2 class="h2-title articles-h2-title">Полезные статьи</h2>
                    <a href="/articles" class="articles-show-more">Показать все</a>
                    <ul class="articles-short-list">

                        <?php
                            foreach($data['articles'] as $article){
                                echo "
                                    <li class=\"articles-short-item\"><a href=\"/articles/{$article['url']}\" class=\"articles-short-content\" title='{$article['title']}'>
                                        <div class=\"articles-short-img\"><img src=\"/application-administrator/public/img/articles/{$article['img']}\" alt=\"{$article['title']}\" title='{$article['title']}'></div>
                                        <div class=\"articles-short-time\">
                                            <time>{$article['date']}</time>
                                        </div>
                                        <div class=\"articles-short-title\">{$article['title']}</div>
                                    </a>
                                    </li>
                                ";
                            }
                        ?>

                    </ul>
                </div>
                <div class="news-short">
                    <a href="/news" class="h2-title news-h2-title">Новости компании</a>
                    <ul class="news-short-list">


                        <?php
                            foreach($data['news'] as $n){
                                echo "
                                    <li class=\"news-short-item\">
                                        <a href=\"/news/{$n['id']}\" class=\"news-short-content\">
                                            <div class=\"news-short-time\">{$n['date']}</div>
                                            <div class=\"news-short-title\">{$n['title']}</div>
                                        </a>
                                    </li>
                                ";
                            }
                        ?>

                    </ul>
                </div>
            </div>
        </div>
</section>
<!--<section class="about-us">
        <div class="section-content">
            <h2 class="h2-title about-us-title">О компании</h2>
            <div class="about-us-content clearfix">
                <div class="about-us-text">
                    <p><strong>ALTEZA - стремительно развивающаяся компания, которая ставит своей задачей завоевание лидирующих позиций на рынке натяжных потолков. И это - не просто слова.</strong></p>
                    <br>
                    <p>История компании «Алтеза» не отличается крутыми поворотами, взлетами и падениями: это хорошо продуманный и короткий путь к честному и заслуженному успеху. Решив заняться производством натяжных потолков, основатели «Алтезы» четко просчитали оптимальный путь развития бизнеса: от простого – к сложному, от банального монтажа импортной пленки – к собственному уникальному производству, основанному на оригинальных технических решениях и позволяющему гибко реагировать на запросы рынка. Такой подход к делу помог выполнять заказы удивительно быстро, а главное – искусно воплощать в жизнь любые, даже самые незаурядные и изысканные, идеи клиентов.</p>
                    <br>
                    <p>Годом рождения новой компании стал 2005 – год выпуска первого собственного потолка, а сутью философии новорожденного бренда Alteza (от итал. altezza - «высота») – всегда и везде быть на высоте, недосягаемой для конкурентов.</p>
                </div>
                <div class="about-us-img">
                    <div class="about-us-img-content">
                        <img src="/images/about-us-img.jpg" alt="">
                        <p>ЛУЧШАЯ СТРОИТЕЛЬНАЯ КОНСТРУКЦИЯ (СИСТЕМА) ГОДА</p>
                    </div>
                </div>
            </div>
        </div>
</section>-->
<section class="about-us">
        <div class="section-content">
            <h2 class="h2-title about-us-title">О компании</h2>
            <div class="about-us-content clearfix">
                <div class="common-page-content"><?php echo $data['content'];?></div>
            </div>
        </div>
</section>
<section class="advantages t-border">
        <div class="section-content">
            <ul class="advantages-list">
                <li class="advantages-item advantages-number-one">
                    <span>Крупнейший производитель натяжных потолков в Беларуси</span>
                </li>
                <li class="advantages-item advantages-quality">
                    <span>Наше кредо - быстро, профессионально и безопасно</span>
                </li>
                <li class="advantages-item advantages-guarantee">
                    <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
                </li>
            </ul>
        </div>
</section>
<script src="/js/video-settings.js"></script>
<script src="/js/trianzoom-video.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".video-zoom").trianZoomVideo();
    });
</script>