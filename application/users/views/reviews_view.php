<div class="section-content">
    <div class="main_nav overflow">
<!--        <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
        <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
            <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
            </li>
            <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                <a href="/reviews" itemprop="item"><span itemprop="name">Отзывы</span><meta itemprop="position" content="2"></a>
            </li>
        </ul>
    </div>
    <div class="box">
    	<div class="product-title">
            <h1><?php echo $data['pageTitle'];?></h1>
        </div>
        <div class="reviews">

            <div class="create-review">
                <form method="post" action="/reviews/create" class="create-review-form">
                    <div class="review-form__wrap">
                        <label class="block">
                            <input type="text" class="review-form__field" name="name" placeholder="Имя" required/>
                        </label>
                    </div>

                    <div class="review-form__wrap">
                        <label class="block">
                            <textarea class="review-form__field review-form__textarea" name="review" placeholder="Текст отзыва" required></textarea>
                        </label>
                    </div>

                    <div class="review-form__wrap">
                        <label class="block">
                            <input type="submit" class="review-form__submit" value="Отправить"/>
                        </label>
                    </div>
                </form>
            </div>

            <div class="reviews-list">
                <?php foreach($data['reviews'] as $review) { ?>

                   <div class='review'>
                        <div>
                            <div class='review__name'>
                                <?php echo $review['name']; ?>
                            </div>
                            <div class='review__info'>
                                <div class='review__date'>
                                    <?php echo $review['created_at']; ?>
                                </div>
                            </div>
                            <div class='review__text'>
                                <?php echo $review['review']; ?>
                            </div>
                        </div>
                        <div class='comments'>
                            <?php foreach($review['comments'] as $comment) { ?>
                                <?php if (isset($comment)) { ?>
                                    <div class="comment">
                                        <div class='review__name comment__name'>
                                            <span class="red_text">Ответ от:</span> <?php echo $comment['comment_author']; ?>
                                        </div>
                                        <div class='review__info'>
                                            <div class='review__date'>
                                                <?php echo $comment['created_at']; ?>
                                            </div>
                                        </div>
                                        <div class='review__text'>
                                            <?php echo $comment['comment']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>

                <?php } ?>
            </div>
        </div>
    </div>
</div>
<section class="advantages t-border">
    <div class="section-content">
        <ul class="advantages-list">
            <li class="advantages-item advantages-number-one">
                <span>Крупнейший производитель натяжных потолков в Беларуси</span>
            </li>
            <li class="advantages-item advantages-quality">
                <span>Наше кредо - быстро, профессионально и безопасно</span>
            </li>
            <li class="advantages-item advantages-guarantee">
                <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
            </li>
        </ul>
    </div>
</section>
