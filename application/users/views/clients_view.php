<div class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/clients" itemprop="item"><span itemprop="name">Клиенты</span><meta itemprop="position" content="2"></a>
        </li>
    </ul>
</div>
<div class="content_wrapper">
    <h1><?php echo $data['pageTitle'];?></h1>
<!--    <p class="clients_desc">-->
<!--        ALTEZA гарантирует всестороннюю поддержку своих дилеров, независимо от географического расположения. Мы уверяем: работать с ALTEZA легко и перспективно. Каждый день мы стараемся создать новые возможности для развития вашего бизнеса. Ведь мы понимаем: успех у нас общий.лков любых форм несколькими измерениями и отправлять их в производство непосредственно из самой программы (не нужно использовать другие почтовые программы и сервисы).-->
<!--    </p>-->
    <div class="common-page-content t-marg-sm"><?php echo $data['content'];?></div>
    <div class="clients_wrap overflow t-marg-sm">
        <?php
            foreach($data['clients'] as $d){
                echo "
                    <div class=\"clients_grid_el\">
                        <img src=\"/images/clients/{$d['img']}\">
                        <div class=\"client_content_wrap\">
                            <h3 class=\"client_name\">
                                {$d['name']}
                            </h3>
                            <p class=\"client_content\">
                                {$d['content']}
                            </p>
                        </div>
                    </div>
                ";
            }
        ?>
    </div>
</div>