<div class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/sertificates" itemprop="item"><span itemprop="name">Сертификаты</span><meta itemprop="position" content="2"></a>
        </li>
    </ul>
</div>
<div class="content_wrapper">
    <h1><?php echo $data['pageTitle'];?></h1>
    <div class="common-page-content t-marg-sm"><?php echo $data['content'];?></div>
    <div class="content-certificates overflow">
<!--        <p class="foreword">-->
<!--            ALTEZA гарантирует всестороннюю поддержку своих дилеров, независимо от географического расположения. Мы уверяем: работать с ALTEZA легко и перспективно. Каждый день мы стараемся создать новые возможности для развития вашего бизнеса. Ведь мы понимаем: успех у нас общий.лков любых форм несколькими измерениями и отправлять их в производство непосредственно из самой программы (не нужно использовать другие почтовые программы и сервисы).-->
<!--        </p>-->
        <div class="certificates-wrapper overflow">
            <?php
                foreach($data['sertificates'] as $d){
                    echo "
                        <div class=\"grid-el overflow\">
                            <div class=\"cerf-img\">
                                <a href=\"/images/sertificates/{$d['img']}\" class='show-img'>
                                    <img src=\"/images/sertificates/{$d['img']}\" title=\"{$d['title']}\" alt=\"{$d['title']}\">
                                </a>
                            </div>
                            <div class=\"cerf-description\">
                                <p class=\"cerf-desc-title\">
                                   {$d['title']}
                                </p>
                                <p class=\"cerf-desc-content\">
                                    {$d['content']}
                                </p>
                            </div>
                        </div>
                    ";
                }
            ?>
        </div>
    </div>
</div>