<div class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/calc" itemprop="item"><span itemprop="name">Калькулятор</span><meta itemprop="position" content="2"></a>
        </li>
    </ul>
</div>
<div ng-app="calc" class="calc-wrap main-content">
	<div class="product-title">
		<h1><?php echo $data['pageTitle'];?></h1>
	</div>
  <div class="common-page-content t-marg-sm"><?php echo $data['content'];?></div>
  <div ng-controller="CalculatorController as vm" class="calc">
    <form class="calc__form" name="vm.CalculatorForm" novalidate ng-if="vm.isLoad" ng-submit="vm.sendToCalculate()">
      <p class="calc__note calc__note--offer" ng-if="vm.calculatorData.offer">Закажи обмер в субботу или воскресенье и получи скидку 10%</p>
      <div class="calc__room" ng-repeat="room in vm.rooms">
        <button ng-if="vm.rooms.length > 1" class="calc__button calc__button--remove" type="button" ng-click="vm.removeRoom(room)">Удалить помещение</button>
        <div class="calc__wrap">
        <div class="calc__ceilings-title">
          Выберите тип пленки:
        </div>
          <ul class="calc__ceilings-list">
            <li class="calc__ceiling-item"
                ng-repeat="ceiling in vm.calculatorData.ceilings"
                ng-click="vm.setSelectedCeiling($parent.$index, ceiling.id)"
                ng-class="{'calc__ceiling-item--selected': (room.ceilingId == ceiling.id)}">
              <span class="calc___ceiling-text">{{ceiling.title}}</span>
              <!-- <p class="calc___ceiling-img">{{ceiling.img}}</p> -->
              <!--<p class="calc___ceiling-text">{{ceiling.price | number : 2}} р.</p>-->
            </li>
          </ul>
          <p class="calc__error" ng-if="!room.ceilingId && vm.CalculatorForm.$submitted">Пожалуйста, выберите нужный тип пленки</p>
        </div>
        <div class="calc__wrap" ng-if="room.ceilingId">
          <div class="calc__input-wrap">
            <label class="calc__label" for="square_{{$index}}">Площадь потолка, м2</label>
            <input
              id="square_{{$index}}"
              type="text"
              name="square_{{$index}}"
              class="calc__input"
              ng-model="room.square"
              ng-pattern="vm.patternForNumber"
              ng-class="{'calc__input--has-error': vm.CalculatorForm['square_' + $index].$invalid && (vm.CalculatorForm['square_' + $index].$dirty || vm.CalculatorForm.$submitted)}"
              required>
              <div class="calc__errors" ng-messages="vm.CalculatorForm['square_' + $index].$error" ng-if="(vm.CalculatorForm['square_' + $index].$dirty || vm.CalculatorForm.$submitted)">
                <div class="calc__error" ng-message="required">Поле обязательно для заполнения</div>
                <div class="calc__error" ng-message="pattern">Неправильно заполнено поле</div>
              </div>
          </div>
          <div class="calc__input-wrap">
            <label class="calc__label" for="quantityOfAngle_{{$index}}">Количество углов, шт</label>
            <input
              id="quantityOfAngle_{{$index}}"
              type="text"
              name="quantityOfAngle_{{$index}}"
              class="calc__input"
              ng-model="room.quantityOfAngle"
              ng-pattern="vm.patternForNumber"
              ng-class="{'calc__input--has-error': vm.CalculatorForm['quantityOfAngle_' + $index].$invalid && (vm.CalculatorForm['quantityOfAngle_' + $index].$dirty || vm.CalculatorForm.$submitted)}"
              required>
              <div class="calc__errors" ng-messages="vm.CalculatorForm['quantityOfAngle_' + $index].$error" ng-if="(vm.CalculatorForm['quantityOfAngle_' + $index].$dirty || vm.CalculatorForm.$submitted)">
                <div class="calc__error" ng-message="required">Поле обязательно для заполнения</div>
                <div class="calc__error" ng-message="pattern">Неправильно заполнено поле</div>
              </div>
            </div>
        </div>
        <div class="calc__wrap" ng-if="room.square && room.quantityOfAngle">
          <div class="calc__input-wrap" ng-repeat="additional in vm.calculatorData.basic_operations">
            <label class="calc__label" for="room_{{$index}}_{{additional.id}}">{{additional.title}}</label>
            <input
              id="room_{{$index}}_{{additional.id}}"
              type="text"
              name="room_{{$index}}_{{additional.id}}"
              class="calc__input"
              ng-model="room.additionals[additional.id]"
              ng-pattern="vm.patternForNumber"
              ng-class="{'calc__input--has-error': vm.CalculatorForm['room_' + $index + '_' + additional.id].$invalid}">
            <div class="calc__errors" ng-messages="vm.CalculatorForm['room_' + $index + '_' + additional.id].$error">
              <div class="calc__error" ng-message="pattern">Неправильно заполнено поле</div>
            </div>
          </div>
        </div>
      </div>
      <div class="calc__wrap">
        <button class="calc__button calc__button--add" type="button" ng-click="vm.addRoom()">Добавить помещение</button>
        <button class="calc__button calc__button--send" type="submit">Расчитать стоимость</button>
      </div>
    </form>
    <div class="calc__wrap" ng-if="vm.totalPrice">
      <p class="calc__text calc__total">Итоговая стоимость: <span class="red_text_theme">{{vm.totalPrice.value | number : 2}}</span> р.</p>
      <p class="calc__note calc__error calc__note--minprice" ng-if="!vm.totalPrice.isSatisfied">Минимальная сумма заказа 180.00 р.</p>
      <form class="calc__form" name="vm.ContactForm" novalidate ng-submit="vm.order.make()" ng-if="vm.totalPrice.isSatisfied">
        <p class="calc__note calc__note--offer">Закажите обмер прямо сейчас и получите скидку <span class="red_text_theme">5%</span></p>
        <div class="calc__input-wrap" ng-if="!vm.order.received">
          <label class="calc__label" for="name">Ваше имя</label>
          <input
            id="name"
            type="text"
            name="name"
            class="calc__input"
            ng-model="vm.contacts.name"
            ng-class="{'calc__input--has-error': vm.ContactForm.name.$invalid && (vm.ContactForm.name.$dirty || vm.ContactForm.$submitted)}"
            required>
          <div class="calc__errors" ng-messages="vm.ContactForm.name.$error" ng-if="(vm.ContactForm.name.$dirty || vm.ContactForm.$submitted)">
            <div class="calc__error" ng-message="required">Поле обязательно для заполнения</div>
          </div>
        </div>
        <div class="calc__input-wrap" ng-if="!vm.order.received">
          <label class="calc__label" for="phone">Ваш телефон</label>
          <input
            id="phone"
            type="text"
            name="phone"
            class="calc__input"
            ng-model="vm.contacts.phone"
            ng-class="{'calc__input--has-error': vm.ContactForm.phone.$invalid && (vm.ContactForm.phone.$dirty || vm.ContactForm.$submitted)}"
            required>
          <div class="calc__errors" ng-messages="vm.ContactForm.phone.$error" ng-if="(vm.ContactForm.phone.$dirty || vm.ContactForm.$submitted)">
            <div class="calc__error" ng-message="required">Поле обязательно для заполнения</div>
          </div>
        </div>
        <div class="calc__wrap" ng-if="!vm.order.received">
          <button class="calc__button calc__button--send ga_ym_zo" type="submit">Заказать обмер</button>
        </div>
        <p class="calc__note calc__note--offer-success" ng-if="vm.order.received">
          Спасибо, ваш заказ был <span class="red_text_theme">получен</span>! Наш менеджер свяжется с вами в ближайшее время.
        </p>
      </form>
    </div>
    <div class="spinner" ng-if="!vm.isLoad"></div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular-messages.js"></script>
<script src="/js/calc/calc.module.js"></script>
<script src="/js/calc/calc.controller.js"></script>
<script src="/js/calc/calc.service.js"></script>
