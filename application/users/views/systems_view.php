<div class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/catalog/matovi_natiajnoy_potolok" itemprop="item"><span itemprop="name">Каталог</span><meta itemprop="position" content="2"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/catalog/sistemy_kreplenija" itemprop="item"><span itemprop="name">Системы крепления</span><meta itemprop="position" content="3"></a>
        </li>
    </ul>
</div>
<div class="profiles-slider-nav">
    <ul class="ps-nav-list">
        <li class="ps-nav-item ps-nav-flag"><a href="/catalog/matovi_natiajnoy_potolok" class="ps-nav-text">Пленки</a></li>
        <li class="ps-nav-item ps-nav-flag ps-nav-active"><a href="" class="ps-nav-text">Системы крепления</a></li>
    </ul>
</div>
<div class="content_wrapper">
    <h1><?php echo $data['pageTitle'];?></h1>
    <div class="common-page-content t-marg-sm"><?php echo $data['content'];?></div>
    <div class="content-certificates overflow">
        <div class="profiles-slider-wrapper-inner">
            <div class="profiles-slider-nav">
                <ul class="ps-nav-list">
                    <?php
                        foreach($data["systems"] as $key=>$syst){
                            ($key == 0) ? $active = 'ps-nav-active' : $active = "";
                            echo "<li class=\"ps-nav-item {$active}\" data-pagination-el='el-{$syst['id']}'><span class=\"ps-nav-text\">{$syst['title']}</span></li>";
                        }
                    ?>
                </ul>
            </div>
            <ul id="profiles-slider">
                <?php
                    foreach($data["systems"] as $syst){
                        if($syst['video'] != '0') {
                            $href = "/video/{$syst['video']}";
                            $class = "video-zoom";
                        }
                        else{
                            $href = "#";
                            $class = "no-active";
                        }

//                        echo "
//                            <li class=\"profiles-slider-item\" data-pagination='el-{$syst['id']}'>
//                                <div class=\"profiles-slider-img\"><img src=\"/images/systems/{$syst['main_img']}\" alt=\"{$syst['title']}\" title='{$syst['title']}'></div>
//                                ".
//                                "<a href=\"{$href}\" class=\"profiles-slider-video {$class}\" title='{$syst['title']}'>
//                                        <div class=\"ps-video-info\">
//                                            <div class=\"ps-video-title\">Посмотреть видео наглядно демонстрирующее монтаж системы {$syst['title']}</div>
//                                            <div class=\"ps-video-time\">2:34</div>
//                                        </div>
//                                </a>"
//                                ."<div class=\"profiles-slider-description overflow\">
//                                    <div class=\"profiles-inter-img\">
//                                        <a href=\"/images/systems/inter/{$syst['inter_img']}\" title=\"Система крепления натяжных потолков {$syst['title']} в итерьере\" class='show-img'>
//                                            <img src=\"/images/systems/inter/{$syst['inter_img']}\" alt=\"Система крепления натяжных потолков {$syst['title']} в итерьере\" title=\"Система крепления натяжных потолков {$syst['title']} в итерьере\">
//                                        </a>
//                                    </div>
//                                    <div class=\"profiles-description\">
//                                        <p>
//                                            {$syst['content']}
//                                        </p>
//                                    </div>
//                                </div>
//                            </li>
//                        ";

                        echo "
                            <li class=\"profiles-slider-item\" data-pagination='el-{$syst['id']}'>
                                <div class=\"profiles-slider-img\"><img src=\"/images/systems/{$syst['main_img']}\" alt=\"{$syst['title']}\" title='{$syst['title']}'></div>
                                ".
                            "<a href=\"{$href}\" class=\"profiles-slider-video {$class}\" title='{$syst['title']}'>
                                        <div class=\"ps-video-info\">
                                            <div class=\"ps-video-title\">Посмотреть видео наглядно демонстрирующее монтаж системы {$syst['title']}</div>
                                            <div class=\"ps-video-time\">2:34</div>
                                        </div>
                                </a>"
                            ."<div class=\"profiles-slider-description overflow\">
                                    <div class=\"profiles-description\">
                                        <p>
                                            {$syst['content']}
                                        </p>
                                    </div>
                                </div>
                            </li>
                        ";
                    }
                ?>
            </ul>
        </div>
    </div>
</div>
<div class="advantages t-border t-marg">
    <div class="section-content">
        <ul class="advantages-list">
            <li class="advantages-item advantages-number-one">
                <span>Крупнейший производитель натяжных потолков в Беларуси</span>
            </li>
            <li class="advantages-item advantages-quality">
                <span>Наше кредо - быстро, профессионально и безопасно</span>
            </li>
            <li class="advantages-item advantages-guarantee">
                <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
            </li>
        </ul>
    </div>
</div>
<script src="/js/trianslider.js"></script>
<script src="/js/trianzoom-video.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#profiles-slider").trianSlider();
        $(".video-zoom ").trianZoomVideo();
    });
</script>