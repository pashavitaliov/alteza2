<div class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow">
        <li><a href="/">Главная</a></li>
        <li><a href="/clients">Клиенты</a></li>
    </ul>
</div>
<div class="content_wrapper">
    <h1>Дорогие друзья!</h1>
    <div class="clients_wrap overflow t-marg-sm">
        <div class="for-clients-img-wrap w_30 float_l">
            <img class="for-clients__image" src="/images/forclients.png">
        </div>
        <div class="for-clients-text-wrap w_70 float_r">
            <p>
                Компания ALTEZA поздравляет Вас с Новым годом и Рождеством. Мы желаем Вам всяческих успехов, здоровья и финансового благополучия.
            </p>
            <p>
                Наша компания дарит всем своим постоянным клиентам, а также их друзьям, скидку на натяжные потолки до <span class="red_text_theme">20%</span> или на дополнительные работы до <span class="red_text_theme">40%</span>.
            </p>
            <p>
                Подробности уточняйте у специалистов компании по телефонам:
            </p>
            <p class="red_text_theme">
                +375 33 33-33-006 (МТС)<br>
                +375 29 33-33-006 (VELCOM)
            </p>
            <p class="for-clients-text-links-container">
                <a href="/catalog" class="float_l red_text_theme">Каталог</a>
                <a href="/construct" class="float_r red_text_theme">Конструктор потолков</a>
            </p>
        </div>
    </div>
</div>
<div class="advantages t-border t-marg-sm">
    <div class="section-content">
        <ul class="advantages-list">
            <li class="advantages-item advantages-number-one">
                <span>Крупнейший производитель натяжных потолков в Беларуси</span>
            </li>
            <li class="advantages-item advantages-quality">
                <span>Наше кредо - быстро, профессионально и безопасно</span>
            </li>
            <li class="advantages-item advantages-guarantee">
                <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
            </li>
        </ul>
    </div>
</div>