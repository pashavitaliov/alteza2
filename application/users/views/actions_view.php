<div class="section-content">
    <div class="main_nav overflow">
<!--        <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
        <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
            <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
            </li>
            <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                <a href="/actions" itemprop="item"><span itemprop="name">Акции</span><meta itemprop="position" content="2"></a>
            </li>
        </ul>
    </div>
    <div class="box">
    	<div class="product-title">
            <h1><?php echo $data['pageTitle'];?></h1>
        </div>
        <div class="">
            <div class="stock-slider">
                <ul class="stock-slider-list clearfix" id="stock-slider">
                    <?php
                        foreach($data['actions'] as $action){
                            ($action["img"] == "0") ? $content = $action["slide"] : $content = '<img src="/images/actions/'.$action["img"].'" alt="'.$action["title"].'" title="'.$action["title"].'">';
//                            echo "
//                                <li class=\"stock-slider-item\">
//                                    <div class=\"stock-slider-container\">{$content}</div>
//                                    <div class=\"stock-item\">
//                                        <h2 class=\"stock-item-title\">{$action['title']}</h2>
//                                        <div class=\"stock-item-container clearfix\">
//                                            {$action['content']}
//                                        </div>
//                                    </div>
//                                </li>
//                            ";
                            echo "
                                <li class=\"stock-slider-item\">
                                    <div class=\"stock-slider-container order_call\">{$content}</div>
                                </li>
                            ";

                        }
                    ?>
                </ul>
                <div class="common-page-content t-marg-sm"><?php echo $data['content']['content'];?></div>
            </div>
        </div>
    </div>
</div>
<section class="advantages t-border">
    <div class="section-content">
        <ul class="advantages-list">
            <li class="advantages-item advantages-number-one">
                <span>Крупнейший производитель натяжных потолков в Беларуси</span>
            </li>
            <li class="advantages-item advantages-quality">
                <span>Наше кредо - быстро, профессионально и безопасно</span>
            </li>
            <li class="advantages-item advantages-guarantee">
                <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
            </li>
        </ul>
    </div>
</section>
<script src="/js/trianslider.js"></script>
<script>
    (function(){
        $("#stock-slider").trianSlider();
    })();
</script>