<section class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/articles" itemprop="item"><span itemprop="name">Статьи</span><meta itemprop="position" content="2"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/articles/<?php echo $data['article']['url'];?>" itemprop="item"><span itemprop="name"><?php echo $data['article']['title'];?></span><meta itemprop="position" content="3"></a>
        </li>
    </ul>
</section>
<section class="articles-news article_page">
        <div class="section-content">
            <div class="articles-news-wrapper clearfix">
                <div class="articles-short">
                    <h1 class="h1-title articles-h2-title"><?php echo $data['article']['title'];?></h1>
                    <div class="articles-short-time">
                        <time><?php echo $data['article']['date'];?></time>
                    </div>
                    <div class="article_content">
                        <?php echo WidgetParser::parse($data['article']['content']);?>
                    </div>
                </div>
            </div>
        </div>
</section>
