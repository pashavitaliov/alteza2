<?php
    if (!!$data['use_breadcrumbs']) {
?>
        <div class="main_nav overflow">
<!--            <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
            <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                    <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
                </li>
                <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                    <a href="/<?php echo $data['url'];?>" itemprop="item"><span itemprop="name"><?php echo $data['title'];?></span><meta itemprop="position" content="2"></a>
                </li>
            </ul>
        </div>
<?php
    }
?>
<div class="content_wrapper">
    <div class="content-about overflow">
        <div class="product-title">
            <h1><?php echo $data['pageTitle'];?></h1>
        </div>
        <?php
            if ($_GET['recaptcha_error'] === 'true') {
                echo '<div class="alert error">Ошибка! Подтвердите, что вы не робот.</div>';
            }
            if ($_GET['add-diler-success'] === 'true') {
                echo '<div class="alert success">Спасибо, вы успешно зарегистрированы! Дополнительную информацию вы можете получить по телефону <a href="tel:+375295223300" class="white_text">+375 (29) 522-33-00</a></div>';
            }
        ?>
        <div class="common-page-content">
            <?php echo $data['content'];?>
        </div>
    </div>
</div>