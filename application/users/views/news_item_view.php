<section class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/news" itemprop="item"><span itemprop="name">Новости</span><meta itemprop="position" content="2"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/news/<?php echo $data['news']['id'];?>" itemprop="item"><span itemprop="name"><?php echo $data['news']['title'];?></span><meta itemprop="position" content="3"></a>
        </li>
    </ul>
</section>
<section class="articles-news article_page">
        <div class="section-content">
            <div class="articles-news-wrapper clearfix">
                <div class="articles-short">
                    <h1 class="h1-title articles-h2-title"><?php echo $data['news']['title'];?></h1>
                    <div class="articles-short-time">
                        <time><?php echo $data['news']['date'];?></time>
                    </div>
                    <div class="article_content">
                        <?php echo $data['news']['content'];?>
                    </div>
                </div>
            </div>
        </div>
</section>
<section class="advantages t-border t-marg">
        <div class="section-content">
            <ul class="advantages-list">
                <li class="advantages-item advantages-number-one">
                    <span>Крупнейший производитель натяжных потолков в Беларуси</span>
                </li>
                <li class="advantages-item advantages-quality">
                    <span>Наше кредо - быстро, профессионально и безопасно</span>
                </li>
                <li class="advantages-item advantages-guarantee">
                    <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
                </li>
            </ul>
        </div>
</section>