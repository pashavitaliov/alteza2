<div class="article">
    <div class="container">
        <div class="article__title">
            <h1 class="h4"><?php echo $data['article']['title'];?></h1>
        </div>
        <div class="article__date news-block__new-date"><?php echo $data['article']['date'];?></div>
        <div class="article__text">
            <?php echo WidgetParser::parse($data['article']['content']);?>
        </div>
    </div>
</div>