<div class="about">
    <div class="header-bg">
        <div class="header-bg__img"></div>
        <div class="header-bg__overlay"></div>
    </div>

    <div class="content-text">
        <div class="container">
            <div class="content-text__title">
                <h1 class="uppercase h2">О компании</h1>
            </div>
            <div class="content-text__text common-content">
                <?php echo $data['content']['mobile_content'];?>
            </div>
        </div>
    </div>
</div>
