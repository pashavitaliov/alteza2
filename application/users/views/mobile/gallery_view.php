<div class="projects-page">

    <div class="container">
        <div class="projects-page__title">
            <h1 class="h2"><?php echo $data['gallery_desc']['gallery_title'];?></h1>
        </div>
    </div>

    <div class="projects">
        <div class="container">
            <div class="projects-inner">
                <div class="projects__nav-wrap">
                    <?php
                    foreach($data['navigation'] as $d){
                        ($d['id'] == $data['gallery_desc']['id']) ? $active = 'active' : $active = '';
                        echo "<a class=\"projects__slide-btn btn {$active} \" href=\"/gallery/{$d['url_mask']}\">{$d['nav_title']}</a>";
                    }
                    ?>
                </div>

                <div class="projects__slider" id="projects__slider">

                    <?php
                    foreach($data['gallery'] as $key=>$d){
                        echo "
                            <div class=\"projects__slide\">
                                <img src=\"/images/materials/gallery/{$d['img']}\"  title='{$data['gallery_desc']['main_title']} для натяжных потолков ALTEZA' alt='{$data['gallery_desc']['main_title']} для натяжных потолков ALTEZA'>
                            </div>
                        ";
                    }
                    ?>
                </div>
                <div class="projects__btn-catalog-wrap">
                    <a class="projects__btn-catalog btn btn--mayablue link" href="/catalog/<?php echo $data['gallery_desc']['url_mask'];?>">
                        Перейти в каталог
                        <svg class="svg-inline--fa fa-angle-right fa-w-8" aria-hidden="true" data-prefix="fas" data-icon="angle-right"
                             role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                            <path fill="currentColor"
                                  d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="services-info block--bg-gainsboro">
        <div class="container">
            <div class="services-info-inner">
                <div class="services-info__text common-content">
                    <?php echo $data['gallery_desc']['gallery_description'];?>
                </div>
            </div>
        </div>
    </div>

</div>