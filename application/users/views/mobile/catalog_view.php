<div class="catalog">
    <div class="catalog__slider-top-wrap ">
        <div class="projects">
            <div class="container">
                <div class="projects-inner">
                    <div class="projects__nav-wrap">
                        <?php
                        foreach($data['navigation'] as $d){
                            ($d['id'] == $data['material']['id']) ? $active = 'active' : $active = '';
                            echo "<a class=\"projects__slide-btn btn {$active} \" href=\"/catalog/{$d['url_mask']}\">{$d['nav_title']}</a>";
                        }
                        ?>
                    </div>
                    <div class="projects__title-wrap">
                        <h1 class="projects__title h2"><?php echo $data['material']['main_title'];?></h1>
                    </div>
                    <div class="projects__slider projects__slider-catalog" id="projects__slider-catalog">
                        <?php
                        foreach($data['material_main_images'] as $key=>$d){
                            echo "
                                <div class=\"projects__slide\">
                                    <img src='/images/materials/main_images/{$d['img']}' title='{$data['material']['main_title']}' alt='{$data['material']['main_title']}' />
                                </div>
                            ";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="product-properties">
        <div class="container">
            <ul class="product-properties__list list-unstyled">
                <li class="product-properties__item">
                    <div class="product-properties__icon-wrap"><img src="/images/mobile/product-properties/fire.png"></div>
                    <div class="product-properties__text">Не поддерживают горение (г2)</div>
                </li>
                <li class="product-properties__item">
                    <div class="product-properties__icon-wrap"><img src="/images/mobile/product-properties/water.png"></div>
                    <div class="product-properties__text">Водо и пыленепроницаемость</div>
                </li>
                <li class="product-properties__item">
                    <div class="product-properties__icon-wrap"><img src="/images/mobile/product-properties/thunder.png"></div>
                    <div class="product-properties__text">Электрическая нейтралльность</div>
                </li>
                <li class="product-properties__item">
                    <div class="product-properties__icon-wrap"><img src="/images/mobile/product-properties/garanty.png"></div>
                    <div class="product-properties__text">Срок службы до 25 лет</div>
                </li>
            </ul>
        </div>
    </div>

    <div class="technical-catalog">
        <div class="technical-catalog__title"><h4>Связаться с отделом продаж для бесплатной консультации</h4></div>
        <div class="technical-catalog__btn-wrap">
            <a class="link btn uppercase technical-catalog__btn" href="#">
                Позвонить
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 31.49 31.49" style="enable-background:new 0 0 31.49 31.49;" xml:space="preserve" width="512px" height="512px">
<path d="M21.205,5.007c-0.429-0.444-1.143-0.444-1.587,0c-0.429,0.429-0.429,1.143,0,1.571l8.047,8.047H1.111  C0.492,14.626,0,15.118,0,15.737c0,0.619,0.492,1.127,1.111,1.127h26.554l-8.047,8.032c-0.429,0.444-0.429,1.159,0,1.587  c0.444,0.444,1.159,0.444,1.587,0l9.952-9.952c0.444-0.429,0.444-1.143,0-1.571L21.205,5.007z" fill="#FFFFFF"/>
</svg>
            </a>
        </div>
    </div>

    <?php if($data['material_desc_images']){?>
        <div class="catalog__slider-bottom-wrap">
            <div class="catalog__slider-bottom-wrap__tite">
                <div class="container"><h5><?php echo $data['material']['main_title'];?> в интерьере</h5></div>
            </div>

            <div class="projects">
                <div class="container">
                    <div class="projects-inner">
                        <div class="projects__slider" id="projects__slider">

                            <?php
                            foreach($data['material_desc_images'] as $desc_image){
                                if ($desc_image['big'] == 1) {
                                    echo "";
                                } else {
                                    echo "
                                        <div class=\"projects__slide\">
                                            <img src=\"/images/materials/inter/{$desc_image['img']}\" alt=\"Натяжной потолок в интерьере\" title=\"Натяжной потолок в интерьере\">    
                                        </div>
                                    ";
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }?>


    <div class="services-info block--bg-gainsboro">
        <div class="container">
            <div class="services-info-inner">
                <div class="services-info__text common-content">
                    <?php echo $data['material']['content'];?>
                </div>
            </div>
        </div>
    </div>
</div>