<div class="news">
    <div class="news-block block--bg-gainsboro">
        <div class="container">
            <div class="news-block-inner">
                <div class="news-block__title"><h5>Новости</h5></div>

                <ul class="new-block__list list-unstyled">
                    <?php
                    foreach($data['news'] as $news_item){
                        echo "
                            <li class=\"news-block__item\">
                                <a class=\"link news-block__link\" href=\"/news/{$news_item['id']}\">
                                    <div class=\"news-block__new-date\">{$news_item['date']}</div>
                                    <h6 class=\"news-block__new-title\">{$news_item['title']}</h6>
                                </a>
                            </li>
                        ";
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>