<div class="dealers">
    <div class="header-bg">
        <div class="header-bg__img"></div>
        <div class="header-bg__overlay"></div>
    </div>

    <div class="dealers-form-wrap">
        <div class="dealers-form block--bg-gainsboro">
            <div class="container">
                <div class="dealers-form-inner">
                    <div class="dealers-form__title"><h1 class="uppercase h5">Стать дилером</h1></div>
                    <form method="post" action="/order_request/diller_order">
                        <input class="dealers-form__input" name="name" type="text" placeholder="Ваше имя">
                        <input class="dealers-form__input" name="company" type="text" placeholder="Компания">
                        <input class="dealers-form__input" name="phone" type="text" placeholder="Телефон">
                        <textarea class="dealers-form__input" name="message" placeholder="Сообщение" rows="8"></textarea>
                        <label>
                            <div class="g-recaptcha" data-sitekey="6LeImC4UAAAAANp2-HxxSUY2GALYcmalLMSKDPO4" data-size='compact'></div>
                        </label>
                        <input type="submit" name="submit" value="Отправить" class="link btn dealers-form__btn ga_ym_sd" />
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="content-text">
        <div class="container">
            <div class="content-text__text common-content">
                <?php echo $data['content']['mobile_content'];?>
            </div>
        </div>
    </div>
</div>
