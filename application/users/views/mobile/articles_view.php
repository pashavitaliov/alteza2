<div class="useful-articles">
    <div class="container">
        <div class="useful-article__list-wrap">
            <ul class="useful-article__list list-unstyled">
                <?php
                foreach($data['articles'] as $article){
                    echo "
                        <li class=\"useful-article__item\">
                            <a class=\"useful-article__link link\" href=\"/articles/{$article['url']}\" title='{$article['title']}'>
                                <div class=\"useful-article__img-wrap\">
                                    <img class=\"useful-article__img\" src=\"/application-administrator/public/img/articles/{$article['img']}\">
                                    <div class=\"useful-article__date\">{$article['date']}</div>
                                </div>
                                <h5 class=\"useful-article__title\">{$article['title']}</h5>
                            </a>
                        </li>  
                    ";
                }
                ?>
            </ul>
        </div>
    </div>
</div>