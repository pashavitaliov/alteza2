<div>
    <div class="container"><h1 class="h4">Отзывы о натяжных потолках в Минске</h1></div>

    <div class="dealers-form-wrap">
        <div class="dealers-form block--bg-gainsboro">
            <div class="container">
                <div class="dealers-form-inner">
                    <div class="dealers-form__title"><h5 class="uppercase">Оставить отзыв</h5></div>
                    <form method="post" action="/reviews/create">
                        <input class="dealers-form__input" name="name" type="text" placeholder="Ваше имя" required>
                        <textarea class="dealers-form__input" name="review" placeholder="Текс отзыва" rows="6" required></textarea>
                        <input type="submit" class="link btn dealers-form__btn" value="Отправить"/>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="reviews-list-wrap">
        <div class="container">
            <ul class="reviews-list list-unstyled">
                <?php foreach($data['reviews'] as $review) { ?>
                    <li class="reviews-list__item">
                        <div class="reviews-list__name"><?php echo $review['name']; ?></div>
                        <div class="reviews-list__date"><?php echo $review['created_at']; ?></div>
                        <div class="reviews-list__text"><?php echo $review['review']; ?></div>

                        <?php if(count($review['comments'])){ ?>
                            <div class="reviews-list__comment-wrap">
                                <ul class="reviews-list__comment__list list-unstyled">

                                    <?php foreach($review['comments'] as $comment) { ?>
                                        <?php if (isset($comment)) { ?>
                                            <div class="comment">
                                                <div class='review__name comment__name'>
                                                    <span class="red_text">Ответ от:</span> <?php echo $comment['comment_author']; ?>
                                                </div>
                                                <div class='review__info'>
                                                    <div class='review__date'>
                                                        <?php echo $comment['created_at']; ?>
                                                    </div>
                                                </div>
                                                <div class='review__text'>
                                                    <?php echo $comment['comment']; ?>
                                                </div>
                                            </div>

                                            <li class="reviews-list__comment__item">
                                                <div class="reviews-list__comment__name"><span> Ответ от:</span> <?php echo $comment['comment_author']; ?></div>
                                                <div class="reviews-list__comment__date reviews-list__date"><?php echo $comment['created_at']; ?></div>
                                                <div class="reviews-list__comment__text"><?php echo $comment['comment']; ?></div>
                                            </li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>

</div>