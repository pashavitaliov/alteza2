<div class="article news-item">
    <div class="container">
        <div class="article__title">
            <h1 class="h4"><?php echo $data['news']['title'];?></h1>
        </div>
        <div class="article__date news-block__new-date"><?php echo $data['news']['date'];?></div>
        <div class="article__text">
            <?php echo WidgetParser::parse($data['news']['content']);?>
        </div>
    </div>
</div>