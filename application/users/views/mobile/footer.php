<div class="measurement">
    <div class="container">
        <a class="link btn measurement__btn btn--mayablue order_call measurement" href="#">Заказать обмер</a>
    </div>
</div>
<footer class="footer">
    <div class="footer-inner">
        <div class="container" itemscope itemtype="http://schema.org/Organization">
            <div class="footer__menu">
                <ul class="list-unstyled">
                    <li><a href="/contacts">Контакты</a></li>
                    <li><a href="/reviews">Отзывы</a></li>
                    <li><a href="" class="order_call">Обратная связь</a></li>
                </ul>
            </div>
            <div class="footer__number-wrap">
                <div class="footer__number-title">
                    <span itemprop="name">Натяжные потолки ALTEZA</span><br>
                    <span itemprop="description">Производство, продажа, монтаж, установка натяжных потолков.</span>
                </div>
                <div class="footer__number-container">
                    <div class="footer__icon"><img src="images/mobile/footer-icon.png"></div>
                    <div class="footer__number">
                        <a class="link" href="tel:+375293333006"><span itemprop="telephone">+ 37533 33 33 006</span></a>
                        <a class="link" href="tel:+375293333006"><span itemprop="telephone">+ 37529 33 33 006</span></a>
                    </div>
                </div>
            </div>
            <div class="footer__address-wrap" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                <div class="footer__address">
                    <span itemprop="addressRegion">Беларусь</span>, <span itemprop="addressLocality">г. Минск</span>, <span itemprop="postalCode">220090</span>, <span itemprop="streetAddress">Логойский тракт 50 , каб. 335 (Бизнес-центр "Аквабел")</span>
                </div>
                <div class="footer__email"><a class="link" href="mailto:office@alteza.by">office@alteza.by</a></div>
            </div>
            <div class="footer__time-work-wrap">
                <div class="footer__time-work">Время работы: ПН - ПТ 9:00 - 18:00 Обед: 13:00 - 14:00</div>
            </div>
        </div>
    </div>
    <div class="footer__social-wrap"><a class="link" href="https://www.facebook.com/www.alteza.by/" target="_blank">
            <div class="container">
                <div class="footer__social"><img src="images/mobile/icon_facebook.png"><span>Мы в facebook</span></div>
            </div>
        </a></div>
    <div class="footer__copy-wrap">
        <div class="container">
            <div class="footer__copy">© 2018 Alteza. Все права защищены</div>
        </div>
    </div>
</footer>