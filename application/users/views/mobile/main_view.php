<div class="main-slider-wrapper">
    <div class="main-slider" id="main-slider">
        <?php
            foreach($data["actions"] as $action){
                $content = '<img class="main-slider__image" src="/images/actions/'.$action["img"].'" alt="'.$action["title"].'" title="Подробнее" />';

                echo "
                    <a class='main-slider__elemtn' href='/actions' title='{$action['title']}'>
                        {$content}
                    </a>
                ";
            }
        ?>
    </div>
</div>

<div class="link-group">
    <div class="container">
        <div class="link-group-inner">
            <div class="link__btn"><a class="link btn btn--mayablue" href="/actions">Акции и скидки</a></div>
            <div class="link__btn"><a class="link btn btn--rhino" href="/gallery/matovi_natiajnoy_potolok">Фото проектов</a></div>
        </div>
    </div>
</div>

<div class="strong-five">
    <div class="container">
        <div class="strong-five-inner"><h5 class="strong-five__title">Сильная пятёрка Alteza</h5>
            <ul class="strong-five__list list-unstyled">
                <li class="strong-five__item">
                    <div class="strong-five__icon-wrapper"><img class="strong-five__icon" src="/images/mobile/strong-five-icon.png"><span
                                class="strong-five__number">1</span></div>
                    <p class="strong-five__text">Собственное производство</p></li>
                <li class="strong-five__item">
                    <div class="strong-five__icon-wrapper"><img class="strong-five__icon" src="/images/mobile/strong-five-icon.png"><span
                                class="strong-five__number">2</span></div>
                    <p class="strong-five__text">Качественные материалы</p></li>
                <li class="strong-five__item">
                    <div class="strong-five__icon-wrapper"><img class="strong-five__icon" src="/images/mobile/strong-five-icon.png"><span
                                class="strong-five__number">3</span></div>
                    <p class="strong-five__text">Лучшие системы профилей</p></li>
                <li class="strong-five__item">
                    <div class="strong-five__icon-wrapper"><img class="strong-five__icon" src="/images/mobile/strong-five-icon.png"><span
                                class="strong-five__number">4</span></div>
                    <p class="strong-five__text">Монтаж</p></li>
                <li class="strong-five__item">
                    <div class="strong-five__icon-wrapper"><img class="strong-five__icon" src="/images/mobile/strong-five-icon.png"><span
                                class="strong-five__number">5</span></div>
                    <p class="strong-five__text">25 лет гарантии</p></li>
            </ul>
        </div>
    </div>
</div>

<div class="main-video-wrap main-video-wrap--mb">
    <div class="container">
        <video id="video" width="100%" preload="auto" controls>
            <source src="/video/video.mp4">
        </video>
    </div>
    <div class="container">
        <div class="main-video__title"><h6>Мы создаем потолки, которые выходят за рамки стандартных схем <br>и заурядных замыслов.</h6></div>
    </div>
</div>

<div class="services-info block--bg-gainsboro">
    <div class="container">
        <div class="services-info-inner">
            <div class="services-info__title"><h5 class="uppercase">Натяжные потолки в Минске от Alteza</h5>
                <div class="services-info__text-wrap services-info__text-wrap--hidden common-content">
                    <p class="services-info__text">Натяжные потолки ALTEZA - это индивидуальный дизайн и отменное качество, разнообразие цветов и фактур. Наши дизайнеры воплотят в жизнь самые смелые и неожиданные идеи, подчеркнут ваш уникальный стиль, а также профессионально проконсультируют.</p>
                    <p class="services-info__text"> Мы предлагаем натяжные потолки под ключ, которые не требуют дополнительных монтажных операций. Благодаря команде специалистов высочайшего уровня мы создаём интерьерные шедевры.</p>
                    <p class="services-info__text">Как лидеры рынка натяжных потолков, мы предлагаем самый широкий в стране ассортимент, отменное качество и кратчайшие сроки исполнения заказов. На сегодняшний день, купить натяжные потолки в Минске и в регионах не составит труда, поскольку у нас разветвлённая сеть филиалов.</p>
                    <p class="services-info__text">Мы уделяем огромное внимание поддержанию ассортимента на наших складах, что позволяет значительно сократить сроки изготовления натяжных потолков и сделать весь процесс максимально комфортным для заказчика. Собственное производство ALTEZA одно из самых крупных в ЕС.</p>
                    <p class="services-info__text">Мы осуществляем поставки комплектующих для натяжных потолков напрямую из Германии, Франции и Юго-Восточной Азии в больших объёмах и поэтому можем предложить самые выгодные цены для наших клиентов. </p>
                    <p class="services-info__text">Все составляющие наших натяжных потолков идеально подогнаны друг под друга. Мы тщательно контролируем все этапы производства.</p>
                    <p class="services-info__text"> ALTEZA - это качественные и разнообразные варианты натяжных потолков, которые удовлетворят даже самых искушенных клиентов.</p>
                    <p class="services-info__text">Мы гордимся тем, что мы делаем. Если вы хотите создать свой собственный стиль в премиум исполнении, вам нужны натяжные потолки ALTEZA.</p>
                </div>

                <a class="services-info__link link" id="services-info-btn" href="">
                    Читать дальше
                    <svg class="svg-inline--fa fa-angle-right fa-w-8" aria-hidden="true" data-prefix="fas"
                         data-icon="angle-right" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                        <path fill="currentColor"
                              d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="main-projects-wrap">
    <div class="projects">
        <div class="container">
            <div class="projects-inner">
                <div class="projects__nav-wrap" id="projects__nav-wrap">
                <?php
                    foreach($data['materials'] as $key => $material){
                        echo "<a class=\"projects__slide-btn btn\" data-slide-index=\"{$key}\" href=\"\">{$material['title']}</a>";
                    }
                ?>
                </div>

                <div class="projects__slider" id="projects__slider">
                <?php
                    foreach($data['materials'] as $key => $material){
                        echo "
                        <div class=\"projects__slide\">
                            <img src=\"/images/materials/main-page/{$material['img']}\" alt='{$material['main_title']}' title='{$material['main_title']}'>
                            <div class=\"projects__btn-catalog-wrap\">
                                <a class=\"projects__btn-catalog btn btn--mayablue link\" href=\"/catalog/{$material['url_mask']}\">
                                    Перейти в каталог 
                                    <svg class=\"svg-inline--fa fa-angle-right fa-w-8\" aria-hidden=\"true\" data-prefix=\"fas\"
                                         data-icon=\"angle-right\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 256 512\">
                                        <path fill=\"currentColor\"
                                              d=\"M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z\"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                        ";
                    }
                ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="main-profiles">
    <div class="profiles">
        <div class="container">
            <div class="profiles__title"><h5>Уникальные профили</h5></div>

            <div class="profiles__slider" id="profiles__slider">

            <?php
                foreach($data["systems"] as $system){
                    echo "
                        <div class=\"profiles__item\">
                            <img class=\"profiles__img\" src=\"/images/systems/{$system['main_img']}\">
                            <h6 class=\"profiles__item-title uppercase\">{$system['title']}</h6>
                            <p class=\"profiles__text\">
                                {$system['preview']}
                            </p>
                        </div>
                    ";
                }
            ?>
            </div>
        </div>
    </div>
</div>

<div class="main-news-wrap">
    <div class="news-block block--bg-gainsboro">
        <div class="container">
            <div class="news-block-inner">
                <div class="news-block__title"><h5>Новости</h5></div>

                <ul class="new-block__list list-unstyled">
                    <?php
                    foreach($data['news'] as $news_item){
                        echo "
                            <li class=\"news-block__item\">
                                <a class=\"link news-block__link\" href=\"/news/{$news_item['id']}\">
                                    <div class=\"news-block__new-date\">{$news_item['date']}</div>
                                    <h6 class=\"news-block__new-title\">{$news_item['title']}</h6>
                                </a>
                            </li>
                        ";
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="main-news-wrap__more-news-btn">
            <a class="link" href="/news">
                Больше новостей
                <svg class="svg-inline--fa fa-angle-right fa-w-8" aria-hidden="true" data-prefix="fas" data-icon="angle-right"
                     role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 256 512">
                    <path fill="currentColor"
                          d="M224.3 273l-136 136c-9.4 9.4-24.6 9.4-33.9 0l-22.6-22.6c-9.4-9.4-9.4-24.6 0-33.9l96.4-96.4-96.4-96.4c-9.4-9.4-9.4-24.6 0-33.9L54.3 103c9.4-9.4 24.6-9.4 33.9 0l136 136c9.5 9.4 9.5 24.6.1 34z"></path>
                </svg>
            </a>
        </div>
    </div>
</div>