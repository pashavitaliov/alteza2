<header class="header">
    <div class="header__inner">
        <div class="container">
            <div class="header__btn-group"><a class="btn btn--cinnabar order_call" href="#">Заказать звонок</a><a
                    class="btn btn--dimgray" href="/reviews">Отзывы</a></div>
            <div class="header__contact-group">
                <div class="header__logo-wrap"><a class="header__logo" href="/"><img src="images/alteza-logo.svg"></a></div>
                <div class="header__contact">
                    <div class="header__number-wrap"><span class="header__number-icon"><img src="images/mobile/icon-m.png"></span><span
                            class="header__number"><a class="link" href="tel:+375293333006">+ 37533 33 33 006</a></span></div>
                    <div class="header__number-wrap"><span class="header__number-icon"><img src="images/mobile/icon-v.png"></span><span
                            class="header__number"><a class="link" href="tel:+375293333006">+ 37529 33 33 006</a></span></div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="menu menu--bg" id="menu">
    <div class="container">
        <div class="menu-inner">
            <div class="burger" id="burger">
                <div class="burger-box">
                    <div class="burger-inner"></div>
                </div>
            </div>
            <div class="menu__title"><h5><?php echo $data['seo_data']['page_title']?></h5></div>
            <nav class="menu__nav">
                <ul class="menu__list list-unstyled">
                    <li class="menu__item"><a class="menu__link link" href="/">Главная</a></li>
                    <li class="menu__item menu__item--with-sub-menu">
                        <a class="menu__link link" href="/catalog">Каталог</a>

                        <ul class="menu__list list-unstyled">
                            <li class="menu__item">
                                <a class="menu__link link" href="/catalog/matovi_natiajnoy_potolok">Матовые потолки</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/catalog/gliancevi_natiajnoy_potolok">Глянцевые потолки</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/catalog/art-print">Art-print</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/catalog/satinovi_natiajnoy_potolok">Сатиновые потолки</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/catalog/transparent">Транспарент</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/catalog/facturni_natiajnoy_potolok">Фактурные потлки</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/catalog/s-otverstiami">С отверстиями</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu__item"><a class="menu__link link" href="/gallery/matovi_natiajnoy_potolok">Проекты</a></li>
                    <li class="menu__item menu__item--with-sub-menu">
                        <a class="menu__link link" href="/about">Клиентам</a>

                        <ul class="menu__list list-unstyled">
                            <li class="menu__item">
                                <a class="menu__link link" href="/actions">Акции</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/about">О компании</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/reviews">Отзывы</a>
                            </li>
                            <li class="menu__item">
                                <a class="menu__link link" href="/articles">Статьи</a>
                            </li>
                        </ul>
                    </li>
                    <li class="menu__item"><a class="menu__link link" href="/designers">Дизайнерам</a></li>
                    <li class="menu__item"><a class="menu__link link" href="/dilers">Дилерам</a></li>
                    <li class="menu__item"><a class="menu__link link" href="/contacts">Контакты</a></li>
                </ul>
            </nav>
        </div>
    </div>
</div>
<div class="menu--bg-overlay"></div>