<div class="actions">
    <div class="main-slider-wrapper">
        <div class="actions-slider" id="actions-slider">
            <?php
            foreach($data["actions"] as $action){
                $content = '<img class="main-slider__image" src="/images/actions/'.$action["img"].'" alt="'.$action["title"].'" title="Подробнее" />';

                echo "
                    <a class='main-slider__elemtn' href='/actions' title='{$action['title']}'>
                        {$content}
                    </a>
                ";
            }
            ?>
        </div>
    </div>

    <div class="services-info block--bg-gainsboro">
        <div class="container">
            <div class="services-info-inner">
                <div class="services-info__text common-content">
                    <?php echo $data['content']['mobile_content'];?>
                </div>
            </div>
        </div>
    </div>
</div>