<?php
(isset($data['seo_data'])) ? $seo_data = $data['seo_data'] : $seo_data = null;
(isset($data['page_title'])) ? $page_name = $data['page_title'] : $page_name = null;

?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="noyaca"/>
	<base href="https://alteza.by/" />
	<link rel="shortcut icon" href="/images/favicon.png" type="image/png">
	<?php if($data["main_page_flag"]) echo '<link rel="canonical" href="https://alteza.by/" />';?>
	<title><?php if(isset($seo_data)) echo $seo_data["title"]?></title>
	<meta name="description" content="<?php if(isset($seo_data)) echo $seo_data["desc"]?>">
	<meta name="keywords" content="<?php if(isset($seo_data)) echo $seo_data["keywords"]?>">
	<link rel="stylesheet" href="/css/font-awesome.min.css">
	<link rel="stylesheet" href="/css/main.css">
	<link rel="stylesheet" href="/css/jquery.mThumbnailScroller.css">
	<link rel="stylesheet" href="/css/zoom.css">
	<link rel="stylesheet" href="/css/jquery-trian-popup.css">
	<link rel="stylesheet" href="/css/custom.css">
	<link rel="stylesheet" href="/css/media.css">

	<script src="/js/jquery-2.2.3.min.js"></script>
    <script src="/js/inputmask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.min.js"></script>
    <!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<div class="wrapper">
	<div class="wrapper-inner simple-page-wrapper-inner <?php echo $page_name;?>">
		<div class="main overflow simple-page-main">

			<?php include 'application/users/views/'.$content_view;?>

		</div>
	</div>
</div>
<script src="/js/nav-selected.js"></script>
<script src="/js/trianzoom.js"></script>
<script src="/js/jquery-trian-popup.js"></script>
<script src="/js/jquery.bxslider.js"></script>
<script src="/js/main.js"></script>
<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-54ZWSC6':true});</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-77208307-1', 'auto');
	ga('require', 'GTM-54ZWSC6');
	ga('send', 'pageview');

</script>
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter20785411 = new Ya.Metrika({id:20785411,
					webvisor:true,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".ga_ym_t").on('click', function() {
		    ga('send', 'event', 'tel-info', 'ClickTel');
		    yaCounter20785411.reachGoal('ClickTelYM');
		    return true;
		   }); 
		$(".ga_ym_t").on('copy', function() {
		    ga('send', 'event', 'tel-info', 'CopyTel');
		    yaCounter20785411.reachGoal('CopyTelYM');
		    return true;
		   });
		$(".ga_ym_t").on('contextmenu', function() {
		    ga('send', 'event', 'tel-info', 'RightTel');
		    yaCounter20785411.reachGoal('RightClickTelYM');
		    return true;
		   });
		$(".ga_ym_m").on('click', function() {
		    ga('send', 'event', 'mail-info', 'ClickMail');
		    yaCounter20785411.reachGoal('ClickMailYM');
		    return true;
		   }); 
		$(".ga_ym_m").on('copy', function() {
		    ga('send', 'event', 'mail-info', 'CopyMail');
		    yaCounter20785411.reachGoal('CopyMailYM');
		    return true;
		   });
		$(".ga_ym_m").on('contextmenu', function() {
		    ga('send', 'event', 'mail-info', 'RightMail');
		    yaCounter20785411.reachGoal('RightClickMailYM');
		    return true;
		   });


		$(".ga_ym_hf").on('click', function() { // - открыть форму в хэдере
		    ga('send', 'event', 'head-info', 'HeadForm');
		    yaCounter20785411.reachGoal('HeadFormYM');
		    return true;
		   }); 
		$(".ga_ym_cf").on('click', function() { // - открыть форму на страницах каталога
		    ga('send', 'event', 'catalog-info', 'CatalogForm');
		    yaCounter20785411.reachGoal('CatalogFormYM');
		    return true;
		   }); 
		$(".ga_ym_ff").on('click', function() { // - открыть форму в футере
		    ga('send', 'event', 'foot-info', 'FootForm');
		    yaCounter20785411.reachGoal('FootFormYM');
		    return true;
		   }); 
		$(".ga_ym_zf").on('click', function() { // - открыть форму обр. звонка на главной
		    ga('send', 'event', 'zakaz-info', 'ZakazForm');
		    yaCounter20785411.reachGoal('ZakazFormYM');
		    return true;
		   }); 
		$("body").on('click', '.ga_ym_f', function() { // - нажатие по кнопке отправить в формах
		    ga('send', 'event', 'forma-info', 'Form');
		    yaCounter20785411.reachGoal('FormYM');
		    return true;
		   }); 
		$(".ga_ym_rs").on('click', function() { // - кнопка расчитать на страницах каталога
		    ga('send', 'event', 'raschet-info', 'Raschet');
		    yaCounter20785411.reachGoal('RaschetYM');
		    return true;
		   }); 
		$(".ga_ym_zo").on('click', function() { // - кнопка заказать обмер после расчета в калькуляторе
		    ga('send', 'event', 'obmer-info', 'ZakObmer');
		    yaCounter20785411.reachGoal('ZakObmerYM');
		    return true;
		   });
		$(".ga_ym_sd").on('click', function() { // - кнопка на странице стать диллером
		    ga('send', 'event', 'diller-info', 'StatDiller');
		    yaCounter20785411.reachGoal('StatDillerYM');
		    return true;
		   });
		$(".ga_ym_sk").on('click', function() { // - кнопка скачать каталога на странице дизайнеров
		    ga('send', 'event', 'sk-katalog', 'SkachatCatal');
		    yaCounter20785411.reachGoal('SkachatCatalYM');
		    return true;
		   });
	});
</script>
</body>
</html>
