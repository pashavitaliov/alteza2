<div class="main_nav overflow">
<!--    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/gallery/matovi_natiajnoy_potolok" itemprop="item"><span itemprop="name">Галлерея</span><meta itemprop="position" content="2"></a>
        </li>
        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
            <a href="/gallery/<?php echo $data['gallery_desc']['url_mask'];?>" itemprop="item"><span itemprop="name"><?php echo $data['gallery_desc']['nav_title'];?></span><meta itemprop="position" content="3"></a>
        </li>
    </ul>
</div>
<div class="box">
    <div class="profiles-slider-nav">
        <ul class="ps-nav-list">
            <?php
                foreach($data['navigation'] as $d){
                    ($d['id'] == $data['gallery_desc']['id']) ? $active = 'ps-nav-active' : $active = '';
                    echo "<li class=\"ps-nav-item {$active}\"><a href=\"/gallery/{$d['url_mask']}\"  class=\"ps-nav-text\">{$d['nav_title']}</a></li>";
                }
            ?>
        </ul>
    </div>
    <div class="content_wrapper gallery_wrap overflow">
        <h1><?php echo $data['gallery_desc']['gallery_title'];?></h1>
        <div class="gallery overflow">
            <div class="gallery-cta">
                <div class="order-call">
                    <button class="btn small-btn red-btn order_call small-btn_mobile_size ga_ym_pf">Заказать звонок</button>
                </div>
            </div>
            <div class="product-slider" id="product-slider">

                <div class="show-slide slider" id="current_img">
                    <img src="/images/materials/gallery/<?php echo $data['gallery'][0]['img'];?>"  alt="<?php echo $data['gallery_desc']['main_title'];?> для натяжных потолков ALTEZA" title="<?php echo $data['gallery_desc']['main_title'];?> для натяжных потолков ALTEZA">
                    <a href="/images/materials/gallery/<?php echo $data['gallery'][0]['img'];?>" class="show-img" title="<?php echo $data['gallery_desc']['main_title'];?> для натяжных потолков ALTEZA"></a>
                </div>
                <div class="slider-cell-all" id="main_images_catalog">
                    <ul class="main_images_catalog">
                        <?php
                            foreach($data['gallery'] as $key=>$d){
                                ($key == 0) ? $selected = 'selected_img' : $selected = '';
                                echo "
                                    <li class=\"{$selected}\">
                                        <a href=\"/images/materials/gallery/{$d['img']}\" title='{$data['gallery_desc']['main_title']} для натяжных потолков ALTEZA'>
                                            <img src=\"/images/materials/gallery/{$d['img']}\" title='{$data['gallery_desc']['main_title']} для натяжных потолков ALTEZA' alt='{$data['gallery_desc']['main_title']} для натяжных потолков ALTEZA'>
                                        </a>
                                    </li>
                                ";
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="content-about content-gallery overflow">
            <div class="btn_wrap">
                <a href="/catalog/<?php echo $data['gallery_desc']['url_mask'];?>" class="btn_arrow">Перейти в каталог</a>
            </div>
            <div class="content overflow">
                <?php echo $data['gallery_desc']['gallery_description'];?>
            </div>
        </div>
    </div>
</div>
<script src="/js/jquery.mThumbnailScroller.min.js"></script>
<script src="/js/triangallery.js"></script>
<script>
    $(document).ready(function(){
        $("#main_images_catalog").mThumbnailScroller();
        $("#product-slider").trianGallery();
    });

    $(".ga_ym_pf").on('click', function() {
        ga('send', 'event', 'head-info', 'HeadForm');
        yaCounter20785411.reachGoal('ProektFormYM');
        return true;
    });
</script>