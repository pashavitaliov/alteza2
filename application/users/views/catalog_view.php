                <div class="main_nav overflow">
<!--                    <a href="/" class="go_back"><img src="/images/go-back-arrow.png" title="Вернуться" alt="Вернуться"></a>-->
                    <ul class="breadcrumbs overflow" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a href="/" itemprop="item"><span itemprop="name">Главная</span><meta itemprop="position" content="1"></a>
                        </li>
                        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a href="/catalog" itemprop="item"><span itemprop="name">Каталог</span><meta itemprop="position" content="2"></a>
                        </li>
                        <li itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                            <a href="/catalog/<?php echo $data['material']['url_mask'];?>" itemprop="item"><span itemprop="name"><?php echo $data['material']['nav_title'];?></span><meta itemprop="position" content="3"></a>
                        </li>
                    </ul>
                </div>
                <div>
                    <div class="profiles-slider-nav">
                        <ul class="ps-nav-list">
                            <li class="ps-nav-item ps-nav-active"><a href="/catalog/matovi_natiajnoy_potolok" class="ps-nav-text">Пленки</a></li>
                            <li class="ps-nav-item"><a href="/catalog/sistemy_kreplenija" class="ps-nav-text">Системы крепления</a></li>
                        </ul>
                        <ul class="profiles-bottom-nav">
                            <?php
                                foreach($data['navigation'] as $d){
                                    ($d['id'] == $data['material']['id']) ? $active = 'profiles-bottom-nav-active' : $active = '';
                                    echo "<li class=\"profiles-bottom-nav-item {$active}\"><a href=\"/catalog/{$d['url_mask']}\" class=\"profiles-bottom-nav-link\">{$d['nav_title']}</a></li>";

                                }
                            ?>
                        </ul>
                    </div>
                    <div class="main-content">
                        <section class="first-level">
                            <div class="product-title">
                                <h1><?php echo $data['material']['main_title'];?></h1>
                            </div>
                            <div class="main-content-left">
                                <div class="product-slider" id="product-slider">

                                    <div class="show-slide slider" id="current_img">
                                        <img src="/images/materials/main_images/<?php echo $data['material_main_images'][0]['img'];?>"  alt="<?php echo $data['material']['main_title'];?>" title="<?php echo $data['material']['main_title'];?>">
                                        <a href="/images/materials/main_images/<?php echo $data['material_main_images'][0]['img'];?>" class="show-img" title="<?php echo $data['material']['main_title'];?>"></a>
                                    </div>
                                    <div class="slider-cell-all" id="main_images_catalog">
                                        <ul class="main_images_catalog">
                                            <?php
                                                foreach($data['material_main_images'] as $key=>$d){
                                                    ($key == 0) ? $selected = 'selected_img' : $selected = '';
                                                    echo "
                                                        <li class=\"{$selected}\">
                                                            <a href=\"/images/materials/main_images/{$d['img']}\" title='{$data['material']['main_title']}'>
                                                                <img src=\"/images/materials/main_images/{$d['img']}\" title='{$data['material']['main_title']}' alt='{$data['material']['main_title']}'>
                                                            </a>
                                                        </li>
                                                    ";
                                                }
                                            ?>
                                        </ul>
                                    </div>
                                </div>

                                <div class="product-designer">
                                    <div class="roof-designer">
                                        <img src="/images/designer-sofa.png" alt="">
                                        <span>Вы можете посмотреть цвет плёнки в интерьере в нашем конструкторе потолков</span>
                                        <a href="/construct">
                                            <div class="designer-arrow"><img src="/images/big-arrow-next-blue.png" alt=""></div>
                                        </a>
                                    </div>
                                    <!-- <div class="roof-designer sec">
                                        <img src="/images/designer-tile.png" alt="">
                                        <span>Вы можете рассчитать стоимость потолка в нашем калькуляторе</span>
                                        <a href="/calc" class="ga_ym_rs">
                                            <div class="designer-arrow"><img src="/images/big-arrow-next-orange.png" alt=""></div>
                                        </a>
                                    </div> -->
                                    <div class="back-call">
                                        <div class="cta-content"></div>
                                        <div>
                                            <div class="cta-text">
                                                <p>Связаться с отделом продаж для бесплатной консультации</p>
                                                <p><span class="ga_ym_t">+375 33 33-33-006 (МТС)</span>, <span class="ga_ym_t">+375 29 33-33-006 (Velcom)</span></p>
                                            </div>
                                            <div class="back-call-button order_call ga_ym_cf">
                                                <span>ЗАКАЗАТЬ СО СКИДКОЙ<img src="/images/big-arrow-next-black.png" alt=""></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="main-content-right">
                                <div class="color-palette">
                                    <span class="color-palette-title">Цветовая палитра</span>
                                    <div class="product-palette">
                                        <div class="color-overflow">
                                            <div class="overflow palette-buttons-wrap palette-buttons-prev">
                                                <div class="prev-palette"><img src="/images/prev-palette.png" alt="Назад" title="Назад"></div>
                                                <div class="prev-palette-all"><img src="/images/prev-palette-all.png" alt="В начало" title="В начало"></div>
                                            </div>
                                            <div class="palette" id="palette">
                                                <ul>
                                                    <?php
                                                        if($data['material']['decor'] == 'color'){
                                                            foreach($data['material_decor'] as $d){
                                                                echo "<li class=\"color\" style=\"background-color:{$d['color']};\" data-color-id = '{$d['id']}'></li>";
                                                            }
                                                        }else{
                                                            ($data['material']['id'] == 3) ? $img_class = 'art-print-img' : $img_class = '';
                                                            foreach($data['material_decor'] as $d){
                                                                echo "<li class=\"color {$img_class}\" data-img-id = '{$d['id']}'>
                                                                        <a href='/images/materials/decor-slider-images/large/{$d['img']}' class='show-img' title='{$d['title']}'>
                                                                            <img src='/images/materials/decor-slider-images/{$d['img']}' alt='{$d['title']}'>
                                                                        </a>
                                                                     </li>";
                                                            }
                                                        }
                                                    ?>
                                                </ul>
                                            </div>
                                            <div class="overflow palette-buttons-wrap palette-buttons-next">
                                                <div class="next-palette"><img src="/images/next-palette.png" alt="Вперед"></div>
                                                <div class="next-palette-all"><img src="/images/next-palette-all.png" alt="В конец"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

								<div class="product-properties">
									<div class="product-prop">
										<h3 class="name property-title"></h3>
										<p class="name-prop property-width">Ширина пленки: <span></span> м.</p>
										<p class="name-prop property-manufacturer">Производитель: <span></span></p>
									</div>

									<div class="content-left">
										<ul>
											<li>
												<div><img alt="" src="/images/fire.png" /></div>
												<span>Не поддерживают горение(Г2)</span>
											</li>
											<li>
												<div><img alt="" src="/images/thunder.png" /></div>
												<span>Электричекая нейтральность</span>
											</li>
											<li>
												<div><img alt="" src="/images/garanty.png" /></div>
												<span>Срок службы - более 25 лет</span>
											</li>
											<li>
												<div><img alt="" src="/images/gear.png" /></div>
												<span>Различные конфигураци</span>
											</li>
										</ul>
									</div>

									<div class="content-right">
										<ul>
											<li>
												<div><img alt="" src="/images/water.png" /></div>
												<span>Водо - и пыленепроницаемость</span>
											</li>
											<li>
												<div><img alt="" src="/images/color-palette.png" /></div>
												<span>Богатая цветовая палитра</span>
											</li>
											<li>
												<div><img alt="" src="/images/strong.png" /></div>
												<span>Выдерживают до 100 литров на 1 м.кв.</span>
											</li>
											<li>
												<div><img alt="" src="/images/light.png" /></div>
												<span>Возможность установки светильников</span>
											</li>
										</ul>
									</div>
								</div>
                            </div>
                        </section>
                        <section class="second-level">
                        	<?php if($data['material_desc_images']){?>
	                            <div class="product-interior">
	                                <h3><?php echo $data['material']['main_title'];?> в интерьере</h3>
	                                <div class="product-inter-images overflow">
	                                    <?php
	                                        foreach($data['material_desc_images'] as $d){
	                                            ($d['big'] == 1) ? $big = 'big-img' : $big = '';
	                                            echo "
	                                                <div class=\"img-grid-el {$big}\">
	                                                    <img src=\"/images/materials/inter/{$d['img']}\" alt=\"Натяжной потолок в интерьере\" title=\"Натяжной потолок в интерьере\">
	                                                    <a href=\"/images/materials/inter/{$d['img']}\" class=\"show-img\" title='Натяжной потолок в интерьере'></a>
	                                                </div>
	                                            ";
	                                        }
	                                    ?>
	                                </div>
	                            </div>
                            <?php }?>

                            <div>
	                			<div class="product-description">
									<?php echo $data['material']['content'];?>
								</div>
                            </div>

                        </section>

                        <div class="advantages t-border t-marg-sm">
                            <div class="section-content">
                                <ul class="advantages-list">
                                    <li class="advantages-item advantages-number-one">
                                        <span>Крупнейший производитель натяжных потолков в Беларуси</span>
                                    </li>
                                    <li class="advantages-item advantages-quality">
                                        <span>Наше кредо - быстро, профессионально и безопасно</span>
                                    </li>
                                    <li class="advantages-item advantages-guarantee">
                                        <span>Alteza дает своим клиентам 25-летнюю гарантию.</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="/js/jquery.mThumbnailScroller.min.js"></script>
                <script src="/js/triangallery.js"></script>
                <script src="/js/palette-settings.js"></script>
                <script src="/js/palette-color-cursor.js"></script>
                <script>
                    $(document).ready(function(){
                        var cursor = new ColorCursor('<?php echo $data['material']['decor'];?>').init();
                        $("#main_images_catalog").mThumbnailScroller();
                        $("#product-slider").trianGallery();
                    });
                </script>
