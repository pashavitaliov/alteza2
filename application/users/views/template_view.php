<?php
(isset($data['seo_data'])) ? $seo_data = $data['seo_data'] : $seo_data = null;
(isset($data['page_title'])) ? $page_name = $data['page_title'] : $page_name = null;
?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
    <style>
        .desktop-site, .mobile-site {
            opacity: 0;
        }
    </style>
	<meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<meta name="robots" content="noyaca"/>
	<base href="https://alteza.by/" />
	<link rel="shortcut icon" href="/images/favicon.png" type="image/png">
	<?php if($data["main_page_flag"]) echo '<link rel="canonical" href="https://alteza.by/" />';?>
	<title><?php if(isset($seo_data)) echo $seo_data["title"]?></title>
	<meta name="description" content="<?php if(isset($seo_data)) echo $seo_data["desc"]?>">
	<meta name="keywords" content="<?php if(isset($seo_data)) echo $seo_data["keywords"]?>">
	<link rel="stylesheet" href="/css/font-awesome.min.css">

    <script src="/js/jquery-2.2.3.min.js"></script>
    <script src="/js/mobile/index.js"></script>

<!--    <link rel="stylesheet" href="/css/main.css">-->
	<link rel="stylesheet" href="/css/jquery.mThumbnailScroller.css">
	<link rel="stylesheet" href="/css/zoom.css">
	<link rel="stylesheet" href="/css/jquery-trian-popup.css">
	<link rel="stylesheet" href="/css/custom.css">
<!--	<link rel="stylesheet" href="/css/media.css">-->

	<!--[if IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	<script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

<div class="spinner-wrapper">
    <div class="spinner-circle"><div></div><div></div><div></div><div></div></div>
</div>

<div class="wrapper desktop-site">
	<div class="wrapper-inner <?php echo $page_name;?>">
		<header class="header">
			<div class="section-content">
				<div class="header-content-wrapper header-content-container clearfix">
					<div class="header-logo">
						<a href="/" class="h-logo-link" title="Натяжные потолки ALTEZA">
							<img src="/images/logo.svg" alt="Натяжные потолки ALTEZA" title="Натяжные потолки ALTEZA">
						</a>
					</div>
					<div class="header-contacts-nav">

                        <div class="header__content flex-row flex-row--align-center">
                            <div class="header-contacts-wrapper">
                                <ul class="header-phones-list flex-row">
                                    <li class="header-phones-item">
                                        <span class="header-phones-item__icon"><img src="images/mobile/icon-v.png"></span>
                                        <a href="tel:+375293333006" class="ga_ym_t">+375 29 33-33-006</a>
                                    </li>
                                    <li class="header-phones-item">
                                        <span class="header-phones-item__icon"><img src="images/mobile/icon-m.png"></span>
                                        <a href="tel:+375333333006" class="ga_ym_t">+375 33 33-33-006</a>
                                    </li>
                                    <li class="header-phones-item">
                                        <a href="mailto:office@alteza.by" class="ga_ym_m header-email">office@alteza.by</a>
                                    </li>
                                </ul>
                                <div class="header-addresses flex-row flex-row--align-center">
                                    <div class="header-addresses__item">
                                        Адрес: <span class="color--dark-gray">г. Минск, Логойский тракт, 50-335</span>
                                    </div>
                                    <div class="header-addresses__item text-right">
                                        Время работы: <span class="color--dark-gray">Пн-Пт с 9:00 до 18:00.</span>
                                    </div>
                                </div>
                            </div>

                            <div class="header-buttons">
                                <div class="header-buttons__item">
                                    <button class="btn small-btn red-btn order_call w--100 ga_ym_hf">Заказать звонок</button>
                                </div>

                                <div class="header-buttons__item">
                                    <button class="btn small-btn blue-btn order_call measurement w--100 ga_ym_hf ">Заказать обмер</button>
                                </div>
                            </div>
                        </div>


                        <nav class="header-nav header__nav">
                            <ul class="header-nav-list">
                                <li class="header-nav-item"><a href="/" class="header-nav-link" title="Главная">Главная</a></li>
                                <li class="header-nav-item with-dropdown">
                                    <ul class="dropdown-nav">
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/matovi_natiajnoy_potolok">Матовые потолки</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/gliancevi_natiajnoy_potolok">Глянцевые потолки</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/art-print">Art-print</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/satinovi_natiajnoy_potolok">Сатиновые потолки</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/transparent">Транспарент</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/facturni_natiajnoy_potolok">Фактурные потлки</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/s-otverstiami">С отверстиями</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/catalog/sistemy_kreplenija">Системы крепления</a>
                                        </li>
                                    </ul>
                                    <a href="/catalog" class="header-nav-link" title="Каталог натяжных потолков">Каталог</a>
                                </li>
                                <li class="header-nav-item"><a href="/gallery/matovi_natiajnoy_potolok" class="header-nav-link" title="Галерея натяжных потолков">Проекты</a></li>

                                <li class="header-nav-item with-dropdown">
                                    <ul class="dropdown-nav">
                                        <li class="dropdown-nav-item">
                                            <a href="/actions">Акции</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/about">О компании</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/construct">Конструктор</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/clients">Наши клиенты</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/reviews">Отзывы</a>
                                        </li>
                                        <li class="dropdown-nav-item">
                                            <a href="/articles">Статьи</a>
                                        </li>
                                    </ul>
                                    <a href="/about" class="header-nav-link" title="Клиентам компании ALTEZA">Клиентам</a>
                                </li>
                                <li class="header-nav-item"><a href="/dilers" class="header-nav-link" title="Дилерам">Дилерам</a></li>
                                <li class="header-nav-item"><a href="/designers" class="header-nav-link" title="Дизайнерам">Дизайнерам</a></li>
                                <li class="header-nav-item"><a href="/contacts" class="header-nav-link" title="Контакты">Контакты</a></li>
                            </ul>
                        </nav>


<!--                        <div class="header-contacts">-->
<!--							<div class="header-contacts-cell h-phones">-->
<!--								<div class="header-phones">-->
<!--									<ul class="header-phones-list">-->
<!--										<li class="header-phones-item"><a href="tel:+375333333006" class="ga_ym_t">+375 33 33-33-006 (МТС)</a></li>-->
<!--										<li class="header-phones-item"><a href="tel:+375293333006" class="ga_ym_t">+375 29 33-33-006 (Velcom)</a></li>-->
<!--									</ul>-->
<!--								</div>-->
<!--							</div>-->
<!--							<div class="header-contacts-cell h-order-call clearfix">-->
<!--								<div class="order-call float_l">-->
<!--									<button class="btn small-btn red-btn order_call small-btn_mobile_size ga_ym_hf">Заказать звонок</button>-->
<!--								</div>-->
<!--                                <div class="float_l header-reviews-btn-indent">-->
<!--                                    <a href="/reviews" class="btn small-btn blue-btn small-btn_mobile_size">Отзывы</a>-->
<!--                                </div>-->
<!--							</div>-->
<!--						</div>-->
<!--						<nav class="header-nav">-->
<!--							<div class="mobile-menu-btn red_text_theme">-->
<!--								<i class="fa fa-bars" aria-hidden="true"></i>-->
<!--							</div>-->
<!--							<ul class="header-nav-list">-->
<!--								<li class="header-nav-item"><a href="/" class="header-nav-link" title="Главная">Главная</a></li>-->
<!--								<li class="header-nav-item with-dropdown">-->
<!--                                    <ul class="dropdown-nav">-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/matovi_natiajnoy_potolok">Матовые потолки</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/gliancevi_natiajnoy_potolok">Глянцевые потолки</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/art-print">Art-print</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/satinovi_natiajnoy_potolok">Сатиновые потолки</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/transparent">Транспарент</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/facturni_natiajnoy_potolok">Фактурные потлки</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/s-otverstiami">С отверстиями</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/catalog/sistemy_kreplenija">Системы крепления</a>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                    <a href="/catalog" class="header-nav-link" title="Каталог натяжных потолков">Каталог</a>-->
<!--                                </li>-->
<!--								<li class="header-nav-item"><a href="/gallery/matovi_natiajnoy_potolok" class="header-nav-link" title="Галерея натяжных потолков">Проекты</a></li>-->
<!---->
<!--								<li class="header-nav-item with-dropdown">-->
<!--                                    <ul class="dropdown-nav">-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/actions">Акции</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/about">О компании</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/construct">Конструктор</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/clients">Наши клиенты</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/reviews">Отзывы</a>-->
<!--                                        </li>-->
<!--                                        <li class="dropdown-nav-item">-->
<!--                                            <a href="/articles">Статьи</a>-->
<!--                                        </li>-->
<!--                                    </ul>-->
<!--                                    <a href="/about" class="header-nav-link" title="Клиентам компании ALTEZA">Клиентам</a>-->
<!--                                </li>-->
<!--								<li class="header-nav-item"><a href="/dilers" class="header-nav-link" title="Дилерам">Дилерам</a></li>-->
<!--								<li class="header-nav-item"><a href="/designers" class="header-nav-link" title="Дизайнерам">Дизайнерам</a></li>-->
<!--								<li class="header-nav-item"><a href="/contacts" class="header-nav-link" title="Контакты">Контакты</a></li>-->
<!--							</ul>-->
<!--						</nav>-->



					</div>
					<div class="separator header-separator"></div>
				</div>
			</div>
		</header>
		<div class="main overflow">

			<?php include 'application/users/views/'.$content_view;?>

		</div>

<!--		<aside class="fixed-nav-panel">-->
<!--			<ul>-->
				<?php
                /*
					foreach($data['aside'] as $d){
						($d['id'] == 1) ? $order = 'order_call' : $order='';
						echo "
							<li>
								<a href=\"{$d['href']}\" title='{$d['title']}' class='{$order}'>
									<img src=\"/images/nav-icons/{$d['img']}\" alt=\"{$d['title']}\" title='{$d['title']}'>
									<span>{$d['title']}</span>
								</a>
							</li>
						";
					}
                */
				?>
<!--			</ul>-->
<!--		</aside>-->

	</div>
	<footer class="footer">
		<div class="footer-top">
			<div class="section-content">
				<div class="footer-logo-buttons clearfix">
					<div class="footer-logo">
						<a href="#" class="footer-logo"><img src="/images/alteza-logo-white.png" alt=""></a>
					</div>
					<div class="footer-buttons">
						<button class="btn footer-obmer red-btn order_call measurement ga_ym_ff">Заказать обмер</button>
						<!-- <a href="/price" title="Цены на натяжные потолки ALTEZA" class="btn footer-price red-btn">Цены на натяжные потолки</a> -->
					</div>
				</div>
				<div class="footer-info" itemscope itemtype="http://schema.org/Organization">
					<div class="footer-info-item footer-catalog">
                        <h3 class="footer-info-item-title"><span itemprop="name">Натяжные потолки ALTEZA</span></h3>
						<ul class="footer-info-list">
							<li class="footer-info-list-item"><a href="/catalog/matovi_natiajnoy_potolok">Мат</a></li>
							<li class="footer-info-list-item"><a href="/catalog/gliancevi_natiajnoy_potolok">Глянец</a></li>
							<li class="footer-info-list-item"><a href="/catalog/art-print">Art-print</a></li>
							<li class="footer-info-list-item"><a href="/catalog/satinovi_natiajnoy_potolok">Сатин</a></li>
							<li class="footer-info-list-item"><a href="/catalog/transparent">Транспарент</a></li>
							<li class="footer-info-list-item"><a href="/catalog/facturni_natiajnoy_potolok">Фактурные</a></li>
							<li class="footer-info-list-item"><a href="/catalog/s-otverstiami">С отверстиями</a></li>
						</ul>
					</div>
					<div class="footer-info-item footer-clients">
						<h3 class="footer-info-item-title">Клиентам</h3>
						<ul class="footer-info-list">
							<li class="footer-info-list-item"><a href="/catalog/sistemy_kreplenija">Системы крепления</a></li>
							<li class="footer-info-list-item"><a href="/sertificates">Сертификаты</a></li>
							<li class="footer-info-list-item"><a href="/news">Новости</a></li>
							<li class="footer-info-list-item"><a href="/articles">Полезные статьи</a></li>
							<li class="footer-info-list-item"><a href="/contacts">Контакты</a></li>
							<li class="footer-info-list-item"><a href="/tc">Технический каталог</a></li>
						</ul>
					</div>
					<div class="footer-info-item footer-location" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
						<h3 class="footer-info-item-title"><a href="/contacts">Контакты</a></h3>
						<div class="footer-location-item">
							<div class="footer-location-title">Главный офис</div>
							<div class="footer-location-text">
                                <span itemprop="addressRegion">Беларусь</span>, <span itemprop="addressLocality">г. Минск</span>, <span itemprop="postalCode">223053</span><br>
                                <span itemprop="streetAddress">Логойский тракт 50, каб. 335 (Бизнес-центр "Аквабел")</span>
                                <br>Время работы: Пн - Пт с 9:00 до 18:00.
                            </div>
						</div>
					</div>
					<div class="footer-info-item footer-contacts">
						<div class="footer-contacts-text">
                            <a href="tel:+375293333006" class="ga_ym_t"><span itemprop="telephone">+375 (29) 33-33-006</span> (Velcom)</a><br>
                            <a href="tel:+375333333006" class="ga_ym_t"><span itemprop="telephone">+375 (33) 33-33-006</span> (MTS)</a><br>
                            <a href="tel:+375173352441" class="ga_ym_t"><span itemprop="telephone">+375 (17) 395-21-41</span> (Тел.)</a><br>
                            <a href="tel:+375173352441" class="ga_ym_t"><span itemprop="telephone">+375 (17) 397-21-41</span> (Тел./факс)</a><br>
							<br>
							<a href="mailto:office@alteza.by" class="ga_ym_m">office@alteza.by</a>
                            <br>
                            <br>
                            <span itemprop="description">Производство, продажа, монтаж, установка натяжных потолков.</span>
						</div>
						<div class="footer-contacts-fb-skype">
<!--							<a href="skype:alteza.by?call" class="footer-skype"><span>alteza.by</span></a>-->
							<a href="https://www.facebook.com/www.alteza.by/" class="footer-fb"><span>Натяжные потолки</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-copyright">
			<div class="section-content">
				<p class="footer-copyright-text">© 2017 Alteza. Все права защищены</p>
			</div>
		</div>
	</footer>
</div>

<div class="wrapper-mobile mobile-site">
    <?php include 'application/users/views/mobile/header.php';?>
    <?php include 'application/users/views/mobile/'.$content_view;?>
    <?php include 'application/users/views/mobile/footer.php';?>
</div>

<script src="/js/nav-selected.js"></script>
<script src="/js/trianzoom.js"></script>
<!--<script src="/js/jquery-trian-popup.js"></script>-->
<script src="/js/jquery-trian-popup-not-minified.js"></script>
<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
<!--<script src="/js/jquery.bxslider.js"></script>-->
<script src="/js/jquery.inputmask.bundle.js"></script>
<script src="/js/main.js"></script>
<style>.async-hide { opacity: 0 !important} </style>
<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
})(window,document.documentElement,'async-hide','dataLayer',4000,
{'GTM-54ZWSC6':true});</script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-77208307-1', 'auto');
	ga('require', 'GTM-54ZWSC6');
	ga('send', 'pageview');

</script>
<script type="text/javascript">
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter20785411 = new Ya.Metrika({id:20785411,
					webvisor:true,
					clickmap:true,
					trackLinks:true,
					accurateTrackBounce:true});
			} catch(e) { }
		});

		var n = d.getElementsByTagName("script")[0],
			s = d.createElement("script"),
			f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$(".ga_ym_t").on('click', function() {
		    ga('send', 'event', 'tel-info', 'ClickTel');
		    yaCounter20785411.reachGoal('ClickTelYM');
		    return true;
		   }); 
		$(".ga_ym_t").on('copy', function() {
		    ga('send', 'event', 'tel-info', 'CopyTel');
		    yaCounter20785411.reachGoal('CopyTelYM');
		    return true;
		   });
		$(".ga_ym_t").on('contextmenu', function() {
		    ga('send', 'event', 'tel-info', 'RightTel');
		    yaCounter20785411.reachGoal('RightClickTelYM');
		    return true;
		   });
		$(".ga_ym_m").on('click', function() {
		    ga('send', 'event', 'mail-info', 'ClickMail');
		    yaCounter20785411.reachGoal('ClickMailYM');
		    return true;
		   }); 
		$(".ga_ym_m").on('copy', function() {
		    ga('send', 'event', 'mail-info', 'CopyMail');
		    yaCounter20785411.reachGoal('CopyMailYM');
		    return true;
		   });
		$(".ga_ym_m").on('contextmenu', function() {
		    ga('send', 'event', 'mail-info', 'RightMail');
		    yaCounter20785411.reachGoal('RightClickMailYM');
		    return true;
		   });


		$(".ga_ym_hf").on('click', function() { // - открыть форму в хэдере
		    ga('send', 'event', 'head-info', 'HeadForm');
		    yaCounter20785411.reachGoal('HeadFormYM');
		    return true;
		   }); 
		$(".ga_ym_cf").on('click', function() { // - открыть форму на страницах каталога
		    ga('send', 'event', 'catalog-info', 'CatalogForm');
		    yaCounter20785411.reachGoal('CatalogFormYM');
		    return true;
		   }); 
		$(".ga_ym_ff").on('click', function() { // - открыть форму в футере
		    ga('send', 'event', 'foot-info', 'FootForm');
		    yaCounter20785411.reachGoal('FootFormYM');
		    return true;
		   }); 
		$(".ga_ym_zf").on('click', function() { // - открыть форму обр. звонка на главной
		    ga('send', 'event', 'zakaz-info', 'ZakazForm');
		    yaCounter20785411.reachGoal('ZakazFormYM');
		    return true;
		   }); 
		$("body").on('click', '.ga_ym_f', function() { // - нажатие по кнопке отправить в формах
		    ga('send', 'event', 'forma-info', 'Form');
		    yaCounter20785411.reachGoal('FormYM');
		    return true;
		   }); 
		$(".ga_ym_rs").on('click', function() { // - кнопка расчитать на страницах каталога
		    ga('send', 'event', 'raschet-info', 'Raschet');
		    yaCounter20785411.reachGoal('RaschetYM');
		    return true;
		   }); 
		$(".ga_ym_zo").on('click', function() { // - кнопка заказать обмер после расчета в калькуляторе
		    ga('send', 'event', 'obmer-info', 'ZakObmer');
		    yaCounter20785411.reachGoal('ZakObmerYM');
		    return true;
		   });
		$(".ga_ym_sd").on('click', function() { // - кнопка на странице стать диллером
		    ga('send', 'event', 'diller-info', 'StatDiller');
		    yaCounter20785411.reachGoal('StatDillerYM');
		    return true;
		   });
		$(".ga_ym_sk").on('click', function() { // - кнопка скачать каталога на странице дизайнеров
		    ga('send', 'event', 'sk-katalog', 'SkachatCatal');
		    yaCounter20785411.reachGoal('SkachatCatalYM');
		    return true;
		   });
	});
</script>
<script async src="//call-tracking.by/scripts/calltracking.js?18ff45b2-acf8-40ce-bf60-2e1a8d3d58df"></script>
<!-- Begin CallBox code -->
<script >
	(function() {
	var d = new Date(); 
	var em = document.createElement('script'); em.type = 'text/javascript'; em.async = true; 
		em.src = ('https:' == document.location.protocol ? 'https://' : 'http://') 
		+ 'callbox.by/js/callbox.js?' + d.getTime(); var s = document.getElementsByTagName('script')[0];
 		s.parentNode.insertBefore(em, s);
	})();
</script>
<!-- End CallBox code -->
</body>
</html>
