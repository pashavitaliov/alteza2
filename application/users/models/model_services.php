<?php

class Model_Services extends Model_Users{

    public function getServices($lang){
        return $this->query("SELECT id, title_{$lang} AS title, img_preview FROM services WHERE enable = 1");
    }

    public function getServiceItem($id,$lang){
        return $this->query("SELECT id, title_{$lang} AS title, content_{$lang} AS content, img FROM services WHERE enable = 1 AND id = {$id}")[0];
    }

    public function getSeoData($id,$lang)
    {
        return $this->query("SELECT seo_title_{$lang} AS title, seo_description_{$lang} AS description, seo_keywords_{$lang} AS keywords FROM services_seo_content WHERE service_id = {$id}")[0];
    }
}