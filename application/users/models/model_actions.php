<?php

class Model_Actions extends Model_Users{
    public function getActions()
    {
        return $this->query("SELECT id, title, img, slide, content FROM actions WHERE enable = 1");
    }
}
