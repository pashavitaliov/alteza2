<?php

class Model_Projects extends Model_Users{

    public function getProjects($lang){
        return $this->query("SELECT id, title_{$lang} AS title, img_preview FROM projects WHERE enable = 1");
    }

    public function getProjectItem($id,$lang){
        return array(
            "project_data" => $this->query("SELECT title_{$lang} AS title, content_{$lang} AS content FROM projects WHERE id ={$id} AND enable = 1")[0],
            "project_images" => $this->query("SELECT src FROM project_images WHERE pr_id = {$id}")
        );
    }

}