<?php

class Model_Main extends Model_Users{
    public function getActions()
    {
        return $this->query("SELECT id, title, img, slide FROM actions WHERE on_main_page = 1 AND enable = 1");
    }

    public function getSystems()
    {
        return $this->query("SELECT id, title, preview, main_img, video FROM systems WHERE enable = 1");
    }

    public function getMaterials()
    {
        return $this->query("SELECT id, main_title, main_page_title AS title, main_page_img AS img, start_price, url_mask, new FROM materials WHERE enable = 1");
    }

    public function getArticles()
    {
        return $this->query("SELECT id, title, img, date, url FROM articles WHERE enable = 1 ORDER BY id DESC LIMIT 6");
    }

    public function getNews()
    {
        return $this->query("SELECT id, title, date FROM news WHERE enable = 1 ORDER BY view_order ASC LIMIT 7");
    }


}