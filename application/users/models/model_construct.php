<?php

class Model_Construct extends Model_Users{
    public function getMaterialColors()
    {
        $result = [];
        if($_POST['decor'] == 'color'){
            $colors = $this->query("SELECT color_id FROM materials_colors_mapping WHERE material_id = {$_POST['id']}");
            foreach($colors as $color_id){
                array_push($result,
                        $this->query("SELECT id, title, color FROM materials_colors WHERE id = {$color_id['color_id']}")[0]
                    );
            }
        }
        return $result;
    }

//    public function filldb(){
//        foreach (range(35, 113) as $number) {
//            $this->query("INSERT INTO `materials_colors_mapping`(`material_id`, `color_id`) VALUES (2,{$number})");
//        }
//    }

}