<?php

class Model_Links extends Model_Users{

    public function markLink($link){
        if($link['visited'] == 0){
            $this->query("UPDATE links SET ip = '{$_SERVER["REMOTE_ADDR"]}',visited = 1 WHERE id = {$link['id']}");
        }
        $this->redirect($link['redirect_link']);
    }

    public function checkLink($link){
        $link_ = $this->query("SELECT id, redirect_link, visited FROM links WHERE link = '{$link}'");
        return (count($link_) > 0) ?  $link_[0] : false;
    }

    private function generateLinks(){
        for($i=0; $i<= 4000; $i++){
            $link = rand()*rand();
            $this->query("INSERT INTO links(link) VALUES ('{$link}')");
        }
    }

    private function checkLinks(){
        $links = $this->query("SELECT link FROM links");
        foreach($links as $link){
            $test = $this->query("SELECT id FROM links WHERE link = '{$link['link']}'");
            if(count($test)>1){
                print_r($test);
            }
        }
    }
}