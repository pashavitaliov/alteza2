<?php

class Model_Gallery extends Model_Users{
    public function getGallerySeoData($id)
    {
        return $this->query("SELECT seo_title, seo_description, seo_keywords FROM materials_gallery_seo_content WHERE material_id =  {$id}")[0];
    }

    public function getMaterials()
    {
        return $this->query("SELECT id, nav_title, url_mask FROM materials WHERE enable = 1");
    }

    public function getMaterialGalleryDescription($id)
    {
        return $this->query("SELECT id, main_title, nav_title, gallery_title, gallery_description, url_mask FROM materials WHERE id = {$id} AND enable =1 ")[0];
    }

    public function getGallery($id)
    {
        return $this->query("SELECT id, img FROM materials_gallery WHERE material_id =  {$id}");
    }
}

