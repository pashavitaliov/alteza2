<?php

class Model_News extends Model_Users{

    public function getNews()
    {
        return $this->query("SELECT id, title, img, date FROM news WHERE enable = 1 ORDER BY view_order ASC");
    }

    public function getNewsItem($id)
    {
        return $this->query("SELECT title, date, content FROM news WHERE id = {$id} AND enable = 1")[0];
    }
}