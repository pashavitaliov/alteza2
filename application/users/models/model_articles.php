<?php

class Model_Articles extends Model_Users{

    public function getArticles()
    {
        return $this->query("SELECT id, title, img, date, url FROM articles WHERE enable = 1  ORDER BY id DESC");
    }

    public function getArticle($id)
    {
        return $this->query("SELECT * FROM articles WHERE id = {$id} AND enable = 1")[0];
    }
}