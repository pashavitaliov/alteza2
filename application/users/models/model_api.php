<?php

class Model_Api extends Model_Users{

    public function getCalculatorData(){
        $data = $this->prepareCalculatorData();

        return[
            "data" => [
                "ceilings" => $data['ceilings'],
                "basic_operations" => $data['basic_operations']
            ],
            "offer" => (date("w") == 0 || date("w") == 6 ) ? true : false
        ];
    }

    public function getMaterialPrice($id){
        return $this->query("SELECT * FROM materials_calc WHERE id = {$id}")[0];
    }

    public function getAnglesPrice(){
        return $this->query("SELECT price FROM price_products WHERE name = 'angles' AND in_calc = 1")[0]['price'];
    }

    public function getAdditionalsPrice($id){
        return $this->query("SELECT price FROM price_products WHERE id = {$id} AND in_calc = 1")[0]['price'];
    }

    private function prepareCalculatorData(){
        $ceilings  = $this->query("SELECT id, main_page_title as title , start_price as price, calc_img as img FROM materials WHERE in_calc = 1 AND enable = 1");
        $basic_operations = $this->query("SELECT id,  calc_title as title FROM price_products WHERE in_calc = 1 AND calc_title IS NOT NULL");

        foreach($ceilings as $ceiling)
            $ceiling['id'] = strval($ceiling['id']);

        foreach($basic_operations as $basic_operation)
            $basic_operation['id'] = strval($basic_operation['id']);

        return [
            "ceilings" => $ceilings,
            "basic_operations"=> $basic_operations
        ];
    }
}
