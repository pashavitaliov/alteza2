<?php

class Model_Reviews extends Model_Users {
    public function getReviews(){

        $reviews = $this->query("SELECT reviews.id, reviews.review, reviews.name, reviews.created_at, r_c.comment, r_c.created_at AS comment_created_at, r_c.name AS comment_author FROM reviews LEFT JOIN reviews_comments r_c ON r_c.review_id = reviews.id WHERE reviews.moderated = 1  ");
        $comments = [];
        $result = [];
        $sortedReviews = [];

        foreach($reviews as $review) {
            if (!isset($comments[$review['id']])) {
                if ($review['comment']) {
                    $comments[$review['id']] = [
                        [
                            "comment" => $review['comment'],
                            "created_at" => $review['comment_created_at'],
                            "comment_author" => $review['comment_author']
                        ]
                    ];
                } else {
                    $comments[$review['id']] = [];
                }
            } else {
                if ($review['comment']) {
                    array_push($comments[$review['id']], [
                        "comment" => $review['comment'],
                        "created_at" => $review['comment_created_at'],
                        "comment_author" => $review['comment_author']
                    ]);
                }
            }

            if (!isset($sortedReviews[$review['id']])) {
                $sortedReviews[$review['id']] = $review;
            }

        }

        foreach($sortedReviews as $review) {
            array_push($result, [
                "id" =>  $review["id"],
                "review" =>  $review["review"],
                "name" =>  $review["name"],
                "created_at" =>  $review["created_at"],
                "comments" =>  $comments[$review["id"]]
            ]);
        }

        return $result;
    }

    public function createReview($review){
        $this->query("INSERT INTO reviews (review, name) VALUES ('{$review['review']}', '{$review['name']}')");
    }
}
