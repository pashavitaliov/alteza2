<?php

class Model_Pages extends Model_Users{
    public function getPage($id){
        return [
            'pageData'       => $this->query("SELECT title, h_title, seo_title, seo_description, seo_keywords, url, template, use_breadcrumbs FROM pages WHERE id = {$id}")[0],
            'pageContent'   => $this->query("SELECT content, mobile_content FROM pages_content WHERE p_id = {$id}")[0]
        ];
    }
}