<?php

class Model_Catalog extends Model_Users{
    public function getMaterialSeoData($id)
    {
        return $this->query("SELECT seo_title, seo_description, seo_keywords FROM materials_seo_content WHERE material_id = {$id}")[0];
    }

    public function getMaterials()
    {
        return $this->query("SELECT id, nav_title,url_mask FROM materials WHERE enable = 1");
    }

    public function getMaterialMainData($id)
    {
        return $this->query("SELECT id, main_title, nav_title, content, decor, inter_img, url_mask FROM materials WHERE enable = 1 AND id = {$id}")[0];
    }

    public function getMaterialMainImages($id)
    {
        return $this->query("SELECT id, img FROM materials_main_images WHERE material_id = {$id}");
    }

    public function getMaterialDescImages($id)
    {
        return $this->query("SELECT id, img, big FROM materials_desc_images WHERE material_id = {$id} ORDER BY big DESC");
    }

    private function getMaterialDecorIds($id,$flag)
    {
        $result = array();
        if($flag == "color")
        {
            $result = $this->query("SELECT color_id AS id FROM materials_colors_mapping WHERE material_id = {$id}");
        }else
        {
            if($flag == "image")
            {
                $result = $this->query("SELECT img_id AS id FROM materials_print_images_mapping WHERE material_id = {$id}");
            }
        }
        return $result;
    }

    public function getMaterialDecor($id,$flag)
    {
        $result = [];
        $ids = $this->getMaterialDecorIds($id,$flag);

        if($flag == "color")
        {
            foreach($ids as $id_)
            {
                array_push($result,
                    $this->query("SELECT id, title, color, width, manufacturer FROM materials_colors WHERE id = {$id_['id']} AND enable = 1")[0]
                );
            }
        }

        if($flag == "image")
        {
            foreach($ids as $id_)
            {
                array_push($result,
                    $this->query("SELECT id, title, img, width, manufacturer FROM materials_print_images WHERE enable = 1 AND id = {$id_['id']}")[0]
                );
            }
        }

        return $result;
    }

    public function getSystems()
    {
        return $this->query("SELECT id, title, content, main_img, inter_img, video FROM systems WHERE enable = 1");
    }

    public function getColorData()
    {
        ($_POST['type'] == 'color') ? $table = 'materials_colors' : $table = 'materials_print_images';
        return $this->query("SELECT title, width, manufacturer FROM {$table} WHERE enable = 1 AND id = {$_POST['id']}")[0];
    }

    public function test(){
//        $arr = ['1s.jpg','2s.jpg','3s.jpg','4s.jpg','5s.jpg','6s.jpg','7s.jpg','8s.jpg'];
//
//        foreach($arr as $ar){
//
//            $this->query("INSERT INTO `materials_gallery`(`material_id`, `img`) VALUES (4,'{$ar}')");
//
//        }
//        $arr = $this->query("SELECT `id` FROM `materials_colors` ");
//        foreach($arr as $ar){
//            $this->query("INSERT INTO `materials_colors_mapping`(`material_id`, `color_id`) VALUES (7,{$ar['id']})");
//        }


        $files = scandir("images/materials/decor-slider-images/new/sm");
        foreach($files as $key => $file){
//            echo substr($file, -3)."<br>";
//            echo substr($file,0, -4)."<br>";
//            echo "----<br>";
            $name = substr($file,0, -4);


            if(substr($file, -3) == "jpg"){

                $this->query("INSERT INTO `materials_print_images`(`title`, `img`,`width`, `manufacturer`, `enable`) VALUES ('{$name}','{$file}','1,3; 3,20','Китай(MSD)',1)");

                $id  = $this->query("SELECT id FROM materials_print_images ORDER BY id DESC LIMIT 1")[0];
                $this->query("INSERT INTO `materials_print_images_mapping`(`material_id`, `img_id`) VALUES (6,{$id['id']})");
            }
        }

//        $arr = $this->query("SELECT `id` FROM `materials_print_images`");
//        foreach($arr as $ar){
//            $this->query("INSERT INTO `materials_print_images_mapping`(`material_id`, `img_id`) VALUES (6,{$ar['id']})");
//        }
    }

    public function test1(){
        $arr = [1,2,3,8,6,7];

        /*
         * 1 - Заказать звонок
         * 2- Прайс-лист
         * 3- Каталог
         * 4 - кальк
         * 5  -галлерея
         * 6 - клиенты
         * 7 -сертификаты
         * 8 - конструктор
         * 9 - акции
         * 10 -тех каталог
         * */

        foreach($arr as $ar){
            $this->query("INSERT INTO `navigation_mapping`(`nav_item_id`, `page_id`) VALUES ({$ar},17)");
        }
    }
}
