<?php

class Controller_Sertificates extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Sertificates("certificates");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"page_title" =>$page->getName(),
			"sertificates" => $this->model->getSertificates(),
			"pageTitle"    => $page->getHTitle(),
			"content" => $this->model->getContent($page->getId())['content']
		);
		$this->view->generate('users','sertificates_view.php', 'template_view.php',$data);
	}



}