<?php

class Controller_Services extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Services();
	}

	public function action_index()
	{
		$page = $this->model->setPage("services","ru");
		$data["seo_data"] = $page->getSeoData();
		$data["services"] = $this->model->getServices($page->getLang());
		$data["lang"] = $page->getLang();
		$data["page_content"] = $page->getContent();
		$this->view->generate('users','services_view.php', 'template_view.php',$data,'sidebar_services_view.php');
	}

	public function action_show_service($id)
	{
		$page = $this->model->setPage("service_item","ru");
		$seo_data = $this->model->getSeoData($id,$page->getLang());
		$page->setSeoData(
			$seo_data['title'],
			$seo_data['description'],
			$seo_data['keywords']
		);
		$data["seo_data"] = $page->getSeoData();
		$data["lang"] = $page->getLang();
		$data["services"] = $this->model->getServices($page->getLang());
		$data["service_item"] = $this->model->getServiceItem($id,$page->getLang());
		$this->view->generate('users','service_item_view.php', 'template_view.php',$data,'sidebar_services_view.php');
	}

	public function action_en()
	{

	}

	private function get_services_data(){

	}

}