<?php

class Controller_Catalog extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Catalog("material_page");
	}

	public function action_index()
	{
		$this->action_404();
	}

	public function action_show_material($id)
	{
		$page = $this->model->setPage();
		$seo_data = $this->model->getMaterialSeoData($id);
		$material = $this->model->getMaterialMainData($id);
		$page->setSeoData(
			$seo_data['seo_title'],
			$seo_data['seo_description'],
			$seo_data['seo_keywords']
		);
		($material['inter_img'] == 1) ? $desc_images = $this->model->getMaterialDescImages($id) : $desc_images = false;
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"navigation" => $this->model->getMaterials(),
			"material" => $material,
			"material_main_images" => $this->model->getMaterialMainImages($id),
			"material_desc_images" => $desc_images,
			"material_decor" => $this->model->getMaterialDecor($id,$material["decor"])
		);
		$this->view->generate('users','catalog_view.php', 'template_view.php',$data);

		//$this->model->test();
	}


	public function action_show_systems()
	{
		$this->model = new Model_Catalog("systems");
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"systems" => $this->model->getSystems(),
			"pageTitle"    => $page->getHTitle(),
			"content" => $this->model->getContent($page->getId())['content']
		);
		$this->view->generate('users','systems_view.php', 'template_view.php',$data);
	}

	public function action_get_color_data()
	{
		echo json_encode($this->model->getColorData());
	}
}