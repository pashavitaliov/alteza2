<?php

class Controller_404 extends Controller_Users
{
	
	function action_index()
	{
		header('HTTP/1.1 404 Not Found');
		$this->view->generate('users','404_view.php', 'template_view.php');
	}

}
