<?php

class Controller_For_Clients extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();

	}

	public function action_index()
	{
		$this->model = new Model_For_Clients("for_clients");
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside()
		);
		$this->model->setVisitedUser();
		$this->view->generate('users','for_clients_view.php', 'template_view.php',$data);
	}
}

