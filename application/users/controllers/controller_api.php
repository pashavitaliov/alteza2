<?php

class Controller_Api extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Api();
	}

	public function action_index()
	{
		$this->action_404();

	}
	public function action_getCalculatorData(){
		echo json_encode($this->model->getCalculatorData(), JSON_UNESCAPED_UNICODE);
	}


	public function action_calculate(){
		$request = json_decode(file_get_contents("php://input"));

		$squareSumm = 0;
		$quantityOfAngles = 0;
		$materialTotalPrice = 0;
		$anglesTotalPrice = 0;
		$additionalsTotalPrice = 0;
		$totalPrice = 0;

		foreach ($request as $request_) {
			$squareSumm += intval($request_->square);
			if($request_->quantityOfAngle > 4){
				$quantityOfAngles += $request_->quantityOfAngle-4;
			}
		}
		($squareSumm > 25) ? $key = "discount_price" : $key = "price";
		foreach ($request as $request_) {
			$price = $this->model->getMaterialPrice($request_->ceilingId)[$key];
			$materialTotalPrice += $price*$request_->square;
			if($request_->ceilingId == 3){
				$price = $this->model->getMaterialPrice(1)[$key];
				$materialTotalPrice += $price*$request_->square;
			}
			foreach($request_->additionals as $id=>$additionals){
				$additionalsTotalPrice += floatval($this->model->getAdditionalsPrice($id))*floatval($additionals);
			}
		}
		
		$anglesTotalPrice = floatval($this->model->getAnglesPrice())*floatval($quantityOfAngles);

		$totalPrice = $anglesTotalPrice + $materialTotalPrice + $additionalsTotalPrice;

		echo json_encode(["data" => [
			"value" => $totalPrice,
			"isSatisfied" =>  ($totalPrice >= 180) ? true : false
		]]);
	}
}