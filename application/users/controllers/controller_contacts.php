<?php

class Controller_Contacts extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Contacts("contacts");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"page_title" =>$page->getName(),
			"content" => $this->model->getContent($page->getId())
		);
		$this->view->generate('users','contacts_view.php', 'template_view.php',$data);
	}



}