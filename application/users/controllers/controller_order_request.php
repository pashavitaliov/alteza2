<?php

class Controller_Order_Request extends Controller_Users
{
	private $mail;

	public function __construct(){
		parent::__construct();
		$this->model = new Model_Users();
		$this->mail = new Mail();
	}

	public function action_email(){
		$this->model->insertCall();
		$this->mail->setTo(["akaynt100@tut.by","office@alteza.by","altezaminsk@gmail.com","tv@alteza.by"]);
		$this->mail->setFromName("Заказ звонка ALTEZA");
		$subj = "Заказ звонка с сайта";
		$content = "<b>Имя:</b> {$_POST['name']}<br><b>Телефон:</b> {$_POST['phone']}";
		$this->mail->send($subj , $content);
	}

	public function action_email_from_calculator(){
		$request = json_decode(file_get_contents("php://input"));
		$this->model->insertCall();
		$this->mail->setTo(["akaynt100@tut.by","office@alteza.by","altezaminsk@gmail.com","tv@alteza.by"]);
		$this->mail->setFromName("Заказ обмера ALTEZA (калькулятор)");
		$subj = "Заказ обмера с сайта (калькулятор)";
		$content = "<b>Имя:</b> {$request->name}<br><b>Телефон:</b> {$request->phone}<br><b>Скидка - 5%</b>";
		$this->mail->send($subj , $content);
	}

	public function action_diller_order(){
		$this->mail->setTo(["viktor@alteza.by"]);
		$this->mail->setFromName("Запрос 'Стать дилером' ALTEZA");
		$subj = "Запрос 'Стать дилером' с сайта";
		$content = "<b>Имя:</b> {$_POST['name']}<br>
					<b>Компания:</b> {$_POST['company']}<br>
					<b>Телефон:</b> {$_POST['phone']}<br>
					<b>Сообщение:</b> {$_POST['message']}";		

		if($this->validateRecaptcha()){
			$this->mail->send($subj , $content);
		}
		$this->model->redirect("alteza.by/dilers");
	}

    public function action_add_diller(){
        $this->mail->setTo(["john.zhabko@alteza.by", "viktor@alteza.by"]);
        $this->mail->setFromName("Регистрация дилера ALTEZA");
        $subj = "Регистрация дилера на сайте";
        $content = "Новый дилер был зарегестрирован в системе";

        if($this->validateRecaptcha()){
            $this->model->insertDiller();
            $this->mail->send($subj , $content);
            $this->model->redirect("alteza.by/add-diler?add-diler-success=true");
        } else {
            $this->model->redirect("alteza.by/add-diler?recaptcha_error=true");
        }
    }

	private function validateRecaptcha() {
		$privatekey = "6LeImC4UAAAAAOYHuBZxTJzuZmaKqAdmVamfXseG";
		$captchaResp = $_POST["g-recaptcha-response"];

		if (!$captchaResp) {
			return false;
		}
		$resp = json_decode(file_get_contents(
			"https://www.google.com/recaptcha/api/siteverify?secret=".$privatekey."&response=".$captchaResp."&remoteip=".$_SERVER['REMOTE_ADDR']
		));
        return $resp->success;
	}
}
