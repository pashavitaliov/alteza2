<?php

class Controller_Gallery extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Gallery("gallery_page");
	}

	public function action_index()
	{
		$this->action_404();
	}

	public function action_show_gallery($id)
	{
		$page = $this->model->setPage();
		$seo_data = $this->model->getGallerySeoData($id);
		$page->setSeoData(
			$seo_data['seo_title'],
			$seo_data['seo_description'],
			$seo_data['seo_keywords']
		);
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"navigation" => $this->model->getMaterials(),
			"gallery_desc" => $this->model->getMaterialGalleryDescription($id),
			"gallery" => $this->model->getGallery($id)
		);
		$this->view->generate('users','gallery_view.php', 'template_view.php',$data);
	}
}