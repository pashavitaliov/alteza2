<?php

class Controller_Projects extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Projects();
	}

	public function action_index()
	{
		$page = $this->model->setPage("projects","ru");
		$data["seo_data"] = $page->getSeoData();
		$data["projects"] = $this->model->getProjects($page->getLang());
		$data["lang"] = $page->getLang();
		$data["page_content"] = $page->getContent();
		$this->view->generate('users','projects_view.php', 'template_view.php',$data,'sidebar_projects_view.php');
	}

	public function action_show($id)
	{
		$page = $this->model->setPage("project_item","ru");
		$data["lang"] = $page->getLang();
		$data["project"] = $this->model->getProjectItem($id,$page->getLang());
		$this->view->generate('users','project_item_view.php', 'template_view.php',$data,'sidebar_projects_view.php');
	}

	public function action_en()
	{

	}

	private function get_services_data(){

	}

}




/*
 * вынести в один метод получение инфы (в зависимости от языка)!!! посредством выноса data как private поле класса
 *
 * */