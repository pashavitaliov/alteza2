<?php

class Controller_Clients extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Clients("clients");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"clients" => $this->model->getClients(),
			"pageTitle"    => $page->getHTitle(),
			"content" => $this->model->getContent($page->getId())['content']
		);
		$this->view->generate('users','clients_view.php', 'template_view.php',$data);
	}
}