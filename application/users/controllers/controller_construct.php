<?php

class Controller_Construct extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Construct("construct");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"pageTitle"    => $page->getHTitle(),
			"content" => $this->model->getContent($page->getId())['content']
		);
		$this->view->generate('users','construct_view.php', 'template_view.php',$data);
	}

	public function action_get_material_colors()
	{
		echo json_encode($this->model->getMaterialColors());
	}
}

