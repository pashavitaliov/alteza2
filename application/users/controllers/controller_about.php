<?php

class Controller_About extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_About("about");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"page_title" =>$page->getName(),
			"content" => $this->model->getContent($page->getId())
		);
		$this->view->generate('users','about_view.php', 'template_view.php',$data);
	}

}
