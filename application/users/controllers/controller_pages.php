<?php
class Controller_Pages extends Controller_Users{

    public function __construct(){
        parent::__construct();
        $this->model = new Model_Pages();
    }

    public function action_index(){
        $this->action_404();
    }

    public function action_generatePage($id){
        $page = $this->model->getPage($id);
        $data = array(
            "seo_data" => [
                "title"         => $page['pageData']['seo_title'],
                "desc"          => $page['pageData']['seo_description'],
                "keywords"      => $page['pageData']['seo_keywords']
            ],
            "title"             => $page['pageData']['title'],
            "pageTitle"         => $page['pageData']['h_title'],
            "url"               => $page['pageData']['url'],
            "content"           => WidgetParser::parse($page['pageContent']['content']),
            "mobile_content"    => WidgetParser::parse($page['pageContent']['mobile_content']),
            "aside"             => $this->model->getPageDefaultAside(),
            "use_breadcrumbs"   => $page['pageData']['use_breadcrumbs'],
            "main_page_flag"    => false,
        );

        $this->view->generate('users','common_page_view.php', $page['pageData']['template'],$data);
    }
}