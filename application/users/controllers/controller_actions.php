<?php

class Controller_Actions extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Actions("actions");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = [
			"seo_data"  => $page->getSeoData(),
			"aside"     => $page->getAside(),
			"actions"   => $this->model->getActions(),
            "pageTitle"    => $page->getHTitle(),
            "content" => $this->model->getContent($page->getId())
		];
		$this->view->generate('users','actions_view.php', 'template_view.php',$data);
	}
}