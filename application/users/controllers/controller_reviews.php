<?php

class Controller_Reviews extends Controller_Users {
	public function __construct(){
		parent::__construct();
		$this->model = new Model_Reviews("reviews");
	}

	public function action_index(){
		$page = $this->model->setPage();
		$data = [
			"seo_data"  => $page->getSeoData(),
			"aside"     => $this->model->getPageDefaultAside(),
			"reviews"   => $this->model->getReviews(),
            "pageTitle" => $page->getHTitle()
		];
		$this->view->generate('users','reviews_view.php', 'template_view.php',$data);
	}

    public function action_create(){
        $review = [
            'review'    => strip_tags($_POST['review']),
            'name'      => strip_tags($_POST['name'])
        ];
        $this->model->createReview($review);
        $this->model->redirect("alteza.by/reviews");
    }

}