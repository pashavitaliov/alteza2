<?php

class Controller_Calc extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Calc("calc");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"pageTitle"    => $page->getHTitle(),
			"content" => $this->model->getContent($page->getId())['content']
		);
		$this->view->generate('users','calc_view.php', 'template_view.php',$data);
	}

}
