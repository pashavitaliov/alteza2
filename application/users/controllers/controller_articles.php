<?php

class Controller_Articles extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Articles("articles");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"articles" => $this->model->getArticles(),
			"pageTitle"    => $page->getHTitle(),
			"content" => $this->model->getContent($page->getId())['content']
		);
		$this->view->generate('users','articles_view.php', 'template_view.php',$data);
	}

	public function action_show($id)
	{
		$this->model = new Model_Articles("article");
		$page = $this->model->setPage();
		$article = $this->model->getArticle($id);
		$data = [
			"seo_data" => [
			    "title"         => $article["seo_title"] != "" ? $article["seo_title"] : $article["title"],
                "desc"          => $article["seo_description"] != "" ? $article["seo_description"] : $article["title"],
                "keywords"      => "",
                "page_title"    => "Статьи"
            ],
			"aside" => $page->getAside(),
			"article" => $article
		];
		$this->view->generate('users','article_view.php', 'template_view.php',$data);
	}
}

