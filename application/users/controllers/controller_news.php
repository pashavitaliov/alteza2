<?php

class Controller_News extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_News("news");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"news" => $this->model->getNews(),
		);
		$this->view->generate('users','news_view.php', 'template_view.php',$data);
	}

	public function action_show($id)
	{
		$this->model = new Model_News("news_item");
		$page = $this->model->setPage();
		$news_item = $this->model->getNewsItem($id);
		$page->setSeoTitle($news_item["title"]);

		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"news" => $news_item
		);
		$this->view->generate('users','news_item_view.php', 'template_view.php',$data);
	}
}

