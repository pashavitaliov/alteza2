<?php

class Controller_Links extends Controller_Users
{
    public function __construct()
    {
        parent::__construct();
        $this->model = new Model_Links();

    }

    public function action_index()
    {
        $this->action_404();
    }

    public function action_u($link){
        $result = $this->model->checkLink($link);
        if($result){
            $this->model->markLink($result);

        }else{
            $this->action_404();
        }
    }

    private function action_generate(){
        //$this->model->checkLinks();
    }
}

