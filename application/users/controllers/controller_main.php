<?php

class Controller_Main extends Controller_Users
{
	public function __construct()
	{
		parent::__construct();
		$this->model = new Model_Main("main");
	}

	public function action_index()
	{
		$page = $this->model->setPage();
		$data = array(
			"seo_data" => $page->getSeoData(),
			"aside" => $page->getAside(),
			"page_title" =>$page->getName(),
			"actions" => $this->model->getActions(),
			"systems" => $this->model->getSystems(),
			"materials" => $this->model->getMaterials(),
			"articles" => $this->model->getArticles(),
			"news" => $this->model->getNews(),
			"main_page_flag" => true,
			"content" => $this->model->getContent($page->getId())['content']
		);
		$this->view->generate('users','main_view.php', 'template_view.php',$data);
	}
}

