<?php
/**
 * Подключаем файлы ядра
 */
require_once 'core/vars.php';
require_once 'core/db.php';
require_once 'core/image-upload.php';
require_once 'core/page.php';
require_once 'core/model.php';
require_once 'core/model_admin.php';
require_once 'core/model_users.php';
require_once 'core/controller.php';
require_once 'core/controller_admin.php';
require_once 'core/controller_users.php';
require_once 'core/view.php';
require_once 'core/route.php';
require_once 'core/email.php';
require_once 'core/widgets-parser.php';

/**
 * Устанавливаем соединение с БД
 */
DB::start();

/**
 * Запускаем маршрутизатор
 */
Route::start();
