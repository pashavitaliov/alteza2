$(document).ready(function(){

    setUpForm();

});

function setUpForm(){
    var form = $('.add-dillers-form');
    var submitBtn = form.find('.add-diller-form-submit-btn');
    var phoneField = form.find('.phone-mask');

    phoneField.mask('8-000 0000000', {
        placeholder: '8-0** *******',
        onChange: handleMaskChange
    });

    phoneField.on('focus', function() {
        var el = $(this);

        if (el.val()) {
            return;
        }

        el.val('8-0');
    });

    function handleMaskChange(value) {
        var isValid = Inputmask.isValid(value, { mask: '8-099 9999999' });
        submitBtn.attr('disabled', !isValid);
    }
}