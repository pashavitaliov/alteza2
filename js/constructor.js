var Constructor = function(){
    var canvas_elems = {},
        picker = $.farbtastic(".colors"),
        controls = {
            "colors_widget" : {
                "start_btn" : {
                    "self" : $(".color_wid"),
                    "content_class" : ".controls_elem_content_color"
                },
                "el" : $(".colors")
            },
            "ceiling_color_widget" : {
                "start_btn": {
                    "self" : $(".ceil_color_wid"),
                    "content_class" : ".controls_elem_content_color"
                },
                "el" : $(".ceil_colors_wrap"),
                "inner_controls_parent" : $(".ceil_colors"),
                "inner_controls" : $(".color_el"),
                "inner_controls_desc" : {
                    "current_color" : $(".color_desc .current_color"),
                    "current_color_title" : $(".color_desc .current_color_title")
                }
            },
            "textures_widget" : {
                "start_btn": {
                    "self" : $(".texture_wid"),
                    "content_class" : ".controls_elem_content"
                },
                "el" : $(".textures_list"),
                "inner_controls" : $(".texture_list_el")
            }
        },
        other_els = {
            "floor" : $("#floor"),
            "ceiling_c" : $("#c-ceiling"),
            "close_widget" : $(".close_widget")
        },
        coordArray = {
            'floor' :
            {
                "el" :"c-floor",
                "coords" :[
                    [0,746], [0,517], [150,450], [680,450],[1220,746]
                ]
            },
            'wall_main' :
            {
                "el" :"c-wall-main",
                "coords" :[
                    [0,520], [0,98], [180,184], [695,184],[695,435],[180,435]
                ]
            },
            'wall_el_1' :
            {
                "el" :"c-wall-el1",
                "coords" :[
                    [645,184], [645,183.5], [695,175],[840,96],[840,122],[695,193],[695,184]
                ]
            },
            'wall_el_2' :
            {
                "el" :"c-wall-el2",
                "coords" : [
                    [730,477],[850,542],[850,518],[730,460]
                ]
            },
            'ceiling' :
            {
                "el" :"c-ceiling",
                "coords" : [
                    [0,5],[233,166],[591,166], [739,1], [0,0]
                ]
            }
        };

     var PolygonCanvas = function(canvasid) {
        var width = 1000,
            height = 600;
        this.canvas = document.getElementById(canvasid);
        this.obCanvas = null;

        if (this.canvas != null) {
            this.canvas.width = width;
            this.obCanvas = this.canvas.getContext('2d');
        }

        this.Polygon = function(arr)
        {
            if (this.obCanvas == null) return false;

            this.obCanvas.beginPath();
            for (var i = 0; i < arr.length; i++) {
                if (i == 0) this.obCanvas.moveTo(arr[i][0], arr[i][1]);
                else this.obCanvas.lineTo(arr[i][0], arr[i][1]);
            }
        };

        this.SetBgColor = function(bgcolor)
        {
            if (this.obCanvas == null) return false;

            this.obCanvas.fillStyle = bgcolor;
            this.obCanvas.fill();
        }
    };

    var setEvents = function(){
        controls.colors_widget.start_btn.self.click(setColorsWidget);
        controls.ceiling_color_widget.start_btn.self.click(setCeilingColorsWidget);
        controls.textures_widget.start_btn.self.click(setTextureWidget);
        controls.ceiling_color_widget.inner_controls.click(setCeilingCanvasColor);
        controls.textures_widget.inner_controls.click(setCeiling);
        other_els.close_widget.click(closeWidget);
    };

    var setCeiling = function () {
        var texture_id = parseInt($(this).attr("data-id"));
        setCeilingCanvasOpacity(texture_id);
        getColors(texture_id);
        controls.textures_widget.start_btn.self.find(controls.textures_widget.start_btn.content_class).text($(this).text());
        $(this).parent().css({"display": "none"});
    };

    var getColors = function (id) {
        http({
            id: id,
            decor: 'color'
        },'get_material_colors',formColorsTmpl);
    };

    var formColorsTmpl = function (data_) {
        var data = JSON.parse(data_),
            parent = controls.ceiling_color_widget.inner_controls_parent;

        parent.html("");
        data.forEach(function(item){
            var div = $("<div class = 'color_el' />");
            div.attr("data-color",item.color).attr("data-title", item.title).css({"background": item.color}).appendTo(parent);
        });

        controls.ceiling_color_widget.inner_controls = $(".color_el");
        controls.ceiling_color_widget.inner_controls.click(setCeilingCanvasColor);
    };

    var setCeilingCanvasOpacity = function(texture_id){
        var val = 0.5;
        if(texture_id == 1){
            val = 1;
        }
        if(texture_id == 4){
            val = 0.9
        }
        other_els.ceiling_c.css({"opacity": val});
    };

    var setCeilingCanvasColor = function () {
        var color = $(this).attr("data-color"),
            title = $(this).attr("data-title");
        setDescriptionContent(color,title);
        canvas_elems.ceiling.SetBgColor(color);
    };

    var setDescriptionContent = function(color, title){
        controls.ceiling_color_widget.inner_controls_desc.current_color.css({"background" : color});
        controls.ceiling_color_widget.inner_controls_desc.current_color_title.text(title);
        controls.ceiling_color_widget.start_btn.self
            .find(controls.ceiling_color_widget.start_btn.content_class)
            .css({"background" : color});
    };

    var setTextureWidget = function(){
        hideControls();
        showElement(controls.textures_widget.el, $(this), 210);
    };

    var setCeilingColorsWidget = function(){
        hideControls();
        showElement(controls.ceiling_color_widget.el, $(this), 222);
    };

    var setColorsWidget = function(){
        var that = $(this);
        hideControls();
        showElement(controls.colors_widget.el, that, 230);
        if(that.attr("data-constr-elem") != "walls"){
           other_els.floor.attr("src","/images/2construct-base-nocolor.png");
            picker.linkTo(function(color){
                canvas_elems.floor.SetBgColor(color);
                that.find(controls.colors_widget.start_btn.content_class).css({"background-color": color});
            });
        }else{
            picker.linkTo(function(color){
                canvas_elems.wall_main.SetBgColor(color);
                canvas_elems.wall_el_1.SetBgColor(color);
                canvas_elems.wall_el_2.SetBgColor(color);
                that.find(controls.colors_widget.start_btn.content_class).css({"background-color": color});//+
            });
        }
    };

    var closeWidget = function () {
        $(this).parent().css({"display":"none"});
    };

    var http = function(data, url, success){
        $.ajax({
            type: "POST",
            url: "/construct/" + url,
            data: data,
            success: success
        });
    };

    var hideControls = function(){
        controls.colors_widget.el.css({"display" : "none"});
        controls.ceiling_color_widget.el.css({"display" : "none"});
        controls.textures_widget.el.css({"display" : "none"});
    };

    var showElement = function (el, position_el, margin_val) {
        el.css({"top": position_el.offset().top - margin_val, "display": "block"});
    };

    this.init = function(){
        var colors_close = $('<div class="close_widget"/>').appendTo(controls.colors_widget.el);
        for (var key in coordArray) {
            var p = new PolygonCanvas(coordArray[key].el);
            p.Polygon(coordArray[key].coords);
            canvas_elems[key] = p;
        }
        other_els.ceiling_c.css({"opacity": 0.8});
        canvas_elems.ceiling.SetBgColor("#eba726");
        setEvents();
        getColors(1);
        colors_close.click(closeWidget);
    };
};

var constructor = new Constructor().init();


