var ColorCursor = function(type){
    var properties = {
        "title": $('.property-title'),
        "width": $('.property-width span'),
        "manufacturer": $('.property-manufacturer span')
    };

    var clickBind = function(){
        $(".color").click((type == 'color') ? clickColor : clickImg);
    };

    var clickColor = function(){
        var cursor = $("<div class='color-cursor' />"),
            coords = $(this).offset(),
            color = $(this).css("background-color"),
            id = $(this).attr("data-color-id");

        $(".color-cursor").remove();
        $("body").append(cursor);
        cursor.css({
            "top": coords.top - 90,
            "left": coords.left,
            "background-color": color
        });
        http(id);
    };

    var clickImg = function(){
        var id = $(this).attr("data-img-id");
        http(id);
    };

    var http = function(id){
        $.ajax({
            type: "POST",
            url: "/catalog/get_color_data",
            data: {
                id: id,
                type: type
            },
            success: success
        });
    };

    var success = function(data){
        var data_ = JSON.parse(data);
        properties.title.html(data_.title);
        properties.width.html(data_.width);
        properties.manufacturer.html(data_.manufacturer);
    };

    this.init = function(){
        clickBind();
        $(".color:first-child").click();
    };
};
