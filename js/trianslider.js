(function($) {
    $.fn.trianSlider = function(settings_) {
        var el = this,
            slider = {controls:{}},
            slides = el.children("li"),
            slides_count = slides.length,
            selected_ = {},
            settings = {
                "markup": {
                    "wrapper" : '<div class="trian-controls" />',
                    "next" : '<a class="slide-next" href="" />',
                    "prev" : '<a class="slide-prev" href="" />'
                }
            };

        var setControls = function(){
            if((typeof settings_) != 'undefined') settings = settings_;

            slider.controls.el = $(settings.markup.wrapper);
            slider.controls.next = $(settings.markup.next);
            slider.controls.prev = $(settings.markup.prev);
            slider.controls.next.bind('click touchend', clickNextBind);
            slider.controls.prev.bind('click touchend', clickPrevBind);
            slider.controls.el.append(slider.controls.prev).append(slider.controls.next);
            el.parent().append(slider.controls.el);
        };

        var clickNextBind = function(e){
            el.goToNextSlide();
            e.preventDefault();
        };

        var clickPrevBind = function(e){
            el.goToPrevSlide();
            e.preventDefault();
        };

        var setCurrentSlide = function(settings){
            $(selected_.el).css({'display': 'none'});
            $(settings.el).css({'display': 'block'});
            selected_.el = settings.el;
            selected_.index = settings.index;
        };

        var setPaginationItem = function(){
            var attr = $(selected_.el).attr("data-pagination");
            if(typeof attr != 'undefined'){
                $(".ps-nav-item:not(.ps-nav-flag)").removeClass("ps-nav-active");
                $("[data-pagination-el='"+attr+"']").addClass("ps-nav-active");
            }
        };

        var init = function(){
            slides.each(function(i,el_){
                if(i!=0){
                    $(el_).css({'display': 'none'});
                }else{
                    selected_.el = el_;
                    selected_.index = i;
                }
            });
            slider.last = slides_count;
            setControls();
        };

        el.goToNextSlide = function(){
            var next_slide_index = selected_.index + 1,
                next_slide = slides[next_slide_index];

            if(next_slide_index == slider.last){ next_slide_index = 0; next_slide = slides[next_slide_index];}
            setCurrentSlide({
                el : next_slide,
                index : next_slide_index
            });
            setPaginationItem();
        };

        el.goToPrevSlide = function(){
            var prev_slide_index = selected_.index - 1,
                prev_slide = slides[prev_slide_index];

            if(prev_slide_index < 0){ prev_slide_index = slider.last - 1; prev_slide = slides[prev_slide_index];}
            setCurrentSlide({
                el : prev_slide,
                index : prev_slide_index
            });
            setPaginationItem();
        };

        init();

        return this;
    };
})(jQuery);