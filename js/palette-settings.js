(function(){
    var palette = $("#palette");

    palette.mThumbnailScroller({
        type: "click"
    });
    $('.next-palette').click(function(){
        palette.mThumbnailScroller("scrollTo", '-=150');
        $(".color-cursor").remove();
    });
    $('.prev-palette').click(function(){
        palette.mThumbnailScroller("scrollTo", '+=150');
        $(".color-cursor").remove();
    });
    $('.next-palette-all').click(function(){
        palette.mThumbnailScroller("scrollTo", 'right');
        $(".color-cursor").remove();
    });
    $('.prev-palette-all').click(function(){
        palette.mThumbnailScroller("scrollTo", 'left');
        $(".color-cursor").remove();
    });
})();