(function() {
  'use strict';

  angular
    .module('calc')
    .controller('CalculatorController', CalculatorController);

  CalculatorController.$inject = ['CalculatorService'];

  function CalculatorController(CalculatorService) {
    var vm = this;


    vm.sendToCalculate = sendToCalculate;
    vm.order = {
      "make": makeOrder,
      "received": false 
    };

    vm.addRoom = addRoom;
    vm.removeRoom = removeRoom;
    vm.setSelectedCeiling = setSelectedCeiling;

    vm.calculatorData = null;
    vm.totalPrice = null;

    vm.isLoad = false;
    vm.rooms = [];
    vm.contacts = {};
    vm.patternForNumber = '^[\\d\\.\\,]+$';


    activate();

    function activate() {
      return getCalculatorData().then(function() {
        vm.addRoom();
        vm.isLoad = true;
      });
    }

    //Operations with rooms

    function addRoom() {
      return vm.rooms.push({
        ceilingId: null,
        square: null,
        quantityOfAngle: null,
        additionals: null
      });
    }

    function removeRoom(targetRoom) {
      vm.rooms = vm.rooms.filter(function(room) {
        return room != targetRoom;
      });
    }

    function setSelectedCeiling(roomId, ceilingId) {
      return vm.rooms[roomId].ceilingId = ceilingId;
    }

    //Actions with forms

    function isInvalid() {
      if(vm.CalculatorForm.$invalid) {
        return true;
      }

      for(var i = 0, n = vm.rooms.length; i < n; i++) {
        if(!vm.rooms[i].ceilingId) {
          return true;
        }
      }

      return false;
    }

    function sendToCalculate() {
      if(isInvalid()) {
        return;
      }

      return getTotalPrice(vm.rooms);
    }

    function makeOrderRequest() {
      if(vm.ContactForm.$invalid) {
        return;
      }
      return sendContacts(vm.contacts);
    }

    function makeOrder() {
      if(makeOrderRequest()){
        vm.order.received = true;
        return true;
      }else
        return;
    }

    //CalculatorService

    function getCalculatorData() {
      return CalculatorService.getCalculatorData()
        .then(function(data) {
          vm.calculatorData = data;
          return vm.calculatorData;
        });
    }

    function getTotalPrice(data) {
      return CalculatorService.getTotalPrice(data)
        .then(function(data) {
          vm.totalPrice = data;
          return vm.totalPrice;
        });
    }

    function sendContacts(data) {
      return CalculatorService.sendContacts(data);
    }

  }

})();
