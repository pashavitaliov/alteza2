(function() {
  'use strict';

  angular
    .module('calc')
    .service('CalculatorService', CalculatorService);

  CalculatorService.$inject = ['$http'];

  function CalculatorService($http) {

    this.getCalculatorData = getCalculatorData;
    this.getTotalPrice = getTotalPrice;
    this.sendContacts = sendContacts;

    function getCalculatorData() {
      return $http.get('/api/getCalculatorData')
        .then(getCalculatorDataComplete)
        .catch(getCalculatorDataFailed);

      function getCalculatorDataComplete(response) {
        return response.data.data;
      }

      function getCalculatorDataFailed(error) {
        console.log(error);
      }
    }

    function getTotalPrice(data) {
      return $http.post('/api/calculate', data)
        .then(getTotalPriceComplete)
        .catch(getTotalPriceFailed);

      function getTotalPriceComplete(response) {
        return response.data.data;
      }

      function getTotalPriceFailed(error) {
        console.log(error);
      }
    }

    function sendContacts(data) {
      return $http.post('/order_request/email_from_calculator', data)
        .then(sendContactsComplete)
        .catch(sendContactsFailed);

      function sendContactsComplete(response) {
        return true;
      }

      function sendContactsFailed(error) {
        console.log(error);
      }
    }
  }
})();
