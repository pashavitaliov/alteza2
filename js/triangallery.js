(function($) {
    $.fn.trianGallery = function() {
        var el = this,
            gallery = {
                controls:{},
                init: true
            },
            thumbs = el.find("ul li a"),
            current_img = el.find("#current_img img"),
            current_img_parent = el.find("#current_img"),
            current_thumb = el.find(".selected_img a"),
            zoom_btn = el.find("#current_img a.show-img");

        var setControls = function(){
            gallery.controls.el = $('<div class="controls-gallery" />');
            gallery.controls.next = $('<a class="next-slider" href="" />');
            gallery.controls.prev = $('<a class="prev-slider" href="" />');
            gallery.controls.next.bind('click touchend', clickNextBind);
            gallery.controls.prev.bind('click touchend', clickPrevBind);
            gallery.controls.el.append(gallery.controls.prev).append(gallery.controls.next);
            el.find("#current_img").append(gallery.controls.el);
            thumbs.bind('click touchend', clickThumb);
        };

        var clickThumb = function(e){
            var thumb = $(this);
            setThumbsEffects(thumb);
            el.setCurrentImg(thumb.attr("href"),thumb);
            checkNeighborEl(thumb.parent());
            e.preventDefault();
        };

        var clickNextBind = function(e){
            if(checkNextEL()){
                var next_thumb = current_thumb.parent().next().find("a");
                clickAction(next_thumb);
            }
            e.preventDefault();
        };

        var clickPrevBind = function(e){
            if(checkPrevEL()){
                var prev_thumb = current_thumb.parent().prev().find("a");
                clickAction(prev_thumb);
            }
            e.preventDefault();
        };

        var clickAction = function(el_){
            el.setCurrentImg(el_.attr("href"),el_);
            setThumbsEffects(el_);
        };

        var checkNextEL = function(){
            var el_ = current_thumb.parent().next();
            return checkNeighborEl(el_);
        };

        var checkPrevEL = function(){
            var el_ = current_thumb.parent().prev();
            return checkNeighborEl(el_);
        };

        var checkNeighborEl = function(el_){
            var flag = false;
            if(el_.length != 0){
                if(el_.next().length == 0){
                    gallery.controls.next.css({"display": "none"});
                }else{
                    gallery.controls.next.css({"display": "block"});
                }

                if(el_.prev().length == 0){
                    gallery.controls.prev.css({"display": "none"});
                }else{
                    gallery.controls.prev.css({"display": "block"});
                }
                flag = true;
            }
            return flag;
        };

        var setThumbsEffects = function(thumb){
            thumbs.parent().removeClass('selected_img');
            thumb.parent().addClass('selected_img');
        };

        var init = function(){
            setControls();
            gallery.controls.prev.css({"display": "none"});

        };

        el.setCurrentImg = function(src,thumb){
            current_img_parent.addClass('loader');
            current_img.attr("src",src);
            current_img.load(function(){
                current_img_parent.removeClass('loader');
            });

            zoom_btn.attr("href",src);
            current_thumb = thumb;
            if(gallery.init){
                gallery.controls.prev.css({"display": "block"});
                gallery.init = false;
            }
        };

        init();

        return this;
    };
})(jQuery);