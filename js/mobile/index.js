$(document).ready(function () {
    var windowWidth = $(window).width();
    var normalizeLink = $('<link rel="stylesheet" href="/css/libs/normalize.min.css">');
    var bxsliderLink = $('<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">');
    var mobileLink = $('<link rel="stylesheet" href="/css/mobile.css">');
    var customMobileLink = $('<link rel="stylesheet" href="/css/custom-mobile.css">');
    var mainLink = $('<link rel="stylesheet" href="/css/main.css">');
    var customDesktopLink = $('<link rel="stylesheet" href="/css/custom-desktop.css">');
    var mediaLink = $('<link rel="stylesheet" href="/css/media.css">');

    var mobileScript = $('<script src="/js/mobile/bundle.js"></script>');
    var mobileNavScript = $('<script src="/js/mobile/nav.js"></script>');

    if (windowWidth <= 768) {
        $('head').append(normalizeLink).append(bxsliderLink).append(mobileLink).append(customMobileLink);
        $('body').append(mobileScript).append(mobileNavScript);
    } else {
        $('head').append(mainLink).append(mediaLink).append(customDesktopLink);
    }

    $('.spinner-wrapper').hide();
    $('.desktop-site, .mobile-site').css({ opacity: 1 });
});