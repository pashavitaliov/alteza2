'use strict';

$(document).ready(function () {

  $('#burger').click(function () {
    $(this).toggleClass('active');
    $('.menu__item').slideToggle();
    $('.menu--bg-overlay').toggleClass('open');
  });
  $('.menu--bg-overlay').click(function () {
    $('#burger').toggleClass('active');
    $('.menu__item').slideToggle();
    $('.menu--bg-overlay').toggleClass('open');
  });

  var slider = void 0;
  var slider1 = void 0;
  var slider2 = void 0;
  var slider3 = void 0;
  var slider4 = void 0;
  var slider5 = void 0;
  var slider6 = void 0;
  var slider7 = void 0;

  var sliderInitialization = function sliderInitialization() {
    $(function () {
      slider = $('#main-slider').bxSlider({
        // mode: 'fade',
        captions: true,
        touchEnabled: false
        // slideWidth: 600
      });

      $('.main-slider-wrapper .bx-wrapper .bx-caption').append('<img src="/images/mobile/slider-control/angle-right-solid.svg"/>');
    });

    $('#actions-slider').bxSlider({
      captions: false,
      touchEnabled: false
    });

    slider1 = $('.main-projects-wrap #projects__slider').bxSlider({
      pagerCustom: '#projects__nav-wrap',
      // controls: false,
      touchEnabled: false
    });
    slider2 = $('#profiles__slider').bxSlider({ touchEnabled: false });
    slider3 = $('.catalog__slider-top-wrap #projects__slider').bxSlider({
      pagerCustom: '#projects__nav-wrap',
      touchEnabled: false
    });
    slider4 = $('.catalog__slider-top-wrap #projects__slider-catalog').bxSlider({
      controls: true,
      touchEnabled: false
    });
    slider5 = $('.catalog__slider-bottom-wrap #projects__slider').bxSlider({
      controls: true,
      touchEnabled: false
    });
    slider6 = $('.projects #projects__slider').bxSlider({
      pagerCustom: '#projects__nav-wrap',
      touchEnabled: false
    });
    slider7 = $('.projects #projects__slider-projects').bxSlider({
      controls: true,
      touchEnabled: false
    });
  };
  var slidersReload = function slidersReload() {
    if (slider && slider.length) {
      slider.reloadSlider();
    }
    if (slider1 && slider1.length) {
      slider1.reloadSlider();
    }
    if (slider2 && slider2.length) {
      slider2.reloadSlider();
    }
    if (slider3 && slider3.length) {
      slider3.reloadSlider();
    }
    if (slider4 && slider4.length) {
      slider4.reloadSlider();
    }
    if (slider5 && slider5.length) {
      slider5.reloadSlider();
    }
    if (slider6 && slider6.length) {
      slider6.reloadSlider();
    }
    if (slider7 && slider7.length) {
      slider7.reloadSlider();
    }
  };
  setTimeout(function () {
    sliderInitialization();
  });

  $(window).resize(function () {
    slidersReload();
  });

  $('#services-info-btn').on('click', function (e) {
    e.preventDefault();
    $('.services-info__text-wrap').removeClass('services-info__text-wrap--hidden');
    $('#services-info-btn').addClass('services-info__link--hidden');
  });
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImNsaWNrIiwidG9nZ2xlQ2xhc3MiLCJzbGlkZVRvZ2dsZSIsInNsaWRlciIsInNsaWRlcjEiLCJzbGlkZXIyIiwic2xpZGVyMyIsInNsaWRlcjQiLCJzbGlkZXI1Iiwic2xpZGVyNiIsInNsaWRlcjciLCJzbGlkZXJJbml0aWFsaXphdGlvbiIsImJ4U2xpZGVyIiwiY2FwdGlvbnMiLCJ0b3VjaEVuYWJsZWQiLCJhcHBlbmQiLCJwYWdlckN1c3RvbSIsImNvbnRyb2xzIiwic2xpZGVyc1JlbG9hZCIsImxlbmd0aCIsInJlbG9hZFNsaWRlciIsInNldFRpbWVvdXQiLCJ3aW5kb3ciLCJyZXNpemUiLCJvbiIsImUiLCJwcmV2ZW50RGVmYXVsdCIsInJlbW92ZUNsYXNzIiwiYWRkQ2xhc3MiXSwibWFwcGluZ3MiOiI7O0FBQUFBLEVBQUVDLFFBQUYsRUFBWUMsS0FBWixDQUFrQixZQUFNOztBQUV0QkYsSUFBRSxPQUFGLEVBQVdHLEtBQVgsQ0FBaUIsWUFBTTtBQUNyQkgsTUFBRSxTQUFGLEVBQWFJLFdBQWIsQ0FBeUIsUUFBekI7QUFDQUosTUFBRSxhQUFGLEVBQWlCSyxXQUFqQjtBQUNBTCxNQUFFLG1CQUFGLEVBQXVCSSxXQUF2QixDQUFtQyxNQUFuQztBQUNELEdBSkQ7QUFLQUosSUFBRSxtQkFBRixFQUF1QkcsS0FBdkIsQ0FBNkIsWUFBTTtBQUNqQ0gsTUFBRSxTQUFGLEVBQWFJLFdBQWIsQ0FBeUIsUUFBekI7QUFDQUosTUFBRSxhQUFGLEVBQWlCSyxXQUFqQjtBQUNBTCxNQUFFLG1CQUFGLEVBQXVCSSxXQUF2QixDQUFtQyxNQUFuQztBQUNELEdBSkQ7O0FBT0EsTUFBSUUsZUFBSjtBQUNBLE1BQUlDLGdCQUFKO0FBQ0EsTUFBSUMsZ0JBQUo7QUFDQSxNQUFJQyxnQkFBSjtBQUNBLE1BQUlDLGdCQUFKO0FBQ0EsTUFBSUMsZ0JBQUo7QUFDQSxNQUFJQyxnQkFBSjtBQUNBLE1BQUlDLGdCQUFKOztBQUVBLE1BQU1DLHVCQUF1QixTQUF2QkEsb0JBQXVCLEdBQU07QUFDakNkLE1BQUUsWUFBTTtBQUNOTSxlQUFTTixFQUFFLGNBQUYsRUFBa0JlLFFBQWxCLENBQTJCO0FBQ2xDO0FBQ0FDLGtCQUFVLElBRndCO0FBR2xDQyxzQkFBYTtBQUNiO0FBSmtDLE9BQTNCLENBQVQ7O0FBT0FqQixRQUFFLDhDQUFGLEVBQWtEa0IsTUFBbEQsQ0FBeUQsa0VBQXpEO0FBQ0QsS0FURDs7QUFXQWxCLE1BQUUsaUJBQUYsRUFBcUJlLFFBQXJCLENBQThCO0FBQzVCQyxnQkFBVSxLQURrQjtBQUU1QkMsb0JBQWE7QUFGZSxLQUE5Qjs7QUFLQVYsY0FBVVAsRUFBRSx1Q0FBRixFQUEyQ2UsUUFBM0MsQ0FBb0Q7QUFDNURJLG1CQUFhLHFCQUQrQztBQUU1RDtBQUNBRixvQkFBYTtBQUgrQyxLQUFwRCxDQUFWO0FBS0FULGNBQVVSLEVBQUUsbUJBQUYsRUFBdUJlLFFBQXZCLENBQWdDLEVBQUNFLGNBQWEsS0FBZCxFQUFoQyxDQUFWO0FBQ0FSLGNBQVVULEVBQUUsNkNBQUYsRUFBaURlLFFBQWpELENBQTBEO0FBQ2xFSSxtQkFBYSxxQkFEcUQ7QUFFbEVGLG9CQUFhO0FBRnFELEtBQTFELENBQVY7QUFJQVAsY0FBVVYsRUFBRSxxREFBRixFQUF5RGUsUUFBekQsQ0FBa0U7QUFDMUVLLGdCQUFVLElBRGdFO0FBRTFFSCxvQkFBYTtBQUY2RCxLQUFsRSxDQUFWO0FBSUFOLGNBQVVYLEVBQUUsZ0RBQUYsRUFBb0RlLFFBQXBELENBQTZEO0FBQ3JFSyxnQkFBVSxJQUQyRDtBQUVyRUgsb0JBQWE7QUFGd0QsS0FBN0QsQ0FBVjtBQUlBTCxjQUFVWixFQUFFLDZCQUFGLEVBQWlDZSxRQUFqQyxDQUEwQztBQUNsREksbUJBQWEscUJBRHFDO0FBRWxERixvQkFBYTtBQUZxQyxLQUExQyxDQUFWO0FBSUFKLGNBQVViLEVBQUUsc0NBQUYsRUFBMENlLFFBQTFDLENBQW1EO0FBQzNESyxnQkFBVSxJQURpRDtBQUUzREgsb0JBQWE7QUFGOEMsS0FBbkQsQ0FBVjtBQUlELEdBM0NEO0FBNENBLE1BQU1JLGdCQUFnQixTQUFoQkEsYUFBZ0IsR0FBTTtBQUMxQixRQUFHZixVQUFVQSxPQUFPZ0IsTUFBcEIsRUFBMkI7QUFDekJoQixhQUFPaUIsWUFBUDtBQUNEO0FBQ0QsUUFBR2hCLFdBQVdBLFFBQVFlLE1BQXRCLEVBQTZCO0FBQzNCZixjQUFRZ0IsWUFBUjtBQUNEO0FBQ0QsUUFBR2YsV0FBV0EsUUFBUWMsTUFBdEIsRUFBNkI7QUFDM0JkLGNBQVFlLFlBQVI7QUFDRDtBQUNELFFBQUdkLFdBQVdBLFFBQVFhLE1BQXRCLEVBQTZCO0FBQzNCYixjQUFRYyxZQUFSO0FBQ0Q7QUFDRCxRQUFHYixXQUFXQSxRQUFRWSxNQUF0QixFQUE2QjtBQUMzQlosY0FBUWEsWUFBUjtBQUNEO0FBQ0QsUUFBR1osV0FBV0EsUUFBUVcsTUFBdEIsRUFBNkI7QUFDM0JYLGNBQVFZLFlBQVI7QUFDRDtBQUNELFFBQUdYLFdBQVdBLFFBQVFVLE1BQXRCLEVBQTZCO0FBQzNCVixjQUFRVyxZQUFSO0FBQ0Q7QUFDRCxRQUFHVixXQUFXQSxRQUFRUyxNQUF0QixFQUE2QjtBQUMzQlQsY0FBUVUsWUFBUjtBQUNEO0FBQ0YsR0F6QkQ7QUEwQkFDLGFBQVcsWUFBTTtBQUNmVjtBQUNELEdBRkQ7O0FBSUFkLElBQUd5QixNQUFILEVBQVlDLE1BQVosQ0FBbUIsWUFBVztBQUM1Qkw7QUFDRCxHQUZEOztBQUlBckIsSUFBRSxvQkFBRixFQUF3QjJCLEVBQXhCLENBQTJCLE9BQTNCLEVBQW9DLFVBQUNDLENBQUQsRUFBTztBQUN6Q0EsTUFBRUMsY0FBRjtBQUNBN0IsTUFBRSwyQkFBRixFQUErQjhCLFdBQS9CLENBQTJDLGtDQUEzQztBQUNBOUIsTUFBRSxvQkFBRixFQUF3QitCLFFBQXhCLENBQWlDLDZCQUFqQztBQUNELEdBSkQ7QUFLRCxDQTFHRCIsImZpbGUiOiJidW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyIkKGRvY3VtZW50KS5yZWFkeSgoKSA9PiB7XG5cbiAgJCgnI21lbnUnKS5jbGljaygoKSA9PiB7XG4gICAgJCgnI2J1cmdlcicpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAkKCcubWVudV9faXRlbScpLnNsaWRlVG9nZ2xlKCk7XG4gICAgJCgnLm1lbnUtLWJnLW92ZXJsYXknKS50b2dnbGVDbGFzcygnb3BlbicpO1xuICB9KTtcbiAgJCgnLm1lbnUtLWJnLW92ZXJsYXknKS5jbGljaygoKSA9PiB7XG4gICAgJCgnI2J1cmdlcicpLnRvZ2dsZUNsYXNzKCdhY3RpdmUnKTtcbiAgICAkKCcubWVudV9faXRlbScpLnNsaWRlVG9nZ2xlKCk7XG4gICAgJCgnLm1lbnUtLWJnLW92ZXJsYXknKS50b2dnbGVDbGFzcygnb3BlbicpO1xuICB9KTtcblxuXG4gIGxldCBzbGlkZXI7XG4gIGxldCBzbGlkZXIxO1xuICBsZXQgc2xpZGVyMjtcbiAgbGV0IHNsaWRlcjM7XG4gIGxldCBzbGlkZXI0O1xuICBsZXQgc2xpZGVyNTtcbiAgbGV0IHNsaWRlcjY7XG4gIGxldCBzbGlkZXI3O1xuXG4gIGNvbnN0IHNsaWRlckluaXRpYWxpemF0aW9uID0gKCkgPT4ge1xuICAgICQoKCkgPT4ge1xuICAgICAgc2xpZGVyID0gJCgnI21haW4tc2xpZGVyJykuYnhTbGlkZXIoe1xuICAgICAgICAvLyBtb2RlOiAnZmFkZScsXG4gICAgICAgIGNhcHRpb25zOiB0cnVlLFxuICAgICAgICB0b3VjaEVuYWJsZWQ6ZmFsc2VcbiAgICAgICAgLy8gc2xpZGVXaWR0aDogNjAwXG4gICAgICB9KTtcblxuICAgICAgJCgnLm1haW4tc2xpZGVyLXdyYXBwZXIgLmJ4LXdyYXBwZXIgLmJ4LWNhcHRpb24nKS5hcHBlbmQoJzxpbWcgc3JjPVwiL2ltYWdlcy9tb2JpbGUvc2xpZGVyLWNvbnRyb2wvYW5nbGUtcmlnaHQtc29saWQuc3ZnXCIvPicpO1xuICAgIH0pO1xuXG4gICAgJCgnI2FjdGlvbnMtc2xpZGVyJykuYnhTbGlkZXIoe1xuICAgICAgY2FwdGlvbnM6IGZhbHNlLFxuICAgICAgdG91Y2hFbmFibGVkOmZhbHNlXG4gICAgfSk7XG5cbiAgICBzbGlkZXIxID0gJCgnLm1haW4tcHJvamVjdHMtd3JhcCAjcHJvamVjdHNfX3NsaWRlcicpLmJ4U2xpZGVyKHtcbiAgICAgIHBhZ2VyQ3VzdG9tOiAnI3Byb2plY3RzX19uYXYtd3JhcCcsXG4gICAgICAvLyBjb250cm9sczogZmFsc2UsXG4gICAgICB0b3VjaEVuYWJsZWQ6ZmFsc2VcbiAgICB9KTtcbiAgICBzbGlkZXIyID0gJCgnI3Byb2ZpbGVzX19zbGlkZXInKS5ieFNsaWRlcih7dG91Y2hFbmFibGVkOmZhbHNlfSk7XG4gICAgc2xpZGVyMyA9ICQoJy5jYXRhbG9nX19zbGlkZXItdG9wLXdyYXAgI3Byb2plY3RzX19zbGlkZXInKS5ieFNsaWRlcih7XG4gICAgICBwYWdlckN1c3RvbTogJyNwcm9qZWN0c19fbmF2LXdyYXAnLFxuICAgICAgdG91Y2hFbmFibGVkOmZhbHNlXG4gICAgfSk7XG4gICAgc2xpZGVyNCA9ICQoJy5jYXRhbG9nX19zbGlkZXItdG9wLXdyYXAgI3Byb2plY3RzX19zbGlkZXItY2F0YWxvZycpLmJ4U2xpZGVyKHtcbiAgICAgIGNvbnRyb2xzOiB0cnVlLFxuICAgICAgdG91Y2hFbmFibGVkOmZhbHNlXG4gICAgfSk7XG4gICAgc2xpZGVyNSA9ICQoJy5jYXRhbG9nX19zbGlkZXItYm90dG9tLXdyYXAgI3Byb2plY3RzX19zbGlkZXInKS5ieFNsaWRlcih7XG4gICAgICBjb250cm9sczogdHJ1ZSxcbiAgICAgIHRvdWNoRW5hYmxlZDpmYWxzZVxuICAgIH0pO1xuICAgIHNsaWRlcjYgPSAkKCcucHJvamVjdHMgI3Byb2plY3RzX19zbGlkZXInKS5ieFNsaWRlcih7XG4gICAgICBwYWdlckN1c3RvbTogJyNwcm9qZWN0c19fbmF2LXdyYXAnLFxuICAgICAgdG91Y2hFbmFibGVkOmZhbHNlXG4gICAgfSk7XG4gICAgc2xpZGVyNyA9ICQoJy5wcm9qZWN0cyAjcHJvamVjdHNfX3NsaWRlci1wcm9qZWN0cycpLmJ4U2xpZGVyKHtcbiAgICAgIGNvbnRyb2xzOiB0cnVlLFxuICAgICAgdG91Y2hFbmFibGVkOmZhbHNlXG4gICAgfSk7XG4gIH07XG4gIGNvbnN0IHNsaWRlcnNSZWxvYWQgPSAoKSA9PiB7XG4gICAgaWYoc2xpZGVyICYmIHNsaWRlci5sZW5ndGgpe1xuICAgICAgc2xpZGVyLnJlbG9hZFNsaWRlcigpXG4gICAgfVxuICAgIGlmKHNsaWRlcjEgJiYgc2xpZGVyMS5sZW5ndGgpe1xuICAgICAgc2xpZGVyMS5yZWxvYWRTbGlkZXIoKVxuICAgIH1cbiAgICBpZihzbGlkZXIyICYmIHNsaWRlcjIubGVuZ3RoKXtcbiAgICAgIHNsaWRlcjIucmVsb2FkU2xpZGVyKClcbiAgICB9XG4gICAgaWYoc2xpZGVyMyAmJiBzbGlkZXIzLmxlbmd0aCl7XG4gICAgICBzbGlkZXIzLnJlbG9hZFNsaWRlcigpXG4gICAgfVxuICAgIGlmKHNsaWRlcjQgJiYgc2xpZGVyNC5sZW5ndGgpe1xuICAgICAgc2xpZGVyNC5yZWxvYWRTbGlkZXIoKVxuICAgIH1cbiAgICBpZihzbGlkZXI1ICYmIHNsaWRlcjUubGVuZ3RoKXtcbiAgICAgIHNsaWRlcjUucmVsb2FkU2xpZGVyKClcbiAgICB9XG4gICAgaWYoc2xpZGVyNiAmJiBzbGlkZXI2Lmxlbmd0aCl7XG4gICAgICBzbGlkZXI2LnJlbG9hZFNsaWRlcigpXG4gICAgfVxuICAgIGlmKHNsaWRlcjcgJiYgc2xpZGVyNy5sZW5ndGgpe1xuICAgICAgc2xpZGVyNy5yZWxvYWRTbGlkZXIoKVxuICAgIH1cbiAgfTtcbiAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgc2xpZGVySW5pdGlhbGl6YXRpb24oKVxuICB9KTtcblxuICAkKCB3aW5kb3cgKS5yZXNpemUoZnVuY3Rpb24oKSB7XG4gICAgc2xpZGVyc1JlbG9hZCgpO1xuICB9KTtcblxuICAkKCcjc2VydmljZXMtaW5mby1idG4nKS5vbignY2xpY2snLCAoZSkgPT4ge1xuICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAkKCcuc2VydmljZXMtaW5mb19fdGV4dC13cmFwJykucmVtb3ZlQ2xhc3MoJ3NlcnZpY2VzLWluZm9fX3RleHQtd3JhcC0taGlkZGVuJyk7XG4gICAgJCgnI3NlcnZpY2VzLWluZm8tYnRuJykuYWRkQ2xhc3MoJ3NlcnZpY2VzLWluZm9fX2xpbmstLWhpZGRlbicpO1xuICB9KVxufSk7XG5cblxuIl19
