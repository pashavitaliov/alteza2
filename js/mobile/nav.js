$(document).ready(function () {
    $('.menu__item--with-sub-menu > .menu__link').on('click', function (event) {
        var menuListItem = $(this).parent();

        menuListItem.toggleClass('menu__item--active');
        menuListItem.find('.menu__list').slideToggle();

        event.preventDefault();
    });
});