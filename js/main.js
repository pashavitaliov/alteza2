$(document).ready(function(){
    
    navigationAlign();

    setTimeout(function () {
        $('.info-slider').bxSlider({
            mode: 'fade',
            captions: false
        });

        $('.profiles-slider').bxSlider({
            mode: 'fade',
            captions: false
        });
    }, 1000);

    $(".show-img").trianZoom();
    $(".order_call").trianPopUp();

    $(".no-active").click(function(e){
        e.preventDefault();
    });
    
    $(".mobile-menu-btn").click(function () {
        $(this).hide().siblings(".header-nav-list, .mobile-menu-close-btn").show();
    });
    $(".mobile-menu-close-btn").click(function () {
        $(this).hide().siblings(".header-nav-list").hide();
        $(this).siblings(".mobile-menu-btn").show();
    });

});

function navigationAlign(){
    var navigation = $('.fixed-nav-panel'),
        navigationHeight = navigation.height(),
        windowHeight = $(window).height(),
        heightsResidual = windowHeight - navigationHeight,
        navigationIndent = 0;

    if(heightsResidual > 0){
        navigationIndent = heightsResidual / 2
    }

    navigation.css({
        'top': navigationIndent
    })
}