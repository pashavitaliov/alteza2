(function(){
    var v = document.getElementById("video"),
        v_wrap = document.getElementById("strong-five-video");

    v_wrap.onclick = function() {
        if (v.paused) {
            v.play();
            v_wrap.querySelector("#video_pause").style.opacity = "0";

        } else {
            v.pause();
            v_wrap.querySelector("#video_pause").style.opacity = "1";
        }
    };

    v.onended = function(){
        v_wrap.querySelector("#video_pause").style.opacity = "1";
    };

})();