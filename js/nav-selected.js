(function(){
    var url = location.href.split('/')[3];
    $('.header-nav-list li a').each(function(a){
        var href = $(this).attr('href').split('/')[1];
        if(href == url)
            $(this).parent().addClass('header-nav-active');
    });
})();