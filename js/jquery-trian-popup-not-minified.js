(function($) {
    $.fn.trianPopUp = function() {
        var el = this,
            tmpl={},
            subj = "Заказать звонок";
        var windowWidth = $(window).width();

        var setControls = function(){
            el.bind('click touchend', function(e){
                ($(e.target).hasClass('measurement')) ? subj = "Заказать обмер" : subj = "Заказать бесплатный обратный звонок";
                if (windowWidth <= 768) {
                    createMobileTmpl();
                } else {
                    createTmpl();
                }
                e.preventDefault();
            });
        };

        var init = function(){
            setControls();
        };

        var createTmpl = function(){
            tmpl.success_message = $('<div class="call-order-success-message">Спасибо! В ближайшее время с Вами свяжется специалист.</div>');
            tmpl.wrapper = $('<div class="cover_wrapper call_order" />');
            tmpl.wrapper.css({"top": $(window).scrollTop()});
            tmpl.body = $("body");
            tmpl.body.css("overflow","hidden");
            tmpl.form = $('<form method="post" action=""/>');
            tmpl.title = $('<h3>'+subj+'</h3>');
            tmpl.sub_title = $('<p class="subtitle">Пожалуйста, оставьте свои контактные данные, в ближайшее время с Вами свяжется специалист.</p>');
            tmpl.label_name = $('<label><div class="input-title">Ваше имя *</div></label>');
            tmpl.input_name = $('<input type="text" name="name"/>');
            tmpl.name_error_msg = $('<p class="error-msg"></p>');
            tmpl.label_name.append(tmpl.input_name).append(tmpl.name_error_msg);
            tmpl.label_phone = $('<label class="clearfix"><div class="input-title">Номер телефона *</div></label>');
            tmpl.input_phone = $('<input type="text" name="phone"/>');
            tmpl.phone_error_msg = $('<p class="error-msg"></p>');
            tmpl.label_phone.append(tmpl.input_phone).append(tmpl.phone_error_msg);
            tmpl.submit = $('<input type="submit" name="submit" value="Заказать звонок"/>');
            tmpl.form.append(tmpl.title).append(tmpl.sub_title).append(tmpl.label_name).append(tmpl.label_phone).append(tmpl.submit);
            tmpl.wrapper.append(tmpl.form).append(tmpl.success_message);
            tmpl.body.append(tmpl.wrapper);
            tmpl.wrapper.bind('click touchend',destroy);
            tmpl.submit.bind('click touchend',submit);
            tmpl.input_name.bind('change',validate);
            tmpl.input_phone.bind('change',validate);
            tmpl.input_phone.inputmask('+375-99-999-99-99');
        };

        var createMobileTmpl = function(){
            tmpl.success_message = $('<div class="call-order-success-message">Спасибо! В ближайшее время с Вами свяжется специалист.</div>');
            tmpl.wrapper = $('<div class="cover_wrapper cover_wrapper--mobile" />');
            tmpl.wrapper.css({"top": $(window).scrollTop()});
            tmpl.body = $("body");
            tmpl.body.css("overflow","hidden");
            tmpl.form = $('<form method="post" action="" class="container dealers-form-inner block--bg-gainsboro"/>');
            tmpl.title = $('<div class="dealers-form__title"><h5 class="uppercase">'+subj+'</h5></div>');

            tmpl.label_name = $('<div></div>');
            tmpl.input_name = $('<input class="dealers-form__input" name="name" type="text" placeholder="Ваше имя"/>');
            tmpl.name_error_msg = $('<p class="error-msg"></p>');
            tmpl.label_name.append(tmpl.input_name).append(tmpl.name_error_msg);

            tmpl.label_phone = $('<div></div>');
            tmpl.input_phone = $('<input class="dealers-form__input" name="phone" type="text" placeholder="Телефон" />');
            tmpl.phone_error_msg = $('<p class="error-msg"></p>');
            tmpl.label_phone.append(tmpl.input_phone).append(tmpl.phone_error_msg);

            tmpl.submit = $('<input type="submit" name="submit" value="Заказать звонок" class="btn dealers-form__btn"/>');

            tmpl.form.append(tmpl.title).append(tmpl.label_name).append(tmpl.label_phone).append(tmpl.submit);

            tmpl.wrapper.append(tmpl.form).append(tmpl.success_message);
            tmpl.body.append(tmpl.wrapper);
            tmpl.wrapper.bind('click touchend',destroy);
            tmpl.submit.bind('click touchend',submit);
            tmpl.input_name.bind('change',validate);
            tmpl.input_phone.bind('change',validate);
            tmpl.input_phone.inputmask('+375-99-999-99-99');
        };

        var submit = function(e){
            if(validate()){
                http();
            }
            e.preventDefault();
        };

        var http = function(){
            $.ajax({
                type: "POST",
                url: "/order_request/email",
                data: {
                    "name" : tmpl.input_name.val(),
                    "phone" : tmpl.input_phone.val()
                },
                success: handleSuccess
            });
        };

        var validate = function(){
            var name = tmpl.input_name,
                phone = tmpl.input_phone,
                flag = true;
            if(name.val().length == 0) {
                name.addClass("error");
                tmpl.name_error_msg.text("Заполните поле");
                flag = false;
            }else{
                name.removeClass("error");
                tmpl.name_error_msg.text("");
            }
            if(phone.val().length == 0) {
                phone.addClass("error");
                tmpl.phone_error_msg.text("Заполните поле");
                flag = false;
            }else {
                if(phone.val().match(/_/)){
                    phone.addClass("error");
                    tmpl.phone_error_msg.text("Введите корректный телефон");
                    flag = false;
                }else {
                    phone.removeClass("error");
                    tmpl.phone_error_msg.text("");
                }
            }
            return flag;
        };

        var destroy = function(e){
            if($(e.target).hasClass('cover_wrapper')) remove();
        };

        var remove = function(){
            $(".cover_wrapper").remove();
            tmpl.body.css("overflow","auto");
        };

        var handleSuccess = function () {
            tmpl.form.hide();
            tmpl.success_message.show();
            setTimeout(remove, 3000);
        };

        init();

        return this;
    };
})(jQuery);